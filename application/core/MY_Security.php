<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class MY_Security extends CI_Security {

    public function __construct()
    {
        parent::__construct();
    }

    /*
     * csrf_show_error: overriding the default error with a redirect to our login page
     *
     * @return void
     *
     */

    public function csrf_show_error()
    {
        http://www.johnkieken.com/how-to-handle-an-expired-csrf-token-after-a-page-is-left-open/
        header('Location: /auth/login', TRUE, 302);
    }
}