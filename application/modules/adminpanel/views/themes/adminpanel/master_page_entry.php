<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>

<?php $this->load->view('themes/'. Settings_model::$db_config['adminpanel_theme'] .'/partials/content_head.php'); ?>

<?php $this->load->view('generic/flash_error'); ?>

<!-- <?php print_r($roles) ?> -->


<div class="row">
  <div class="col-sm-4">

    <div class="form-group">
      <label><?php print $this->lang->line('master_page_upload_type'); ?></label>
      <select class="form-control" name= "page_upload_type" id="page_upload_type" required="required">
       <option value="">--Select Upload Type--</option><?php foreach($upload_types as $upload_type) {?>
       <option value="<?php print $upload_type->upload_typ_id; ?>">
        <?php print $upload_type->upload_typ_name; ?></option><?php } ?>
      </select>
    </div>
  </div>

  <div class="col-sm-4">
    <div class="form-group"> 
      <label><?php print $this->lang->line('master_page_intended'); ?></label>
      <select class="form-control"  required="required"name="page_role_id" id="page_role_id">
       <option value="">--Select Role--</option><?php foreach($roles as $role) {?>
       <option value="<?php print $role->role_id; ?>">
        <?php print $role->role_name; ?></option><?php } ?>   
      </select> 
    </div>
  </div>
  <div class="col-sm-4 ">
   <div class="form-group">
    <label><?php print $this->lang->line('master_page_activation'); ?></label>
    <select class="form-control" name= "page_active" id="page_active"  required="required">
     <option value="">--Select Activation--</option>
     <option value="1"> Activate </option>
     <option value="0"> De-Activate </option>

   </select>
 </div>
</div>

<div class="col-sm-4">
  <div class="page_noty_typ">

  </div>  
</div>

<div class="col-sm-8">
  <div class="page_noty_content">

  </div>
</div>


<div class="col-sm-4">
  <div class="page_title">

  </div>

</div>

<div class="col-sm-4">
  <div class="page_header">

  </div>
</div>



<div class="col-sm-4">
  <div class="page_st_dt">

  </div>
</div>

<div class="col-sm-4">
  <div class="page_end_dt">

  </div>
</div>


<div class="col-lg-12">
  <div class="page_upload">

  </div>
  
</div>


</div>

<div class="row" style="margin-top: 10px; margin-bottom: 100px">
  <div class="col-lg-12">
    <div class="form-group">
      <button type="submit" name="add_page_submit" id="add_page_submit" class="add_page_submit btn btn-primary btn-lg" data-loading-text="<?php print $this->lang->line('master_page_btn_loading_text'); ?>"><i class="fa fa-user-plus pd-r-5"></i> <?php print $this->lang->line('master_page_btn'); ?></button>
    </div>
  </div>
</div>


<script>

  function date_func() {
   $( "#start_date" ).datepicker();
   $( "#anim" ).on( "change", function() {
    $( "#start_date" ).datepicker( "option", "showAnim", $( this ).val() );
  });

   $( "#end_date" ).datepicker();
   $( "#anim" ).on( "change", function() {
    $( "#end_date" ).datepicker( "option", "showAnim", $( this ).val() );
  });
 }  


 $(document).ready(function(){

  var formdata = false;  
  var err_data =[];

  if (window.FormData) {
    notification = new FormData();
  }

  var pg_title = '<div class="form-group"><label for="page_title"><?php print $this->lang->line('master_page_title'); ?></label> <input type="text" name="page_title" id="page_title" class="form-control" placeholder = "<?php print $this->lang->line('master_page_title'); ?>" value="<?php print $this->session->flashdata('page_title'); ?>"data-parsley-maxlength="100"required></div>';

  var pg_header = '<div class="form-group"><label for="page_header"><?php print $this->lang->line('master_page_header'); ?></label><input type="text" name="page_header" id="page_header" class="form-control" placeholder = "<?php print $this->lang->line('master_page_header'); ?>"    value="<?php print $this->session->flashdata('page_header'); ?>"data-parsley-maxlength="100"required> </div>';

  var pg_upload_typ = '<div class="form-group"><label><?php print $this->lang->line('master_page_notice'); ?></label><select class="form-control"  required="required"name="page_notice_id" id="page_notice_id"> <option value="">--Select Notice Type--</option><?php foreach($notices as $notice) {?> <option value="<?php print $notice->notice_id; ?>"> <?php print $notice->notice_name; ?></option><?php } ?></select></div>';

  var pg_menu_name = '<div class="form-group"><label><?php print $this->lang->line('master_page_menu'); ?></label><select class="form-control"  required="required"name="page_menu_id" id="page_menu_id">  <option value="">--Select Menu--</option><?php foreach($menus as $menu) {?> <option value="<?php print $menu->menu_id; ?>"><?php print $menu->menu_name; ?></option><?php } ?></select></div>';

  var pg_content = '<div class="col-lg-12 nopadding"><label for="page_content"><?php print $this->lang->line('master_page_content'); ?></label><div class="col-lg-12 nopadding"><textarea name="page_content" id="page_content"></textarea></div> </div>';

  var pg_pdf_upload = '<div class="form-group"> <label for="page_pdf_upload"><?php print $this->lang->line('master_page_pdf_file'); ?></label>  <input type="file" name="page_pdf_upload" id="page_pdf_upload" class="form-control"  accept="pdf" required size="2048" placeholder = "<?php print $this->lang->line('master_page_pdf_file'); ?>" value="<?php print $this->session->flashdata('page_pdf_upload'); ?>" data-parsley-maxlength="100"required> <span class="required">* Allowed file size is 2MB and file type is pdf.</span></div>';

  var pg_img_upload = '<div class="form-group"> <label for="page_img_upload"><?php print $this->lang->line('master_page_img_file'); ?></label>  <input type="file" name="page_img_upload" id="page_img_upload" class="form-control"  accept="image/x-png,image/gif,image/jpeg" required size="2048" placeholder = "<?php print $this->lang->line('master_page_img_file'); ?>" value="<?php print $this->session->flashdata('page_img_upload'); ?>" data-parsley-maxlength="100"required> </div>';

  var pg_st_dt = '<div class="form-group"><label>Start Date</label>  <input type="text" readonly="readonly" name="start_date" id="start_date"   class="form-control" placeholder = "Start Date" value="<?php print $this->session->flashdata('start_date'); ?>"data-parsley-maxlength="100" required></div>';

  var pg_end_dt = '<div class="form-group">  <label>End Date</label> <input type="text" readonly="readonly" name="end_date" id="end_date" class="form-control" placeholder = "End Date" value="<?php print $this->session->flashdata('end_date'); ?>" data-parsley-maxlength="100" required> </div>';

  $('#page_upload_type').change(function(){
    $('.page_upload').html('');
    if($('#page_upload_type').val() == '1' ) {

      $('.page_header').html('');
      $('.page_title').html('');
      $('.page_noty_typ').html('');
      $('.page_st_dt').html('');
      $('.page_end_dt').html('');
      $('.page_title').append(pg_title);
      $('.page_header').append(pg_header);
      $('.page_noty_typ').append(pg_menu_name);
      $('.page_upload').append(pg_content);        
      $("#page_content").Editor();
      $('.page_upload').prop('required',true);
    } else if ($('#page_upload_type').val() == '2' )  {

     $('.page_header').html('');
     $('.page_title').html('');
     $('.page_noty_typ').html('');
     $('.page_st_dt').html('');
     $('.page_end_dt').html('');
     $('.page_title').append(pg_title);
     $('.page_header').append(pg_header);
     $('.page_noty_typ').append(pg_menu_name);
     $('.page_upload').append(pg_pdf_upload);
     $('.page_upload').prop('required',true);
   } else if ($('#page_upload_type').val() == '3' )  {

     $('.page_header').html('');
     $('.page_title').html('');
     $('.page_noty_typ').html('');
     $('.page_st_dt').html('');
     $('.page_end_dt').html('');
     $('.page_title').append(pg_title);
     $('.page_header').append(pg_header);
     $('.page_noty_typ').append(pg_menu_name);
     $('.page_upload').append(pg_img_upload);
     $('.page_upload').prop('required',true);
   }else if ($('#page_upload_type').val() == '4' )  {

     $('.page_header').html('');
     $('.page_title').html('');
     $('.page_noty_typ').html('');
     $('.page_title').append(pg_title);
     $('.page_header').append(pg_header);
     $('.page_noty_typ').append(pg_menu_name);
     $('.page_upload').append(pg_pdf_upload);
     $('.page_upload').prop('required',true);
     $('.page_st_dt').append(pg_st_dt);
     $('.page_end_dt').append(pg_end_dt);
     date_func();
     $('.page_st_dt').prop('required',true);
   }else if ($('#page_upload_type').val() == '5' )  {

     $('.page_header').html('');
     $('.page_title').html('');
     $('.page_noty_typ').html('');
     $('.page_st_dt').html('');
     $('.page_end_dt').html('');
     $('.page_upload').html('');
     $('.page_noty_typ').append(pg_upload_typ);
     $('#page_notice_id').change(function(){ 


      if($('#page_notice_id').val() == '1' ) {

       $('.page_header').html('');
       $('.page_title').html('');
       $('.page_upload').html('');
       $('.page_noty_content').html('');
       $('.page_title').append(pg_title);
       $('.page_header').append(pg_header);
       $('.page_upload').append(pg_pdf_upload);
       $('.page_upload').prop('required',true);

     } else if ($('#page_notice_id').val() == '2' )  {

       $('.page_header').html('');
       $('.page_title').html('');
       $('.page_upload').html('');
       $('.page_noty_content').html('');
       $('.page_title').append(pg_title);
       $('.page_header').append(pg_header);
       $('.page_upload').append(pg_pdf_upload);
       $('.page_upload').prop('required',true);
     }else if ($('#page_notice_id').val() == '3' )  {

       $('.page_header').html('');
       $('.page_title').html('');
       $('.page_upload').html('');
       $('.page_noty_content').html('');
       $('.page_title').append(pg_title);
       $('.page_header').append(pg_header);
       $('.page_upload').append(pg_pdf_upload);
       $('.page_upload').prop('required',true);
     }else if ($('#page_notice_id').val() == '4' )  {

      $('.page_header').html('');
      $('.page_title').html('');
      $('.page_upload').html('');
      $('.page_noty_content').html('');
      $('.page_noty_content').append(pg_title);
      $('.page_noty_content').prop('required',true);

    } else {
     $('.page_header').html('');
     $('.page_title').html('');
     $('.page_noty_content').html('');
     $('.page_upload').html('');
     $('.page_st_dt').html('');
     $('.page_end_dt').html('');
    }


  });
   } else {

     $('.page_header').html('');
     $('.page_title').html('');
     $('.page_noty_typ').html('');
     $('.page_upload').html('');
     $('.page_st_dt').html('');
     $('.page_end_dt').html('');
     $('.page_noty_content').html('');
   }
 });





$("#add_page_submit").click(function(){

  var page_upload_type  =  $('#page_upload_type').val();
  var page_role_id      =  $('#page_role_id').val();
  var page_notice_id    =  $('#page_notice_id').val();
  var page_noty_title   =  $('#page_noty_content').val();
  var page_menu_id      =  $('#page_menu_id').val();
  var page_title        =  $('#page_title').val();
  var page_header       =  $('#page_header').val();
  var page_act          =  $('#page_active').val();
  var page_pdf          =  $('#page_pdf_upload').val();
  var page_img          =  $('#page_img_upload').val();
  var page_stDate       =  $('#start_date').val();
  var page_endDate      =  $('#end_date').val();
  var page_content;
  if ($('#page_upload_type').val() == '1') {
    page_content    =  $("#page_content").Editor("getText");
  }


  var data = [];

  if (page_upload_type=='' ) {

    alert('Please select upload type');
    return false;

  } 

  function validate_data() {

    if (page_upload_type == 1) {

        if (page_role_id=='' || page_role_id==null ) {
        err_data.push('Please select role.\n');
        } if (page_act=='' || page_act==null ) {
        err_data.push('Please select activation.\n');
        }
        if (page_menu_id=='' || page_menu_id==null ) {
        err_data.push('Please select Menu.\n');
        }
        if (page_title=='' || page_title==null ) {
        err_data.push('Please Enter Title.\n');
        }
        if (page_header=='' || page_header==null ) {
        err_data.push('Please Enter Header.\n');
        }
        if (page_content=='' || page_content==null ) {
        err_data.push('Please Enter Page Content.\n');
        }

    } else if (page_upload_type == 2) {

      if (page_role_id=='' || page_role_id==null ) {
        err_data.push('Please select role.\n');
        } if (page_act=='' || page_act==null ) {
        err_data.push('Please select activation.\n');
        }
        if (page_menu_id=='' || page_menu_id==null ) {
        err_data.push('Please select Menu.\n');
        }
        if (page_title=='' || page_title==null ) {
        err_data.push('Please Enter Title.\n');
        }
        if (page_header=='' || page_header==null ) {
        err_data.push('Please Enter Header.\n');
        }
        if (page_pdf=='' || page_pdf==null ) {
        err_data.push('Please Select PDF File.\n');
        }

    }else if (page_upload_type == 3) {

      if (page_role_id=='' || page_role_id==null ) {
        err_data.push('Please select role.\n');
        } if (page_act=='' || page_act==null ) {
        err_data.push('Please select activation.\n');
        }
        if (page_menu_id=='' || page_menu_id==null ) {
        err_data.push('Please select Menu.\n');
        }
        if (page_title=='' || page_title==null ) {
        err_data.push('Please Enter Title.\n');
        }
        if (page_header=='' || page_header==null ) {
        err_data.push('Please Enter Header.\n');
        }
        if (page_img=='' || page_img==null ) {
        err_data.push('Please Select Image File.\n');
        }

    } else if (page_upload_type == 4) {

      if (page_role_id=='' || page_role_id==null ) {
        err_data.push('Please select role.\n');
        } if (page_act=='' || page_act==null ) {
        err_data.push('Please select activation.\n');
        }
        if (page_menu_id=='' || page_menu_id==null ) {
        err_data.push('Please select Menu.\n');
        }
        if (page_title=='' || page_title==null ) {
        err_data.push('Please Enter Title.\n');
        }
        if (page_header=='' || page_header==null ) {
        err_data.push('Please Enter Header.\n');
        }
        if (page_stDate=='' || page_stDate==null ) {
        err_data.push('Please Enter Start Date.\n');
        }
        if (page_endDate=='' || page_endDate==null ) {
        err_data.push('Please Enter End Date.\n');
        }
        if (page_pdf=='' || page_pdf==null ) {
        err_data.push('Please Select PDF File.\n');
        }

    }else if (page_upload_type == 5) {
       if (page_role_id=='' || page_role_id==null ) {
        err_data.push('Please select role.\n');
        } 
        if (page_act=='' || page_act==null ) {
        err_data.push('Please select activation.\n');
        }
        if (page_notice_id=='' || page_notice_id==null ) {
        err_data.push('Please select Notice Type.\n');
        }
        
        if (page_notice_id==1 || page_notice_id==2 || page_notice_id==3 ) {
        if (page_title=='' || page_title==null ) {
        err_data.push('Please Enter Title.\n');
        }
        if (page_header=='' || page_header==null ) {
        err_data.push('Please Enter Header.\n');
        }
        if (page_pdf=='' || page_pdf==null ) {
        err_data.push('Please Select File to upload.\n');
        }
        } else if (page_notice_id==4) {
          if (page_title=='' || page_title==null ) {
        err_data.push('Please Enter Title noty.\n');
        }
        }

    }

        if (err_data=='') {
          return true;
        } else {          
          alert(err_data);
          err_data =[];
        }
        
    
 
}

  if (page_upload_type==1) {
    //alert(validate_data());
    if (validate_data()) {
      ajaxupload(notification,page_upload_type,page_role_id,page_notice_id,page_menu_id,page_title,page_header,page_content,page_stDate,page_endDate,page_act);
    } 
    

  } else if (page_upload_type ==2) {
  if (validate_data()) {
    var file_size = $('#page_pdf_upload')[0].files[0].size;
    if (file_size > 2094304) {
     alert('File Size Must be Less Then 2MB..');
     return false;
   }else {
     input = document.getElementById('page_pdf_upload');
     file = input.files[0]; 
     var ext = file.type.toLowerCase();
     if (ext=='application/pdf' || ext=='application/x-download') {
      if ( window.FileReader ) {
        reader = new FileReader();
        reader.readAsDataURL(file);
      }
      if (notification) {
        notification.append("images",file);
      }
    } else {
      alert('check file type');
    } 
    if (notification) {  
      ajaxupload(notification,page_upload_type,page_role_id,page_notice_id,page_menu_id,page_title,page_header,page_content,page_stDate,page_endDate,page_act);
    }
  }
}
} else if (page_upload_type==3) {
if (validate_data()) {
  var file_size = $('#page_img_upload')[0].files[0].size;
  if (file_size > 2094304) {
   alert('File Size Must be Less Then 2MB..');
   return false;
 }else {
   input = document.getElementById('page_img_upload');
   file = input.files[0]; 

   var ext = file.type.toLowerCase();
   if (ext=='image/jpeg' || ext=='image/jpg' || ext=='image/png' || ext=='image/gif')  {
    if ( window.FileReader ) {
      reader = new FileReader();
      reader.readAsDataURL(file);
    }
    if (notification) {
      notification.append("images",file);
    }
  } else {
    alert('check file type');
  } 
  if (notification) {  
    ajaxupload(notification,page_upload_type,page_role_id,page_notice_id,page_menu_id,page_title,page_header,page_content,page_stDate,page_endDate,page_act);
  }
}
}

}  else if (page_upload_type ==4) {
  if (validate_data()) {

  var file_size = $('#page_pdf_upload')[0].files[0].size;
  if (file_size > 2094304) {
   alert('File Size Must be Less Then 2MB..');
   return false;
 }else {
   input = document.getElementById('page_pdf_upload');
   file = input.files[0]; 
   var ext = file.type.toLowerCase();
   if (ext=='application/pdf' || ext=='application/x-download') {
    if ( window.FileReader ) {
      reader = new FileReader();
      reader.readAsDataURL(file);
    }
    if (notification) {
      notification.append("images",file);
    }
  } else {
    alert('check file type');
  } 
  if (notification) {  
    ajaxupload(notification,page_upload_type,page_role_id,page_notice_id,page_menu_id,page_title,page_header,page_content,page_stDate,page_endDate,page_act);
  }
}
}
}  else if (page_upload_type ==5) {
  if (validate_data()) {
  if(page_notice_id==1 || page_notice_id==2 || page_notice_id==3  ) {

    var file_size = $('#page_pdf_upload')[0].files[0].size;
    if (file_size > 2094304) {
     alert('File Size Must be Less Then 2MB..');
     return false;
   }else {
     input = document.getElementById('page_pdf_upload');
     file = input.files[0]; 
     var ext = file.type.toLowerCase();
     if (ext=='application/pdf' || ext=='application/x-download') {
      if ( window.FileReader ) {
        reader = new FileReader();
        reader.readAsDataURL(file);
      }
      if (notification) {
        notification.append("images",file);
      }
    } else {
      alert('check file type');
    } 
    if (notification) {  
      ajaxupload(notification,page_upload_type,page_role_id,page_notice_id,page_menu_id,page_title,page_header,page_content,page_stDate,page_endDate,page_act);
    }
  }
} else if (page_notice_id==4) {
   ajaxupload(notification,page_upload_type,page_role_id,page_notice_id,page_menu_id,page_title,page_header,page_content,page_stDate,page_endDate,page_act);

} 

}
}


   //alert(page_img);

   function ajaxupload(formdata,page_upload_type,page_role_id,page_notice_id='',page_menu_id='',page_title='',page_header='',page_content='',page_st_dt='',page_end_dt='',page_active=''){

    formdata.append("csrf_token_name",CI.csrf_cookie_name);
    formdata.append("page_upload_type",page_upload_type);
    formdata.append("page_role_id",page_role_id);
    formdata.append("page_notice_id",page_notice_id);
    formdata.append("page_menu_id",page_menu_id);
    formdata.append("page_title",page_title);
    formdata.append("page_header",page_header);
    formdata.append("page_content",page_content);
    formdata.append("page_start_date",page_st_dt);
    formdata.append("page_end_date",page_end_dt);
    formdata.append("page_active",page_active);

    //alert(formdata);

    $.ajax({
     type: 'post',
     url: '<?php echo base_url('adminpanel/master_page_entry/create_page');?>',
     
     async: true,
     data: formdata,
     processData: false,
     contentType: false,
     dataType: 'json',


     success: function (j) {
      if(j.valid == "true"){
        alert('Data saved successfully');
        location.reload();
      } else {
        alert('Data not saved , Please try again !!!');
      }
    },
    complete:function(msg){

    }
  }); 

  }


});







});





  /*$("#add_page_submit").validate({
    ignore:[],
    rules: {

     page_upload_type:{
       required: true
     },
     select_menu_is_parent:{
       required: true
     },
     select_menu_has_parent:{
       required: true
     },
     menu_parent_id:{
       required: true
     }

   },
   messages: {

    select_menu_active:{
      required: " Please select activation"
    },
    select_menu_is_parent:{
      required: " Please select YES or NO"
    },
    select_menu_has_parent:{
      required: " Please select YES or NO"
    },
    menu_parent_id:{
      required: " Please select a Menu"
    }
  }
});*/


</script>
