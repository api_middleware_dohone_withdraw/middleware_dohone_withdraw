<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>

<?php $this->load->view('themes/'. Settings_model::$db_config['adminpanel_theme'] .'/partials/content_head.php'); ?>

<?php $this->load->view('generic/flash_error'); ?>

<?php print form_open('adminpanel/change_password/update_password', array('id' => 'add_member_form', 'class' => 'js-parsley', 'data-parsley-submit' => 'add_member_submit')) ."\r\n"; ?>

<div class="row">


	<div class="col-sm-6">
     


		<div class="form-group">
			<label for="password"><?php print $this->lang->line('add_member_password'); ?></label>
			<input type="password" name="password" id="password" class="form-control"
                   data-parsley-maxlength="255"
                   required>
		</div>

		<div class="form-group">
			<label for="password_confirm"><?php print $this->lang->line('add_member_password_confirm'); ?></label>
			<input type="password" name="password_confirm" id="password_confirm" class="form-control"
                   data-parsley-maxlength="255"
                   required>
		</div>

		<p>
			<button type="submit" name="add_member_submit" id="add_member_submit" class="add_member_submit btn btn-primary btn-lg" data-loading-text="<?php print 'wait...'; ?>"><i class="fa fa-user-plus pd-r-5"></i> <?php print 'Update'; ?></button>
		</p>

	</div>
</div>
<?php print form_close() ."\r\n"; ?>

