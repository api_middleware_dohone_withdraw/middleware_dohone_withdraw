<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>
<style type="text/css">
  .error{
    color: red;
  }
</style>
<?php $this->load->view('themes/'. Settings_model::$db_config['adminpanel_theme'] .'/partials/content_head.php'); ?>
<?php $this->load->view('generic/flash_error'); ?>

<?php /* $l_table = $list_table->result() ; 
echo "<pre>";
print_r($l_table); */
?>

<div class="panel card bd-0">
  <div class="panel-body bg-white">
    <div class="table-responsive">
      <table class="table table-hover" id="table1">
        <thead>
          <tr>
            
            <th style="width: 17px;">SL NO</th>
            <th style="width: 17px;">Title</th>
            <th style="width: 30px;">URL</th>
            <th style="width: 17px;">Position</th>
            <th style="width: 17px;">Parent ID</th>
            <th style="width: 30px;">IS Parent</th>
            <th style="width: 30px;">Show Menu</th>
            <th style="width: 17px;">Edit</th>
          </tr>
        </thead>
        <tbody>

          <?php
          $i=0;
          foreach($list_table->result() as $l_table) { ?>
          <tr>
            
            <td><?php print $l_table->menu_id; ?></td>
            <td><?php print $l_table->title; ?></td>
            <td><?php print $l_table->url; ?></td>
            <td><?php print $l_table->position; ?></td>
            <td><?php print $l_table->parent_id; ?></td>
            <?php if ($l_table->is_parent == 1) { ?>
            <td style="color:green;font-weight: bold;"> <?php  print "YES"; ?> </td>
            <?php } else { ?>
            <td style="color:red;font-weight: bold;"> <?php  print "NO"; ?> </td>
            <?php  }  ?>
            <?php if ($l_table->show_menu == 1) { ?>
            <td style="color:green;font-weight: bold;"> <?php  print "YES"; ?> </td>
            <?php } else { ?>
            <td style="color:red;font-weight: bold;"> <?php  print "NO"; ?> </td>
            <?php  }  ?>                        
            <td>
              <a href="<?php print base_url(); ?>adminpanel/add_menu/edit_menu/<?php print $l_table->menu_id; ?>" style="color:blue;font-weight: bold;">Edit</a>
            </td>
          </tr>
          <?php } ?>
        </tbody>
      </table>
    </div>
  </div>
</div>


<script>
  $(function () {
    $('#table1').DataTable({
      "paging": true,
      "lengthChange": true,
      "searching": true,
      "ordering": false,
      "info": true,
      "autoWidth": true
    });
  });
</script>

<div class="row">

</div>