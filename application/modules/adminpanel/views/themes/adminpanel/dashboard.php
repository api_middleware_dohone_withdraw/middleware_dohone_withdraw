<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>

<?php $this->load->view('themes/'. Settings_model::$db_config['adminpanel_theme'] .'/partials/content_head.php'); ?>

<?php $this->load->view('generic/flash_error'); ?>


<div class="row text-center">
    <div class="col-sm-3">
        <div class="panel card bd-0">
            <div class="panel-body bg-primary">
                <h4 class="f300"><?php print $this->lang->line('dash_new_members_week'); ?></h4>
            </div>
            <div class="panel-body bg-white">
                <h3 class="mg-0 f900"><?php print $new_week->count; ?></h3>
            </div>
        </div>
    </div>
    <div class="col-sm-3">
        <div class="panel card bd-0">
            <div class="panel-body bg-primary">
                <h4 class="f300"><?php print $this->lang->line('dash_new_members_month'); ?></h4>
            </div>
            <div class="panel-body bg-white">
                <h3 class="mg-0 f900"><?php print $new_month->count; ?></h3>
            </div>
        </div>
    </div>
    <div class="col-sm-3">
        <div class="panel card bd-0">
            <div class="panel-body bg-primary">
                <h4 class="f300"><?php print $this->lang->line('dash_new_members_year'); ?></h4>
            </div>
            <div class="panel-body bg-white">
                <h3 class="mg-0 f900"><?php print $new_year->count; ?></h3>
            </div>
        </div>
    </div>
    <div class="col-sm-3">
        <div class="panel card bd-0">
            <div class="panel-body bg-primary">
                <h4><?php print $this->lang->line('dash_total_members'); ?></h4>
            </div>
            <div class="panel-body bg-white">
                <h3 class="mg-0 f900"><?php print $total_users; ?></h3>
            </div>
        </div>
    </div>
</div>

<h2><?php print $this->lang->line('dash_latest_members_title'); ?></h2>

<div class="panel card bd-0">
    <div class="panel-body bg-white">
        <div class="table-responsive">
            <table class="table table-hover">
                <thead>

                <tr>
                    <th>id</th>
                    <th><?php print $this->lang->line('dashboard_username'); ?></th>
                    <th><?php print $this->lang->line('dashboard_first_name'); ?></th>
                    <th><?php print $this->lang->line('dashboard_last_name'); ?></th>
                    <th><?php print $this->lang->line('dashboard_email_address'); ?></th>
                </tr>
                </thead>
                <tbody>

                <?php foreach($latest_members as $member) { ?>
                    <tr>
                        <td><?php print $member->user_id; ?></td>
                        <td><?php print $member->username; ?></td>
                        <td><?php print $member->first_name; ?></td>
                        <td><?php print $member->last_name; ?></td>
                        <td><?php print $member->email; ?></td>
                    </tr>
                <?php } ?>
                </tbody>
            </table>
        </div>
    </div>
</div>


<div class="row">

</div>


