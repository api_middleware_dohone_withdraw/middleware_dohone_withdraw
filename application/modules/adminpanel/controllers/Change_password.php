<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Change_password extends Admin_Controller {

    public function __construct()
    {   parent::__construct();
		/* Set current timezone */
        date_default_timezone_set("Asia/Kolkata");
		$this->load->helper('form');
        $this->load->library('form_validation');
    }

    public function index() {

       /* if (! self::check_permissions(4)) {
            redirect("/adminpanel/no_access");
        }*/

        // get all roles
        $this->template->set_breadcrumb('Change Password', 'adminpanel/change_password');
                
        $this->template->set_metadata('description', 'Change Password');        

        $this->quick_page_setup(Settings_model::$db_config['adminpanel_theme'], 'adminpanel', 'Change Password', 'change_password', 'header', 'footer');
    }


 

    /**
     *
     * Update: update password from post data.
     *
     */

    public function update_password() {

        /*if (! self::check_permissions(4)) {
            redirect("/adminpanel/no_access");
        }*/

        $this->form_validation->set_error_delimiters('<p>', '</p>');
        
        $this->form_validation->set_rules('password', $this->lang->line('add_member_password'), 'trim|required|max_length[255]|min_length[9]|is_valid_password');
        $this->form_validation->set_rules('password_confirm', $this->lang->line('add_member_password_confirm'), 'trim|required|max_length[255]|min_length[9]|matches[password]');

        

        if (!$this->form_validation->run()) {
            $this->session->set_flashdata('error', validation_errors());
            $this->session->set_flashdata($_POST);
            redirect('/adminpanel/change_password');
            exit();
        }
        
        // load membership model
        $this->load->model('adminpanel/change_password_model');
        if($return_array = $this->change_password_model->update_pass($this->input->post('password'))) {
            
            $this->session->set_flashdata('success', '<p>'. 'Password Updated Successfully' .'</p>');
        }else{
            $this->session->set_flashdata('error', '<p>'. 'Unable to update password' .'</p>');
        }
        redirect('/adminpanel/change_password');
    }

}