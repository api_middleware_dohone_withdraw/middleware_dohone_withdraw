<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Report_controller extends Admin_Controller {

    public function __construct()
    {
        parent::__construct();
        /* Set current timezone */
        date_default_timezone_set("Asia/Kolkata");
    
        $this->load->model('report_model');
        

    }
}