<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Crypto_log extends Admin_Controller {

    public function __construct()
    {   parent::__construct();
		/* Set current timezone */
        date_default_timezone_set("Asia/Kolkata");
		// pre-load
        $this->load->helper('form');
        $this->load->library('form_validation');
        $this->load->model('adminpanel/crypto_log_model');
        $this->template->set_breadcrumb('Deposit', 'adminpanel/crypto_log/deposit');

    }

    public function deposit($order_by = "user_id", $sort_order = "asc", $search = "all", $offset = 0) {
        
        if (! self::check_permissions(1)) {
            redirect("/adminpanel/no_access");
        }

        if (!is_numeric($offset)) {
            redirect('/adminpanel/crypto_log/deposit');
        }

        $data = $this->crypto_log_model->deposit();

        $content_data['data'] = $data;
        $content_data['order_by'] = $order_by; 
        $content_data['sort_order'] = $sort_order; 

        // set layout data
        $this->quick_page_setup(Settings_model::$db_config['adminpanel_theme'], 'adminpanel', $this->lang->line('crypto_deposit_log.php'), 'crypto_deposit_log.php', 'header', 'footer', '', $content_data);
    }

    public function withdraw($order_by = "user_id", $sort_order = "asc", $search = "all", $offset = 0) {
        
        if (! self::check_permissions(1)) {
            redirect("/adminpanel/no_access");
        }

        if (!is_numeric($offset)) {
            redirect('/adminpanel/crypto_log/deposit');
        }

        $data = $this->crypto_log_model->withdraw();

        $content_data['data'] = $data;
        $content_data['order_by'] = $order_by; 
        $content_data['sort_order'] = $sort_order; 

        // set layout data
        $this->quick_page_setup(Settings_model::$db_config['adminpanel_theme'], 'adminpanel', $this->lang->line('crypto_withdraw_log'), 'crypto_withdraw_log', 'header', 'footer', '', $content_data);
    }

    public function pay() {

        $cryptapi_coin_n_tokens = array(
            'btc' => 'Bitcoin',
            'bch' => 'Bitcoin Cash',
            'ltc' => 'Litecoin',
            'eth' => 'Ethereum',
            'usdt' => 'USDT (ERC-20)',
            'usdc' => 'USDC (ERC-20)',
            'busd' => 'BUSD (ERC-20)',
            'pax' => 'PAX (ERC-20)',
            'tusd' => 'TUSD (ERC-20)',
            'bnb' => 'BNB (ERC-20)',
            'link' => 'ChainLink (ERC-20)',
            'cro' => 'Crypto Coin (ERC-20)',
            'mkr' => 'Maker (ERC-20)',
            'nexo' => 'NEXO (ERC-20)',
            'bcz' => 'BECAZ (ERC-20)',
            'xmr' => 'Monero',
            'iota' => 'IOTA',
            'trx' => 'TRX(TRC-20)',
        );
        
        $COIN_MULTIPLIERS = [
            'btc' => 100000000,
            'bch' => 100000000,
            'ltc' => 100000000,
            'eth' => 1000000000000000000,
            'trx' => 1000000000000000000,
            'usdt' => 10000000,
            'usdc' => 10000000,
            'busd' => 10000000000000000000,
            'pax' => 10000000000000000000,
            'tusd' => 10000000000000000000,
            'bnb' => 10000000000000000000,
            'link' => 10000000000000000000,
            'cro' => 1000000000,
            'mkr' => 10000000000000000000,
            'nexo' => 10000000000000000000,
            'bcz' => 10000000000000000000,
            'iota' => 1000000,
            'trx' => 1000000,
            'xmr' => 1000000000000,
        ];

        $ERC_TOKENS_URI = [
            'usdt' => '0xdAC17F958D2ee523a2206206994597C13D831ec7',
            'usdc' => '0xA0b86991c6218b36c1d19D4a2e9Eb0cE3606eB48',
            'busd' => '0x4Fabb145d64652a948d72533023f6E7A623C7C53',
            'pax' => '0x8E870D67F660D95d5be530380D0eC0bd388289E1',
            'tusd' => '0x0000000000085d4780B73119b644AE5ecd22b376',
            'bnb' => '0xB8c77482e45F1F44dE1745F52C74426C631bDD52',
            'link' => '0x514910771AF9Ca656af840dff83E8264EcF986CA',
            'cro' => '0xA0b73E1Ff0B80914AB6fe0444E65848C4C34450b',
            'mkr' => '0x9f8F72aA9304c8B593d555F12eF6589cC3A579A2',
            'nexo' => '0xB62132e35a6c13ee1EE0f84dC5d40bad8d815206',
            'bcz' => '0x08399ab5eBBE96870B289754A7bD21E7EC8c6FCb',
        ];

        $data_id = $_GET['data_id'];
        $data_pay = $this->crypto_log_model->pay($data_id);
        $crypto_coin = $data_pay[0]->crypto_coin;
        $address_in = $data_pay[0]->coin_address_in;
        $crypto_value = $data_pay[0]->crypto_amount;
        $currency_uri_first = str_replace(' ','',strtolower($cryptapi_coin_n_tokens[$crypto_coin]));
        $currency_amount_in = 'amount';
        
        $show_crypto_coin = $crypto_coin;
        if ($show_crypto_coin == 'iota') $show_crypto_coin = 'miota';
    
        $COIN_MULTIPLIERS = $COIN_MULTIPLIERS;
    
        $qr_value = $crypto_value;
        if (in_array( $crypto_coin, array('eth', 'iota') ) ) $qr_value = ($crypto_value * $COIN_MULTIPLIERS[$crypto_coin]);
    
        if (in_array( $crypto_coin, array('usdt', 'usdc', 'busd', 'pax', 'tusd', 'bnb', 'link', 'cro', 'mkr', 'nexo', 'bcz' ) ) ) {
            $qr_value = ($crypto_value * $COIN_MULTIPLIERS[$crypto_coin]);
            $crypto_erc_address = $ERC_TOKENS_URI[$crypto_coin];
            $currency_uri_first = 'ether';
           }
        
        if( $crypto_coin == 'bch' ) {
            if (strpos($address_in, $currency_uri_first.':') !== false) {
                $address_in = str_replace($currency_uri_first.':','',$address_in);
            }
        }
        if( $crypto_coin == 'eth' ) {
            $qr_value = number_format($qr_value);
            if (strpos($qr_value, ',') !== false) {
                $qr_value = str_replace(',','',$qr_value);
            }
            $currency_amount_in = 'value';
        }
    
        if( $qr_value > 1000 ) {
    
            $qr_value = number_format($qr_value);
            if (strpos($qr_value, ',') !== false) {
                $qr_value = str_replace(',','',$qr_value);
            }
        }
        $sending_array = array(
            'address_in' => isset($address_in) ? $address_in : '',
            'currency_uri_first' => isset($currency_uri_first) ? $currency_uri_first : '',
            'currency_amount_in' => isset($currency_amount_in) ? $currency_amount_in : '',
            'crypto_erc_address' => isset($crypto_erc_address) ? $crypto_erc_address : '',
            'qr_value' => isset($qr_value) ? $qr_value : '',
            'crypto_value' => isset($crypto_value) ? $crypto_value : '',
            'show_crypto_coin' => isset($show_crypto_coin) ? $show_crypto_coin : '',
            'currency_symbol' => isset($data_pay[0]->currency) ? $data_pay[0]->currency : '',
            'total' => isset($data_pay[0]->amount) ? $data_pay[0]->amount : '',
        );
        echo json_encode($sending_array);
    }

    public function update_status() {

        $id = $_GET['id'];
        if( isset( $_GET['deposit'] ) && !empty( $_GET['deposit'] ) ) {
            if( $_GET['deposit'] == 'deposit' ) {
                $pay = $this->crypto_log_model->deposit_ret($id);
            }
            else {
                $pay = $this->crypto_log_model->pay($id);
            }
        } else {
            $pay = $this->crypto_log_model->pay($id);
        }
        $prefix_url = $pay[0]->prefix_url;
        $nonce = $pay[0]->nonce;
        $invoice_id = $pay[0]->order_id;
        $crypto_value = $pay[0]->crypto_amount;
        $success_url = "http://192.168.43.202/bitlogic/gateway/perfectmoney/success";
        $callback_url_with_nonce = "{$success_url}&invoice={$invoice_id}&nonce={$nonce}";
        $amount_paid = 0;

        $order_id = $invoice_id;
        $crypto_value = $crypto_value;
        $amount_paid = $amount_paid;
        $verify_callback_url = $callback_url_with_nonce;
        $prefix_url = $prefix_url;
        $cryptapi_nonce = $nonce;
        $prefix_url = str_replace("create","logs",$prefix_url);
        $verify_cb_url = "{$prefix_url}?callback={$verify_callback_url}";
        $payment_cb = $this->payment_confirmation( $verify_cb_url );
        $this->validate_payment( $payment_cb, $order_id, $cryptapi_nonce, $crypto_value, $amount_paid );
        echo json_encode('*ok*');
    }

    private function validate_payment( $payment_cb, $order_id, $nonce, $crypto_value, $amount_paid ) {

		$data = $this->process_callback( $payment_cb );
		
        $value_convert = $data['coin'];

        $paid = $amount_paid + $value_convert;

		if( isset( $data['confirmations'] ) && !empty( $data['confirmations'] ) ) {

			if( $data['confirmations'] >= 0 ) {

				if ($paid >= $crypto_value) {

                    $data = array(
                        'status' => 'success',
                        'payment_received_time' => date('Y-m-d H:i:s'),
                        'amount_remaining' => '',
                    );
                    $this->db->where('order_id', $order_id);

                    $this->db->update(DB_PREFIX .'crypto_deposit', $data);

					echo json_encode('payment_completed');
				}
				else
				{
					$amount_remaining = $crypto_value - $paid;

                    $payment_made_partial = array(
                        'status' => 'partial',
                        'amount_remaining' => $amount_remaining
                    );

                    $data = array(
                        'status' => 'partial',
                        'payment_received_time' => date('Y-m-d H:i:s'),
                        'crypto_amount_remaining' => $amount_remaining,
                    );
            
                    $this->db->where('order_id', $order_id);
                    $this->db->update(DB_PREFIX .'crypto_deposit', $data);

                    echo json_encode($payment_made_partial);
				}
			}
		}
	}

    private function process_callback($_get, $convert = false) {
		
		$params = [
            'address_in' => isset($_get['address_in']) ? $_get['address_in'] : '',
            'address_out' => isset($_get['address_out']) ? $_get['address_out'] : '',
            'callbacks' => isset($_get['callbacks'][0]) ? $_get['callbacks'][0] : '',
            'txid_in' => isset($_get['callbacks'][0]['txid_in']) ? $_get['callbacks'][0]['txid_in'] : null,
            'txid_out' => isset($_get['callbacks'][0]['txid_out']) ? $_get['callbacks'][0]['txid_out'] : null,
            'confirmations' => isset($_get['callbacks'][0]['confirmations']) ? $_get['callbacks'][0]['confirmations'] : null,
            'value' => isset( $_get['callbacks'][0]['value'] ) ? $_get['callbacks'][0]['value'] : '',
            'value_forwarded' => isset($_get['callbacks'][0]['value_forwarded']) ? $_get['callbacks'][0]['value_forwarded'] : null,
            'coin' => isset($_get['callbacks'][0]['value_coin']) ? $_get['callbacks'][0]['value_coin'] : null,
            'pending' => isset($_get['notify_pending']) ? $_get['notify_pending'] : false,
        ];

        return $params;
	}

    private function payment_confirmation( $verify_cb_url ) {

		$ca_curl = curl_init();
        curl_setopt( $ca_curl, CURLOPT_HEADER, false );
        curl_setopt( $ca_curl, CURLOPT_HTTPHEADER, array( 'Accept: application/json', 'Accept-Language: en_US' ) );
        curl_setopt( $ca_curl, CURLOPT_VERBOSE, 1 );
        curl_setopt( $ca_curl, CURLOPT_TIMEOUT, 30 );
        curl_setopt( $ca_curl, CURLOPT_URL, $verify_cb_url );
        curl_setopt( $ca_curl, CURLOPT_RETURNTRANSFER, true );
        curl_setopt( $ca_curl, CURLOPT_SSLVERSION, 6 );
        $ca_curl_response = curl_exec( $ca_curl );
        curl_close( $ca_curl );
		$ca_curl_response_array = json_decode( $ca_curl_response, true );
		return $ca_curl_response_array;
	}
}