<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dashboard extends Admin_Controller {

    public $content_data;
    public $loggeduser;

    public function __construct()
    {   parent::__construct();

        /* Set current timezone */
        date_default_timezone_set("Asia/Kolkata");


        // load dashboard data
        $this->load->model('adminpanel/dashboard_model');

        //code to send data for role
        $this->content_data['user'] = $this->session->userdata();
        $this->loggeduser = $this->content_data['user']["user_id"];

        /** This code basically use for select anganbari session **/
        if($this->uri->segment(3)!=NULL)
        {
            $this->content_data["uri_segment"] = $this->uri->segment(3);
        }
        else
        {
            $this->content_data["uri_segment"] = NULL;
        }

        /** controller **/
        $this->content_data["uri_segment1"] = $this->uri->segment(1);

        $this->content_data["uri_segment2"] = $this->uri->segment(2);
	}

    /*
    * function used on administrator dashboard where dashboard data showing user
    * details and name of the latest 5 user
    */
    public function index(){

        if (!self::check_permissions(7)) {
            redirect("/adminpanel/no_access");
        }

        $this->content_data['total_users'] = $this->dashboard_model->count_users();
        $this->content_data['new_week'] = $this->dashboard_model->count_users_this_week();
        $this->content_data['new_month'] = $this->dashboard_model->count_users_this_month();
        $this->content_data['new_year'] = $this->dashboard_model->count_users_this_year();
        $this->content_data['latest_members'] = $this->dashboard_model->get_latest_members();

        $this->quick_page_setup(Settings_model::$db_config['adminpanel_theme'], 'adminpanel', $this->lang->line('dashboard_title'), 'dashboard', 'header', 'footer', '', $this->content_data);
    }


    /*
    * function used on administrator dashboard where dashboard data showing user
    * details and name of the latest 5 user
    */
    public function dashboard2(){

        if (!self::check_permissions(7)) {
            redirect("/adminpanel/no_access");
        }

        $this->content_data['total_users'] = $this->dashboard_model->count_users();
        $this->content_data['new_week'] = $this->dashboard_model->count_users_this_week();
        $this->content_data['new_month'] = $this->dashboard_model->count_users_this_month();
        $this->content_data['new_year'] = $this->dashboard_model->count_users_this_year();
        $this->content_data['latest_members'] = $this->dashboard_model->get_latest_members();

        $this->quick_page_setup(Settings_model::$db_config['adminpanel_theme'], 'adminpanel', $this->lang->line('dashboard_title'), 'rptDashBoard', 'header', 'footer', '', $this->content_data);
    }


}