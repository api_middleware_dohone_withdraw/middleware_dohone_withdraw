<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Home extends Admin_Controller {

    public function __construct() {   parent::__construct();
		/* Set current timezone */
        date_default_timezone_set("Asia/Kolkata");
		}

    public function index() {
        // dashboard is our home page
        redirect('adminpanel/dashboard');
    }

    function get_state_map() {

        //check if its an ajax request, exit if not
        if (!$this->input->is_ajax_request()) {
            exit("request should be ajax");
        }

        //check we have role id
        if ($this->input->post('roleid')) {
            // get state data model
            $this->load->model('adminpanel/list_states_all_model');

            $role_id = $this->input->post('roleid');

            $result = $this->list_states_all_model->get_district();

            if ($result) {

                $res = array("valid" => "true", "data" => $result);
                echo json_encode($res);
            } else {

                $res = array("valid" => "false");
                echo json_encode($res);
            }
        } elseif ($this->input->post('role_id') && $this->input->post('mapid')) {

            // get state data model
            $this->load->model('adminpanel/list_states_all_model');


            if ($this->input->post('role_id') == 4) {

                $result = $this->list_states_all_model->get_block($this->input->post('mapid'));
            } elseif ($this->input->post('role_id') == 5) {

                $result = $this->list_states_all_model->get_panchayat($this->input->post('mapid'));
            }


            if ($result) {

                $res = array("valid" => "true", "data" => $result);
                echo json_encode($res);
            } else {

                $res = array("valid" => "false");
                echo json_encode($res);
            }
        }
    }

}
