<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Test_controller extends Admin_Controller {

    public $content_data;
    public $loggeduser;

    public function __construct()
    {
        parent::__construct();

        /* Set current timezone */
        date_default_timezone_set("Asia/Kolkata");

        $this->load->helper('form','url');
        $this->load->library('form_validation');
        $this->load->library('adhar_validation');
        $this->load->model('test_model');
        $this->template->set_breadcrumb('Dashboard', 'master/lsdashboard');

        //code to send data for role
        $this->content_data['user'] = $this->session->userdata();
        $this->loggeduser = $this->content_data['user']["user_id"];

        /** This code basically use for select anganbari session **/
        if($this->uri->segment(3)!=NULL)
        {
            $this->content_data["uri_segment"] = $this->uri->segment(3);
        }
        else
        {
            $this->content_data["uri_segment"] = NULL;
        }

        /** controller **/
        $this->content_data["uri_segment1"] = $this->uri->segment(1);
        /** function **/
        $this->content_data["uri_segment2"] = $this->uri->segment(2);
    }

    public function index() {

        
    }

    /**
    *
    * get data and return one column only
    *
    *
    **/
    public function retAWCName($col1, $col2)
    {
        $data = $this->test_model->get_array($col1, $col2)->row();

        if(isset($data))
        {
            return $data->name;
        }
        else
        {
            return "NA";
        }
    }
}
