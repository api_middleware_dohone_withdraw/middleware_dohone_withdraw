<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Master_page_entry extends Admin_Controller {

    public function __construct()
    {   parent::__construct();
		/* Set current timezone */
        date_default_timezone_set("Asia/Kolkata");
		$this->load->helper('form');
        $this->load->library('form_validation');
        $this->load->model('master_page_entry_model');

        if (! self::check_permissions(1)) {
            redirect("/adminpanel/no_access");
        }
    }

    public function index() {

        if (! self::check_permissions(1)) {
            redirect("/adminpanel/no_access");
        }

        // get all menu        
        $this->load->model('add_menu_model');
        $content_data['menus'] = $this->add_menu_model->get_menu();

        // get Notice type  
        $content_data['notices'] = $this->master_page_entry_model->get_notice_typ();        

        // get Upload type  
        $content_data['upload_types'] = $this->master_page_entry_model->get_upload_typ();

        // get all roles
        $content_data['roles'] = $this->rbac_model->get_roles();
        
        $this->template->set_metadata('description', 'Master Page Entry');
        $this->template->set_breadcrumb('Master Page Entry', 'adminpanel/master_page_entry');

        $this->quick_page_setup(Settings_model::$db_config['adminpanel_theme'], 'adminpanel', $this->lang->line('master_page_entry'), 'master_page_entry', 'header', 'footer', '', $content_data);
    }



    function create_page() {

    //check if its an ajax request, exit if not
        if (!$this->input->is_ajax_request()) {
            exit("request should be ajax");
        }

        if (isset($_POST)) {

            $page_upload_type = $this->input->post('page_upload_type');
            $page_role_id = $this->input->post('page_role_id');
            $page_active = $this->input->post('page_active');
            $page_title = $this->input->post('page_title');
            $page_header = $this->input->post('page_header');
            $page_content = $this->input->post('page_content');
            $page_menu_id = $this->input->post('page_menu_id');
            $page_start_date = strtotime($this->input->post('page_start_date'));
            $page_end_date = strtotime($this->input->post('page_end_date'));
            $created_on = date('Y-m-d H:i:s');
            $ip_address = $this->input->ip_address();

            if ($page_upload_type == '' || $page_role_id == '' || $page_active == '') 
            {
                return false;
            }
            

            if ($page_upload_type==1) {

                if ($page_title == '' || $page_header == '' || $page_content == '' || $page_menu_id == '')
                {
                    return false;
                }

                $data = array(
                    'page_upload_type' => $page_upload_type ,
                    'page_title' =>$page_title ,
                    'page_header' =>$page_header ,
                    'page_content' =>$page_content ,
                    'page_menu_id' =>$page_menu_id ,
                    'page_role_id' =>$page_role_id  ,
                    'page_active' =>$page_active  ,
                    'created_on' =>$created_on  ,
                    'ip_address' =>$ip_address  
                    );  

    // Save page content to db
                $result = $this->master_page_entry_model->create_page($data);


                if ($result) {

                    $res = array("valid" => "true");
                    echo json_encode($res);

                } else {

                    $res = array("valid" => "false");
                    echo json_encode($res);
                }

            } else if ($page_upload_type==2) {

                if (isset($_FILES) && !empty($_FILES)) {

                    $filename=explode(".",$_FILES["images"]["name"]);

                    if ($_FILES["images"]["error"] == UPLOAD_ERR_OK) {

                        $chk_file_type = strtolower($_FILES['images']['type']);

                    }

                    if ($chk_file_type == 'application/pdf') 
                    {

                    if ($page_title == '' || $page_header == '' || $page_menu_id == '')
                    {
                        return false;
                    }

                        $data = array(
                            'page_upload_type' =>$page_upload_type ,
                            'page_title' =>$page_title ,
                            'page_header' =>$page_header ,
                            'page_file' =>base64_encode(file_get_contents($_FILES['images']['tmp_name'])) ,
                            'page_menu_id' =>$page_menu_id ,
                            'page_role_id' =>$page_role_id  ,
                            'page_active' =>$page_active ,
                            'created_on' =>$created_on  ,
                            'ip_address' =>$ip_address   );

            // Save page content to db
                        $result = $this->master_page_entry_model->create_page($data);

                        if ($result) {

                            $res = array("valid" => "true");
                            echo json_encode($res);

                        } else {

                            $res = array("valid" => "false");
                            echo json_encode($res);
                        }
                    }


                } else {

                    return 'false';
                }

            } else if ($page_upload_type==3) {

                if (isset($_FILES) && !empty($_FILES)) {

                    $filename=explode(".",$_FILES["images"]["name"]);

                    if ($_FILES["images"]["error"] == UPLOAD_ERR_OK) {

                        $chk_file_type = strtolower($_FILES['images']['type']);

                    }

                    if ($chk_file_type = 'application/jpeg' || $chk_file_type = 'application/jpg' || $chk_file_type = 'application/png'|| $chk_file_type = 'application/gif') {

                        $image_data = addslashes($_FILES['images']['tmp_name']);
                        $image_name = addslashes($_FILES['images']['name']);
                        $image_data = file_get_contents($image_data);
                        $image_data = base64_encode($image_data);


                    if ($page_title == '' || $page_header == '' || $page_menu_id == '')
                    {
                        return false;
                    }

                        $data = array(
                            'page_upload_type' =>$page_upload_type ,
                            'page_title' =>$page_title ,
                            'page_header' =>$page_header ,
                            'page_file_name' => $image_name ,
                            'page_file' => $image_data ,
                            'page_menu_id' =>$page_menu_id ,
                            'page_role_id' =>$page_role_id  ,
                            'page_active' =>$page_active,
                            'created_on' =>$created_on  ,
                            'ip_address' =>$ip_address   );

                        // Save page content to db
                        $result = $this->master_page_entry_model->create_page($data);

                        if ($result) {

                            $res = array("valid" => "true");
                            echo json_encode($res);

                        } else {

                            $res = array("valid" => "false");
                            echo json_encode($res);
                        }
                    }
                } else {
                    return 'false';
                }

            } else if ($page_upload_type==4) {

                if (isset($_FILES) && !empty($_FILES)) {

                    $filename=explode(".",$_FILES["images"]["name"]);

                    if ($_FILES["images"]["error"] == UPLOAD_ERR_OK) {

                        $chk_file_type = strtolower($_FILES['images']['type']);

                    }

                    if ($chk_file_type == 'application/pdf') 
                    {

                    if ($page_title == '' || $page_header == '' || $page_menu_id == '' || $page_end_date == '' || $page_end_date == '')
                    {
                        return false;
                    }

                        $data = array(
                            'page_upload_type' =>$page_upload_type ,
                            'page_title' =>$page_title ,
                            'page_header' =>$page_header ,
                            'page_file' =>base64_encode(file_get_contents($_FILES['images']['tmp_name'])) ,
                            'page_menu_id' =>$page_menu_id ,
                            'page_role_id' =>$page_menu_id  ,
                            'page_start_date' => $page_start_date  ,
                            'page_end_date' => $page_end_date  ,
                            'page_active' =>$page_active ,
                            'created_on' =>$created_on  ,
                            'ip_address' =>$ip_address   );

            // Save page content to db
                        $result = $this->master_page_entry_model->create_page($data);

                        if ($result) {

                            $res = array("valid" => "true");
                            echo json_encode($res);

                        } else {

                            $res = array("valid" => "false");
                            echo json_encode($res);
                        }
                    }


                } else {

                    return 'false';
                }

            } else if ($page_upload_type==5) {

                $notice_id = $this->input->post('page_notice_id') ;

                if ($notice_id == 1 || $notice_id ==2 || $notice_id ==3) {

                    if (isset($_FILES) && !empty($_FILES)) {

                        $filename=explode(".",$_FILES["images"]["name"]);

                        if ($_FILES["images"]["error"] == UPLOAD_ERR_OK) {

                            $chk_file_type = strtolower($_FILES['images']['type']);

                        }

                        if ($chk_file_type == 'application/pdf') 
                        {
                            if ($page_title == '' || $page_header == '' || $notice_id == '' )
                            {
                                return false;
                            }

                            $data = array(
                                'page_upload_type' =>$page_upload_type ,
                                'page_title' =>$page_title ,
                                'page_header' =>$page_header ,
                                'page_file' =>base64_encode(file_get_contents($_FILES['images']['tmp_name'])) ,
                                'page_notice_type' =>$notice_id ,
                                'page_role_id' =>$page_role_id  ,
                                'page_active' =>$page_active ,
                                'created_on' =>$created_on  ,
                                'ip_address' =>$ip_address   );

            // Save page content to db
                            $result = $this->master_page_entry_model->create_page($data);

                            if ($result) {

                                $res = array("valid" => "true");
                                echo json_encode($res);

                            } else {

                                $res = array("valid" => "false");
                                echo json_encode($res);
                            }
                        }


                    }
                } elseif ($notice_id == 4) {

                    if ($page_title == '' || $notice_id == '' )
                            {
                                return false;
                            }

                    $data = array(
                        'page_upload_type' =>$page_upload_type ,
                        'page_role_id' =>$page_role_id  ,
                        'page_notice_type' => $notice_id  ,
                        'page_title' =>$page_title ,
                        'page_active' =>$page_active,
                        'created_on' =>$created_on  ,
                        'ip_address' =>$ip_address   );

                // Save page content to db
                    $result = $this->master_page_entry_model->create_page($data);


                    if ($result) {

                        $res = array("valid" => "true");
                        echo json_encode($res);

                    } else {

                        $res = array("valid" => "false");
                        echo json_encode($res);
                    }
                }

                else {

                    return 'false';
                }

            }        

        }

    }




    public function view_page() {  

     if (! self::check_permissions(1)) {
        redirect("/adminpanel/no_access");
    }     

        //code to send parameter to datatable
    $id = $this->uri->segment(4);
    $tablename='master_page_entry';
    $column='page_id, page_title, page_header, page_content';
    $where="page_id=".$id;
    $order_by=null;
    $result = $this->master_page_entry_model->get_page_data($tablename, $column,$where, $order_by, null, 1000);

    foreach($result->result() as $results) { 
        $p_id = $results->page_id; 
        $p_title = $results->page_title;
        $p_header = $results->page_header;
        $p_content = $results->page_content;
    }

    $content_data['data'] =  array('p_id' => $p_id,
        'p_title' => $p_title,
        'p_header' => $p_header,
        'p_content' => $p_content );



          //code to send parameter to page
    $this->quick_page_setup(Settings_model::$db_config['adminpanel_theme'], 'adminpanel', $p_title, 'master_page_view', 'header', 'footer', '', $content_data);
}



}