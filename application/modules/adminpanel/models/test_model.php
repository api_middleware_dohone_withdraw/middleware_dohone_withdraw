<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Test_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    /*Code to retrieve list as associative array*/
    public function get_array($where_val, $where_val2=NULL) {
        $table = 'table_name';
        $columns = 'column1,column2,column3';
        $where = array('column_name'=>$where_val, 'active'=>1);

        #where clause on some condition
        if(isset($awccode))
            $where = array('column_name'=>$where_val, 'code'=>$where_val2, 'active'=>1);

        $orderBy = array('column_name'=>'asc');
        return $this->sql_helper->getArray($table, $columns, $where, $orderBy);
    }

    /*Code to retrieve list as object*/
    public function get_result($where_val, $where_val2=NULL) {
        $table = 'table_name';
        $columns = 'column1,column2,column3';
        $where = array('column_name'=>$where_val, 'active'=>1);

        #where clause on some condition
        if(isset($awccode))
            $where = array('column_name'=>$where_val, 'code'=>$where_val2, 'active'=>1);

        $orderBy = array('column_name'=>'asc');
        return $this->sql_helper->getResult($table, $columns, $where, $orderBy);
    }

    /*Code to retrieve single column as string*/
    public function get_onetext($where_val)
    {
        $table = "table_name";
        $retcolumn = "date_format(interval 1 day + date_column_name, '%d-%m-%Y')"; #example if any condition in query
        $where = "active=1 and column1='".$where_val."' order by id desc";
        return $this->sql_helper->getText($table, $retcolumn, $where);
    }
    
    /*Code to retrieve single column as string*/
    public function get_onetext2($where_val, $where_val2)
    {
        $table = "table_name";
        $retcolumn = "ret_columnName";
        $where = array("wherecolumn_name"=>$where_val, "wherecolumnname2"=>$where_val2);
        return $this->sql_helper->retText($table, $retcolumn, $where);
    }
    
    /*Code with join as example - return list of object*/
    public function get_service() {
        $table = 'service_master s';
        $columns = 's.id, s.service_name, s.service_type_id, t.service_type, s.acronym';
        $join = array('servicetype_master t'=>"s.service_type_id=t.id");
        $where = array('s.active'=>1, "s.id!=3"=>NULL);
        $orderby = array('s.service_type_id'=>'asc');
        return $this->sql_helper->getJoinResult($table, $columns, $join, $where, $orderby);
    }
    
    //insert data
    public function insert()
    {
        $table = "table_name";
        $columnArray = array(
            "col1"=>"value",
            "col2"=>"value"
        );
        $this->sql_helper->insert_data($table, $columnArray);
    }
    
    //update data
    public function update_anganbari($columnArray, $whereArray)
    {
        $table = "awc_master";
        $columnArray = array(
            "col1"=>"value",
            "col2"=>"value"
        );
        $where = array("wherecolumn_name"=>$where_val, "wherecolumnname2"=>$where_val2);
        $this->sql_helper->update_data($table, $columnArray, $whereArray);
    }
    

    //insert and update with condition
    public function awcdetails_entry($emptype_id, $avaibility, $awccode)
    {
        $table="table_name";
        
        $columnArray = array(
        'col1' => $emptype_id,
        'col2' => $avaibility,
        'entry_date' => date("Y-m-d"),
        'col3' => $col3
        );

        $whereArray = array(
        'wcol1' => $emptype_id,
        'wcol2' => $awccode
        );

        if($this->sql_helper->getCount($table, $whereArray)==0)
            $this->sql_helper->insert_data($table, $columnArray);
        else
            $this->sql_helper->update_data($table, $columnArray, $whereArray);
    }
}