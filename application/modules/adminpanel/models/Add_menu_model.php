<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Add_menu_model extends CI_Model {

    public function __construct() {
        parent::__construct();
        }


    /**
     *
     * get_menu
     *
     * @return mixed
     *
     */

    public function get_menu() {
        $this->db->select('menu_id as menu_id, title as menu_name')->from(DB_PREFIX .'dynamic_menu');
        $q = $this->db->get();
        if ($q->num_rows() > 0) {
            return $q->result();
        }
        return false;
    }

    /**
     *
     * create_menu
     *
     * @param string $menu_name
     * @param string $url
     * @param string $position
     * @param bool $parent_id
     * @param string $is_parent
     * @param bool $show_menu
     * @return mixed
     *
     */

    public function create_menu($menu_name, $url, $is_parent, $parent_id, $show_menu) {

        
        $data = array(
            'title' => $menu_name,
            'link_type' => 'page',
            'url' => $url,
            'dyn_group_id' => 1,
            'position' => '0',
            'parent_id' => $parent_id,
            'is_parent' => $is_parent,
            'show_menu' => $show_menu,
            'created_on' => date('Y-m-d H:i:s'),
            'ip_address' => $this->input->ip_address()
        );



        $this->db->trans_start();

        $this->db->insert(DB_PREFIX .'dynamic_menu', $data);

        $last_id = $this->db->insert_id();

        $last_id_n = $this->db->count_all('dynamic_menu');;

        $this->db->where('id', $last_id);
        $this->db->update(DB_PREFIX .'dynamic_menu',
            array('menu_id' => $last_id_n
            )
        );

        $this->db->trans_complete();

        if (! $this->db->trans_status() === false)
        {
            return true;
        }

        return false;
    }

    /**
     *
     * create_menu
     *
     * @param string $menu_name
     * @param string $url
     * @param string $position
     * @param bool $parent_id
     * @param string $is_parent
     * @param bool $show_menu
     * @return mixed
     *
     */

    public function update_menu($menu_id, $menu_name, $url,  $is_parent, $parent_id, $show_menu) {

        
        $data = array(
            'title' => $menu_name,
            'link_type' => 'page',
            'url' => $url,
            'dyn_group_id' => 1,
            'position' => '0',
            'parent_id' => $parent_id,
            'is_parent' => $is_parent,
            'show_menu' => $show_menu,
            'created_on' => date('Y-m-d H:i:s'),
            'ip_address' => $this->input->ip_address()
        );

       
        $this->db->trans_start();

        $this->db->where('menu_id', $menu_id);
        $this->db->update(DB_PREFIX .'dynamic_menu', $data);

        $this->db->trans_complete();

        if (! $this->db->trans_status() === false)
        {
            return true;
        }

        return false;
    }


    /**
     *
     * get_menu_data_list
     *
     * @param string $tablename ,$column,$where, $order_by=null 
     * @param string $sort_order = "asc", $limit = 0, $offset = 0 
     * @return mixed
     *
     */

   /* Code for retreieve data in list table */
    public function get_menu_data_list($tablename, $column, $order_by=null, $sort_order = "asc", $limit = 0, $offset = 0)
    {
        $fields = $this->db->list_fields(DB_PREFIX .$tablename);

        $this->db->select($column);
        $this->db->from(DB_PREFIX .$tablename);
        
        if(isset($where))
            $this->db->where($where);

        if(isset($order_by))
            $this->db->order_by($order_by, $sort_order);

        $this->db->limit($limit, $offset);

        $q = $this->db->get();
        
        if($q->num_rows() > 0) {
            return $q;
        }

        return false;
    }


    /* Code for retreieve data in list table */
    public function get_menu_data($tablename, $column, $where, $order_by=null, $sort_order = "asc", $limit = 0, $offset = 0)
    {
        $fields = $this->db->list_fields(DB_PREFIX .$tablename);

        $this->db->select($column);
        $this->db->from(DB_PREFIX .$tablename);
        
        if(isset($where))
            $this->db->where($where);

        if(isset($order_by))
            $this->db->order_by($order_by, $sort_order);

        $this->db->limit($limit, $offset);

        $q = $this->db->get();
        
        if($q->num_rows() > 0) {
            return $q;
        }

        return false;
    }
}

