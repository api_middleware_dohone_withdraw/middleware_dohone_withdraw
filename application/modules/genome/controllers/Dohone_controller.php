<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require 'vendor/dohone/payment/config/config.php';
require 'vendor/dohone/payment/src/PayOut/Transfer/DohoneTransfer.php';

class Dohone_controller extends Gateway_Controller {

    public $content_data;
    //public $merchant_code = 'GD216E19969793310088401'; // old merchant code
    public $merchant_code = 'MF216E21681053825080002'; // new merchant code
    public $hash_uniq_code = '8A36DB3A56A8D27135B56FAB10AD12';
    //public $hash_code_withdraw = '';
    public function __construct() {

        parent::__construct();
		/* Set current timezone */
        date_default_timezone_set("Asia/Kolkata");
        $this->load->helper('form');
        $this->load->library('form_validation');
        $this->load->model('dohone_logs_model');
        $this->load->helper('url');
        $this->content_data["endPage"] = base_url()."index.php/dohone/success";
        $this->content_data["cancelPage"] = base_url()."index.php/dohone/decline";
        $this->content_data["cancelPage"] = base_url()."index.php/dohone/cancel";
        $this->content_data["notifyPage"] = base_url()."index.php/dohone/notify";
    }

    public function deposit() {
        $post = $this->input->post();
        $success = $this->content_data["endPage"]; //site_url('genome/dohone_controller/success');
        $notify = $this->content_data["notifyPage"]; //site_url('genome/dohone_controller/notify');
        $cancel = $this->content_data["cancelPage"]; //site_url('genome/dohone_controller/cancel');*/

        $this->content_data['username'] = $post["username"];
        $this->content_data['telephone'] = $post["telephone"];
        $this->content_data['email'] = $post["email"];
        $this->content_data['merchant_code'] = $this->merchant_code;
        $this->content_data['uniqueTransactionId'] = $post["uniqueTransactionId"];
        $this->content_data['amount'] = $post["amount"];
        $this->content_data['currency'] = $post["currency"];


        $this->dohone_logs_model->insert_deposit_log( $post["username"], $post["telephone"], $post["email"], $this->merchant_code, $post["uniqueTransactionId"], $post["amount"], $post["currency"], $success, $notify, $cancel, $this->hash_uniq_code );
        $this->quick_page_setup(Settings_model::$db_config['genome_theme'], '', 'Dohone Checkout', 'dohone_checkout', 'header', 'footer', '', $this->content_data);
    }



        public function withdrawal() {

        $this->form_validation->set_rules('username', 'Dohone Withdraw Username', 'required|alpha_numeric');
        $this->form_validation->set_rules('currency', 'Dohone Withdraw Currency', 'required|alpha_numeric');
        $this->form_validation->set_rules('uniqueTransactionId', 'Dohone Withdraw Transaction ID', 'required|alpha_numeric');
        $this->form_validation->set_rules('email', 'Dohone Withdraw Email', 'required|valid_email');
        $this->form_validation->set_rules('telephone','Dohone Withdraw Telephone Number','required|numeric');
        $this->form_validation->set_rules('amount', 'Dohone Withdraw Amount', 'required|numeric');
        $this->form_validation->set_rules('receiver_name', 'Dohone Withdraw Receiver Name', 'required|alpha_numeric_spaces');
        $this->form_validation->set_rules('receiver_country', 'Dohone Withdraw Receiver Country', 'required|alpha_numeric_spaces');
        $this->form_validation->set_rules('receiver_city', 'Dohone Withdraw Receiver City', 'required|alpha_numeric_spaces');

        if ($this->form_validation->run() == FALSE)
        {

        //echo 'entered_first_block'; exit;

        $reqData = $this->input->get();

        $this->content_data["cryptapi_coin_options"] = array(
            'btc' => 'Bitcoin',
            'bch' => 'Bitcoin Cash',
            'ltc' => 'Litecoin',
            'eth' => 'Ethereum',
            'usdt' => 'USDT (ERC-20)',
            'usdc' => 'USDC (ERC-20)',
            'busd' => 'BUSD (ERC-20)',
            'pax' => 'PAX (ERC-20)',
            'tusd' => 'TUSD (ERC-20)',
            'bnb' => 'BNB (ERC-20)',
            'link' => 'ChainLink (ERC-20)',
            'cro' => 'Crypto Coin (ERC-20)',
            'mkr' => 'Maker (ERC-20)',
            'nexo' => 'NEXO (ERC-20)',
            'bcz' => 'BECAZ (ERC-20)',
            'xmr' => 'Monero',
            'iota' => 'IOTA',
            'trx' => 'TRX(TRC-20)',
        );

        $this->content_data["uniqueTransactionId"] = time();
        $this->content_data["username"] = isset($reqData["username"])? $reqData["username"]:"";
        $this->content_data["currency"] = isset($reqData["currency"])? $reqData["currency"]:"";
        if ($this->content_data['username'] == '') {

            $this->content_data["username"] = $this->input->post('username');
        }
        if ($this->content_data["currency"] == '') {

            $this->content_data["currency"] = $this->input->post('currency');
        }

        $this->content_data['form_validation_errors'] = 'contains validations errors';

        $this->quick_page_setup(Settings_model::$db_config['genome_theme'], 'main', 'Genome Home', 'withdraw_dashboard', 'header', 'footer', '', $this->content_data);

        }
        else
        {

        //echo 'entered_second_block'; exit;
        $post = $this->input->post();

        $this->content_data['username'] = $post["username"];
        $this->content_data['uniqueTransactionId'] = $post["uniqueTransactionId"];
        $this->content_data['currency'] = $post["currency"];
        $this->content_data['mobile'] = $post["telephone"];
        $this->content_data['email'] = $post["email"];
        $this->content_data['amount'] = $post["amount"];
        $this->content_data['receiver_name'] = $post["receiver_name"];
        $this->content_data['receiver_city'] = $post["receiver_city"];
        $this->content_data['receiver_country'] = $post["receiver_country"];
        $this->content_data['notify_url'] = site_url('genome/dohone_controller/notify_withdraw');
        $this->content_data['hash_uniq_code'] = $this->hash_uniq_code;
        $this->content_data['merchant_token'] = $this->merchant_code;
        $account = '695100003';
        $mode = 1;
        $hash = md5($account.$mode.$this->content_data['amount'].$this->content_data['currency'].$this->content_data['uniqueTransactionId'].$this->content_data['hash_uniq_code']);
        $this->content_data['hash'] = $hash;

        //$account = '695100003';
        $destination = $this->content_data['mobile']; //exit;

        $amount = $this->content_data['amount'];
        $devise = $this->content_data['currency'];
        $nameDest = $this->content_data['receiver_name'];
        $ville = $this->content_data['receiver_city'];
        $pays = $this->content_data['receiver_country'];
        $hash = $this->content_data['hash'];
        $notifyUrl = $this->content_data['notify_url'];

        //exit;

        $data_withdraw_dohone = array(

            'userid' => $this->content_data['username'],
            'transaction_id' => $this->content_data['uniqueTransactionId'],
            'amount' => $this->content_data['amount'],
            'currency' => $this->content_data['currency'],
            'email' => $this->content_data['email'],
            'notify_url' => $this->content_data['notify_url'],
            'unique_hash_code' => $this->content_data['hash_uniq_code'],
            'merchant_token' => $this->content_data['merchant_token'],
            'mode' => $mode,
            'dohone_payout_hash' => $hash,
            'payer_account_telephone_no' => $account,
            'destination_acc_tele_number' => $destination,
            'receiver_name' => $nameDest,
            'receiver_city' => $ville,
            'receiver_country' => $pays,
            'response_text' => '',
            'transaction_status' => '',
            'entry_date' => date('d-m-Y H:i:s'),
            'entry_ip' => $this->getUserIpAddr()

        );

        $transaction_log_id = $this->dohone_logs_model->insert_withdraw_log($data_withdraw_dohone);
        if ($transaction_log_id != '') {

        $params = "account={$account}&destination={$destination}&mode={$mode}&amount={$amount}&devise={$devise}&nameDest={$nameDest}&ville={$ville}&pays={$pays}&transID={$this->content_data['uniqueTransactionId']}&hash={$hash}&notifyUrl={$notifyUrl}";

        $linkSource = "https://www.my-dohone.com/dohone-sandbox/transfert?".$params;
        $curl = curl_init ($linkSource);
        curl_setopt ($curl, CURLOPT_FAILONERROR, TRUE);
        curl_setopt ($curl, CURLOPT_FOLLOWLOCATION, TRUE);
        curl_setopt ($curl, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt ($curl, CURLOPT_SSL_VERIFYHOST, FALSE);
        curl_setopt ($curl, CURLOPT_SSL_VERIFYPEER, FALSE);
        $result = curl_exec ($curl);
        $result_array = explode('/', $result);
        $result_msg = trim($result_array[0]);
        if ($result_msg == 'OK') {

            $this->content_data["transactionId"] = $this->content_data['uniqueTransactionId'];
            $this->content_data["uniqueUserId"] = $this->content_data['username'];
            $this->content_data["totalAmount"] = $this->content_data['amount'];
            $this->content_data["currency"] = $this->content_data['currency'];
            $this->content_data["message"] = "SUCCESS";

            //Update success response
            $columnArray = array(
              "response_text"=>json_encode($result),
              "transaction_status"=>"SUCCESS"
            );
            $this->dohone_logs_model->update_deposit($columnArray,$transaction_log_id);

            $this->quick_page_setup(Settings_model::$db_config['genome_theme'], '', 'Dohone Withdrawal Success', 'success_withdraw', 'header', 'footer', '', $this->content_data);

        }
        //}
        else{

            $this->content_data["transactionId"] = $this->content_data['uniqueTransactionId'];
            $this->content_data["uniqueUserId"] = $this->content_data['username'];
            $this->content_data["totalAmount"] = $this->content_data['amount'];
            $this->content_data["currency"] = $this->content_data['currency'];
            $this->content_data["message"] = "SORRY TRANSFER FAILED !!";

            //Update success response
            $columnArray = array(
              "response_text" => $result,
              "transaction_status" => "FAILED"
            );
            $this->dohone_logs_model->update_deposit($columnArray,$transaction_log_id);

            $this->quick_page_setup(Settings_model::$db_config['genome_theme'], '', 'Dohone Withdrawal Failed', 'error', 'header', 'footer', '', $this->content_data);
        }
        }

        }
    }

        public function getUserIpAddr(){

            if(!empty($_SERVER['HTTP_CLIENT_IP'])){
                //ip from share internet
                $ip = $_SERVER['HTTP_CLIENT_IP'];
            }elseif(!empty($_SERVER['HTTP_X_FORWARDED_FOR'])){
                //ip pass from proxy
                $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
            }else{
                $ip = $_SERVER['REMOTE_ADDR'];
            }
            return $ip;
        }



    public function success() {

        if( isset($_GET) && !empty($_GET) ) {
            $r_uniqueTransactionId = $_GET['rI'];
            $r_amount_paid = (float)$_GET['rMt'];
            $r_currency = $_GET['rDvs'];
            $r_txn_reference = $_GET['idReqDoh'];
            $r_merchant_code = $_GET['rH'];
            $r_payment_mode = $_GET['mode'];
            $r_hash_key = $_GET['hash'];

            if( $r_txn_reference ) {
                $params = "cmd=verify&rI={$r_uniqueTransactionId}&rMt={$r_amount_paid}&idReqDoh={$r_txn_reference}&rDvs={$r_currency}";
                $linkSource = "https://www.my-dohone.com/dohone/pay?".$params;
                $curl = curl_init ($linkSource);
                curl_setopt ($curl, CURLOPT_FAILONERROR, TRUE);
                curl_setopt ($curl, CURLOPT_FOLLOWLOCATION, TRUE);
                curl_setopt ($curl, CURLOPT_RETURNTRANSFER, TRUE);
                curl_setopt ($curl, CURLOPT_SSL_VERIFYHOST, FALSE);
                curl_setopt ($curl, CURLOPT_SSL_VERIFYPEER, FALSE);
                $result = curl_exec ($curl);
                if ($result == 'OK') {
                    $verified = $this->dohone_logs_model->verify_transaction($r_uniqueTransactionId);
                    if( $verified == 'ok' ) {
                        $amount_verified = $this->dohone_logs_model->validate_amount($r_uniqueTransactionId, $r_amount_paid, $r_currency);
                        if( $amount_verified == 'ok' ) {
                            $merchant_verified = $this->dohone_logs_model->validate_merchant($r_uniqueTransactionId, $r_merchant_code);
                            if( $merchant_verified == 'ok' ) {
                                $this->dohone_logs_model->save_notify_data($r_uniqueTransactionId, $r_txn_reference, $r_payment_mode, $r_hash_key);
                                $uniqueUserId = $this->dohone_logs_model->get_user_id($r_uniqueTransactionId);
                                $this->content_data["transactionId"] = $r_uniqueTransactionId;
                                $this->content_data["uniqueUserId"] = $uniqueUserId;
                                $this->content_data["totalAmount"] = $r_amount_paid;
                                $this->content_data["currency"] = $r_currency;
                                $this->content_data["message"] = 'Your Amount has been received and transferred to Merchant';
                                $this->quick_page_setup(Settings_model::$db_config['genome_theme'], '', 'Dohone Success', 'success', 'header', 'footer', '', $this->content_data);
                            }
                        } else {
                            echo 'Amount is paid in wrong currency or Low amount paid';
                        }
                    } else {
                        echo 'Order or Transaction key does not Match';
                    }
                } else{
                    echo 'This Payment is not recognized by DOHONE';
                }
            }
        }
    }

    public function notify() {

        if( isset($_GET) && !empty($_GET) ) {
            $r_uniqueTransactionId = $_GET['rI'];
            $r_amount_paid = (float)$_GET['rMt'];
            $r_currency = $_GET['rDvs'];
            $r_txn_reference = $_GET['idReqDoh'];
            $r_merchant_code = $_GET['rH'];
            $r_payment_mode = $_GET['mode'];
            $r_hash_key = $_GET['hash'];

            if( $r_txn_reference ) {
                $params = "cmd=verify&rI={$r_uniqueTransactionId}&rMt={$r_amount_paid}&idReqDoh={$r_txn_reference}&rDvs={$r_currency}";
                $linkSource = "https://www.my-dohone.com/dohone/pay?".$params;
                $curl = curl_init ($linkSource);
                curl_setopt($curl, CURLOPT_FAILONERROR, TRUE);
                curl_setopt($curl, CURLOPT_FOLLOWLOCATION, TRUE);
                curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
                curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, FALSE);
                curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, FALSE);
                $result = curl_exec ($curl);
                if ($result == 'OK') {
                    $verified = $this->dohone_logs_model->verify_transaction($r_uniqueTransactionId);
                    if( $verified == 'ok' ) {
                        $amount_verified = $this->dohone_logs_model->validate_amount($r_uniqueTransactionId, $r_amount_paid, $r_currency);
                        if( $amount_verified == 'ok' ) {
                            $merchant_verified = $this->dohone_logs_model->validate_merchant($r_uniqueTransactionId, $r_merchant_code);
                            if( $merchant_verified == 'ok' ) {
                                $this->dohone_logs_model->save_notify_data($r_uniqueTransactionId, $r_txn_reference, $r_payment_mode, $r_hash_key);
                            }
                        } else {
                            echo 'Amount is paid in wrong currency or Low amount paid';
                        }
                    } else {
                        echo 'Order or Transaction key does not Match';
                    }
                } else{
                    echo 'This Payment is not recognized by DOHONE';
                }
            }
        }
    }

    public function decline() {
        //echo 'reached'; exit;
        $this->content_data["transactionId"] = $_REQUEST['transaction_id'];
        $this->content_data["uniqueUserId"] = $_REQUEST['username'];
        $this->content_data["totalAmount"] = 'NA';
        $this->content_data["currency"] = $_REQUEST['currency'];
        $this->content_data["message"] = 'Either you have cancelled the payment, or server timeout';
        $this->quick_page_setup(Settings_model::$db_config['genome_theme'], '', 'Dohone Cancelled', 'decline', 'header', 'footer', '', $this->content_data);
    }

    public function decline_withdraw() {
        //echo 'reached'; exit;
        $this->content_data["transactionId"] = $_REQUEST['transaction_id'];
        $this->content_data["uniqueUserId"] = $_REQUEST['username'];
        $this->content_data["totalAmount"] = 'NA';
        $this->content_data["currency"] = $_REQUEST['currency'];
        $this->content_data["message"] = 'Either you have cancelled the payment, or server timeout';
        $this->quick_page_setup(Settings_model::$db_config['genome_theme'], '', 'Dohone Cancelled', 'decline_withdraw', 'header', 'footer', '', $this->content_data);
    }

    public function cancel() {
        $this->content_data["transactionId"] = '';
        $this->content_data["uniqueUserId"] = '';
        $this->content_data["totalAmount"] = '';
        $this->content_data["currency"] = '';
        $this->content_data["message"] = 'Either you have cancelled the payment, or server timeout';
        $this->quick_page_setup(Settings_model::$db_config['genome_theme'], '', 'Dohone Cancelled', 'decline', 'header', 'footer', '', $this->content_data);
    }

    public function alphawith_somechar($str) {


        if($str == '')
        {

          $this->form_validation->set_message('required', 'The %s field cannot be empty');
        }

        else if(!preg_match("/^([a-zA-Z0-9\.\s\-])+$/i", $str))    // username to contain only alphabets and spaces
        {
                $this->form_validation->set_message('alphawith_somechar', 'The %s field can only contain alphabets and numbers with spaces , dots and hyphen');
                return FALSE;
        }
        else
        {
                return TRUE;
        }
    }
}