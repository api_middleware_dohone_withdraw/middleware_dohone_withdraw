<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dashboard extends Gateway_Controller {

    public $content_data;
    public function __construct()
    {
        parent::__construct();
		/* Set current timezone */
        date_default_timezone_set("Asia/Kolkata");
        $this->load->helper('form');
        $this->load->library('form_validation');
    }

    public function index() {

        $reqData = $this->input->get();
        $uniqueTransactionId = strtolower(date("dmYHis"));
        $this->content_data["uniqueTransactionId"] = $uniqueTransactionId;
        $this->content_data["username"] = isset($reqData["username"])? $reqData["username"]:"";
        $this->content_data["currency"] = isset($reqData["currency"])? $reqData["currency"]:"";
        
        $this->quick_page_setup(Settings_model::$db_config['genome_theme'], 'main', 'Genome Dashboard', 'dashboard', 'header', 'footer', '', $this->content_data);
    }

    public function test_deposit() {

        $this->quick_page_setup(Settings_model::$db_config['genome_theme'], '', 'Iframe Dashboard', 'test_deposit', 'header', 'footer', '', $this->content_data);
    }

    public function test_withdraw() {

        $this->quick_page_setup(Settings_model::$db_config['genome_theme'], '', 'Iframe Dashboard', 'test_withdraw', 'header', 'footer', '', $this->content_data);
    }



    public function deposit() {

        $reqData = $this->input->get();

        $this->content_data["cryptapi_coin_options"] = array(
            'btc' => 'Bitcoin',
            'bch' => 'Bitcoin Cash',
            'ltc' => 'Litecoin',
            'eth' => 'Ethereum',
            'usdt' => 'USDT (ERC-20)',
            'usdc' => 'USDC (ERC-20)',
            'busd' => 'BUSD (ERC-20)',
            'pax' => 'PAX (ERC-20)',
            'tusd' => 'TUSD (ERC-20)',
            'bnb' => 'BNB (ERC-20)',
            'link' => 'ChainLink (ERC-20)',
            'cro' => 'Crypto Coin (ERC-20)',
            'mkr' => 'Maker (ERC-20)',
            'nexo' => 'NEXO (ERC-20)',
            'bcz' => 'BECAZ (ERC-20)',
            'xmr' => 'Monero',
            'iota' => 'IOTA',
            'trx' => 'TRX(TRC-20)',
        );
        $this->content_data["uniqueTransactionId"] = time();
        $this->content_data["username"] = isset($reqData["username"])? $reqData["username"]:"";
        $this->content_data["currency"] = isset($reqData["currency"])? $reqData["currency"]:"";

        $this->quick_page_setup(Settings_model::$db_config['genome_theme'], 'main', 'Genome Home', 'deposit_dashboard', 'header', 'footer', '', $this->content_data);
    }

    public function withdraw() {

        $reqData = $this->input->get();

        $this->content_data["cryptapi_coin_options"] = array(
            'btc' => 'Bitcoin',
            'bch' => 'Bitcoin Cash',
            'ltc' => 'Litecoin',
            'eth' => 'Ethereum',
            'usdt' => 'USDT (ERC-20)',
            'usdc' => 'USDC (ERC-20)',
            'busd' => 'BUSD (ERC-20)',
            'pax' => 'PAX (ERC-20)',
            'tusd' => 'TUSD (ERC-20)',
            'bnb' => 'BNB (ERC-20)',
            'link' => 'ChainLink (ERC-20)',
            'cro' => 'Crypto Coin (ERC-20)',
            'mkr' => 'Maker (ERC-20)',
            'nexo' => 'NEXO (ERC-20)',
            'bcz' => 'BECAZ (ERC-20)',
            'xmr' => 'Monero',
            'iota' => 'IOTA',
            'trx' => 'TRX(TRC-20)',
        );

        $this->content_data["uniqueTransactionId"] = time();
        $this->content_data["username"] = isset($reqData["username"])? $reqData["username"]:"";
        $this->content_data["currency"] = isset($reqData["currency"])? $reqData["currency"]:"";

        $this->quick_page_setup(Settings_model::$db_config['genome_theme'], 'main', 'Genome Home', 'withdraw_dashboard', 'header', 'footer', '', $this->content_data);
    }

}