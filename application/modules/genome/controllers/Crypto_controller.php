<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Crypto_controller extends Gateway_Controller {

    public $content_data;
    public function __construct() {

        parent::__construct();
		/* Set current timezone */
        date_default_timezone_set("Asia/Kolkata");
        $this->load->helper('form');
        $this->load->library('form_validation');
        $this->load->model('crypto_logs');

        $this->content_data["success_url"] = base_url()."index.php/crypto/success";
        $this->content_data["decline_url"] = base_url()."index.php/crypto/decline";
        $this->content_data["backUrl"] = base_url()."index.php/crypto/cancel";
    }

    public static $COIN_MULTIPLIERS = [
        'btc' => 100000000,
        'bch' => 100000000,
        'ltc' => 100000000,
        'eth' => 1000000000000000000,
        'trx' => 1000000000000000000,
        'usdt' => 10000000,
        'usdc' => 10000000,
        'busd' => 10000000000000000000,
        'pax' => 10000000000000000000,
        'tusd' => 10000000000000000000,
        'bnb' => 10000000000000000000,
        'link' => 10000000000000000000,
        'cro' => 1000000000,
        'mkr' => 10000000000000000000,
        'nexo' => 10000000000000000000,
        'bcz' => 10000000000000000000,
        'iota' => 1000000,
        'trx' => 1000000,
        'xmr' => 1000000000000,
    ];

    public $cryptapi_coin_n_tokens = array(
        'btc' => 'Bitcoin',
        'bch' => 'Bitcoin Cash',
        'ltc' => 'Litecoin',
        'eth' => 'Ethereum',
        'usdt' => 'USDT (ERC-20)',
        'usdc' => 'USDC (ERC-20)',
        'busd' => 'BUSD (ERC-20)',
        'pax' => 'PAX (ERC-20)',
        'tusd' => 'TUSD (ERC-20)',
        'bnb' => 'BNB (ERC-20)',
        'link' => 'ChainLink (ERC-20)',
        'cro' => 'Crypto Coin (ERC-20)',
        'mkr' => 'Maker (ERC-20)',
        'nexo' => 'NEXO (ERC-20)',
        'bcz' => 'BECAZ (ERC-20)',
        'xmr' => 'Monero',
        'iota' => 'IOTA',
        'trx' => 'TRX(TRC-20)',
    );

    public $ERC_TOKENS_URI = [
        'usdt' => '0xdAC17F958D2ee523a2206206994597C13D831ec7',
        'usdc' => '0xA0b86991c6218b36c1d19D4a2e9Eb0cE3606eB48',
        'busd' => '0x4Fabb145d64652a948d72533023f6E7A623C7C53',
        'pax' => '0x8E870D67F660D95d5be530380D0eC0bd388289E1',
        'tusd' => '0x0000000000085d4780B73119b644AE5ecd22b376',
        'bnb' => '0xB8c77482e45F1F44dE1745F52C74426C631bDD52',
        'link' => '0x514910771AF9Ca656af840dff83E8264EcF986CA',
        'cro' => '0xA0b73E1Ff0B80914AB6fe0444E65848C4C34450b',
        'mkr' => '0x9f8F72aA9304c8B593d555F12eF6589cC3A579A2',
        'nexo' => '0xB62132e35a6c13ee1EE0f84dC5d40bad8d815206',
        'bcz' => '0x08399ab5eBBE96870B289754A7bD21E7EC8c6FCb',
    ];

    private $erc20_tokens = array('usdt', 'usdc', 'busd', 'pax', 'tusd', 'bnb', 'link', 'cro', 'mkr', 'nexo', 'bcz');

    public function deposit() {
        
        $this->cryptapi_coin_addresses = array(
            'btc' => '1PE5U4temq1rFzseHHGE2L8smwHCyRbkx3',
            'bch' => 'bitcoincash:qrz6pkvsaxmy6aypy8sdvg5l3pac8zg3qvtqdaarwq',
            'ltc' => 'LLGrzcbppKCii4377sMpRiiBeNP8QjWnqt',
            'eth' => '0x2C8eD8e82862D1DdE03C9bf1f4feC60EF642E406',
            'usdt' => '0x76e81bf0d554c1c9237cc143905079c6a4a5bf26',
            'usdc' => '0x76e81bf0d554c1c9237cc143905079c6a4a5bf26',
            'busd' => '0x76e81bf0d554c1c9237cc143905079c6a4a5bf26',
            'pax' => '0x76e81bf0d554c1c9237cc143905079c6a4a5bf26',
            'tusd' => '0x76e81bf0d554c1c9237cc143905079c6a4a5bf26',
            'bnb' => '0x76e81bf0d554c1c9237cc143905079c6a4a5bf26',
            'link' => '0x76e81bf0d554c1c9237cc143905079c6a4a5bf26',
            'cro' => 'Crypto 0x76e81bf0d554c1c9237cc143905079c6a4a5bf26',
            'mkr' => '0x76e81bf0d554c1c9237cc143905079c6a4a5bf26',
            'nexo' => '0x76e81bf0d554c1c9237cc143905079c6a4a5bf26',
            'bcz' => '0x76e81bf0d554c1c9237cc143905079c6a4a5bf26',
            'xmr' => '84wnBPZxhSrHFRnrHnBsnti5jZkWcXJRrYJWJ4A61xEQfJF4H8VL9RZGcvSrQKumMNFHgEqHiJvMHWkx67w64Q2sQRmkxE7',
            'iota' => 'GCSTVSRTLLPBEXZNGWTDHCGPNVARNSRXFPWGAWTQBSZLIMELSZPWKKTKXHKBXIUDNGQFHVYAZMHG9NXNDRXPRLL9ZA',
            'trx' => 'TBEQYbaTXZdYd9hUpEpp6QRRTbzs3o6kSr',
        );

        $crypto_amount = $_POST['amount'];
        $crypto_coin_address = $this->cryptapi_coin_addresses[$_POST['cryptapi_selected_coin']];
        $crypto_username = $_POST['username'];
        $crypto_currency = $_POST['currency'];
        $crypto_uniqueTransactionId = $_POST['uniqueTransactionId'];
        $crypto_selected_coin = $_POST['cryptapi_selected_coin'];
        $success_url = $this->content_data["success_url"];

        $customproduct = array(
            "currency" => $crypto_currency,
            "amount" => $crypto_amount
          );

        $this->content_data["uniquetid"] = $crypto_uniqueTransactionId;
        $this->content_data["uniqueuserid"] = $crypto_username;
        $this->content_data["crypto_selected_coin"] = $crypto_selected_coin;
        $this->content_data["crypto_coin_address"] = $crypto_coin_address;
        $this->content_data["customproduct"] = $customproduct;

        if( isset( $crypto_selected_coin ) && !empty( $crypto_selected_coin ) && isset( $crypto_coin_address ) && !empty( $crypto_coin_address ) && isset( $crypto_amount ) && !empty( $crypto_amount ) ) {

            $base_url = 'https://api.cryptapi.io/';
            $prefix_url = "{$base_url}{$crypto_selected_coin}/create/";

            $nonce = $this->generate_nonce();

            if( in_array( $crypto_selected_coin, $this->erc20_tokens) ) {
                $prefix_url = "{$base_url}erc20/{$crypto_selected_coin}/create/";
            }

            $invoice_id = $_POST['uniqueTransactionId'];

            if( isset( $prefix_url ) && !empty( $prefix_url ) ) {

                $send_first_url = "{$prefix_url}?address={$crypto_coin_address}&callback={$success_url}&invoice={$invoice_id}&nonce={$nonce}";

                $callback_url_with_nonce = "{$success_url}&invoice={$invoice_id}&nonce={$nonce}";

                $this->content_data["send_first_url"] = $send_first_url;

                $this->content_data["prefix_url"] = $prefix_url;

                $this->content_data["callback_url_with_nonce"] = $callback_url_with_nonce;
             
                $this->initiliaze_url = isset( $send_first_url ) ? $send_first_url : "";

                $total = $crypto_amount;

                $this->info_url = 'https://api.cryptapi.io/'.$crypto_selected_coin.'/info/';

                if( in_array( $crypto_selected_coin, $this->erc20_tokens ) ) {
                    $this->info_url = 'https://api.cryptapi.io/erc20/'.$crypto_selected_coin.'/info/';
                }

                $info = $this->cryptapi_use_curl('info');

                $currency = $crypto_currency;

                $price = floatval($info->prices->USD);

                if ( isset( $info->prices->{$currency} ) ) {
                    $price = floatval( $info->prices->{$currency} );
                }

                $crypto_total = $this->round_sig( $total / $price, 5 );

                $min_tx = $this->convert_div( $info->minimum_transaction, $crypto_selected_coin );

                if ($crypto_total < $min_tx) {
                    echo('Payment error: ') .('Value too low, minimum is') . ' ' . $min_tx . ' ' . strtoupper($crypto_selected_coin);
                    return null;
                }
                $curl_req_for_add = $this->cryptapi_use_curl( 'address' );
                
                if( isset( $curl_req_for_add ) && !empty( $curl_req_for_add ) && ( $curl_req_for_add == 'Success' ) ) {
                    
                    $this->content_data["cryptapi_nonce"] = $nonce;
                    $this->content_data["invoice_id"] = $invoice_id;
                    $this->content_data["cryptapi_address_in"] = $this->address_in;
                    $this->content_data["cryptapi_total"] = $crypto_total;
                    $this->content_data["cryptapi_currency"] = $crypto_selected_coin;
                    $this->content_data["cryptapi_coin_n_tokens"] = $this->cryptapi_coin_n_tokens;
                    $this->content_data["ERC_TOKENS_URI"] = $this->ERC_TOKENS_URI;
                    $this->content_data["COIN_MULTIPLIERS"] = Crypto_controller::$COIN_MULTIPLIERS;
                    $this->content_data["cryptapi_currency_full_name"] = $this->cryptapi_coin_n_tokens[$crypto_selected_coin];

                    $this->crypto_logs->insert_deposit_log( $crypto_username, $total, $currency, $crypto_selected_coin, $crypto_total, $invoice_id, $nonce, $crypto_coin_address, $this->address_in, $prefix_url );

                    $this->quick_page_setup(Settings_model::$db_config['genome_theme'], 'main', 'CryptApi Checkout', 'crypto_checkout', 'header', 'footer', '', $this->content_data);

                }
                else if( isset( $curl_req_for_add ) && !empty( $curl_req_for_add ) && ( $curl_req_for_add == 'error' ) ) {
                    echo(( 'Payment error: ' ) . ( $this->error_msg ));
                    return null;
                }
                else {
                    echo(( 'Payment error: ' ) . ( 'Payment Could not be proccessed, try Again' ));
                    return null;
                }
            }
        }
    }

    public function deposit_ajax() {

        $order_id = $_GET['ca_order_id'];
        $crypto_value = $_GET['crypto_value'];
        $amount_paid = $_GET['amount_paid'];
        $verify_callback_url = $_GET['verify_callback_url'];
        $prefix_url = $_GET['prefix_url'];
        $cryptapi_nonce = $_GET['cryptapi_nonce'];
        $prefix_url = str_replace("create","logs",$prefix_url);
        $verify_cb_url = "{$prefix_url}?callback={$verify_callback_url}";
        $payment_cb = $this->payment_confirmation( $verify_cb_url );
        $this->validate_payment( $payment_cb, $order_id, $cryptapi_nonce, $crypto_value, $amount_paid );
    }

    private function validate_payment( $payment_cb, $order_id, $nonce, $crypto_value, $amount_paid ) {

		$data = $this->process_callback( $payment_cb );
		
        $value_convert = $data['coin'];

        $paid = $amount_paid + $value_convert;

		if( isset( $data['confirmations'] ) && is_array( $data['confirmations'] ) && !empty( $data['confirmations'] ) ) {

			if( $data['confirmations'] > 1 ) {

				if ($paid >= $crypto_value) {

                    $data = array(
                        'status' => 'success',
                        'payment_received_time' => date('Y-m-d H:i:s'),
                        'amount_remaining' => '',
                    );
                    $this->db->where('order_id', $order_id);

                    $this->db->update(DB_PREFIX .'crypto_deposit', $data);

					echo json_encode('payment_completed');
				}
				else
				{
					$amount_remaining = $crypto_value - $paid;

                    $payment_made_partial = array(
                        'status' => 'partial',
                        'amount_remaining' => $amount_remaining
                    );

                    $data = array(
                        'status' => 'partial',
                        'payment_received_time' => date('Y-m-d H:i:s'),
                        'crypto_amount_remaining' => $amount_remaining,
                    );
            
                    $this->db->where('order_id', $order_id);
                    $this->db->update(DB_PREFIX .'crypto_deposit', $data);

                    echo json_encode($payment_made_partial);
				}
			}
		}
	}

    private function process_callback($_get, $convert = false) {
		
		$params = [
            'address_in' => $_get['address_in'],
            'address_out' => $_get['address_out'],
            'callbacks' => $_get['callbacks'][0],
            'txid_in' => isset($_get['callbacks'][0]['txid_in']) ? $_get['callbacks'][0]['txid_in'] : null,
            'txid_out' => isset($_get['callbacks'][0]['txid_out']) ? $_get['callbacks'][0]['txid_out'] : null,
            'confirmations' => isset($_get['callbacks'][0]['confirmations']) ? $_get['callbacks'][0]['confirmations'] : null,
            'value' => isset( $_get['callbacks'][0]['value'] ) ? $_get['callbacks'][0]['value'] : '',
            'value_forwarded' => isset($_get['callbacks'][0]['value_forwarded']) ? $_get['callbacks'][0]['value_forwarded'] : null,
            'coin' => isset($_get['callbacks'][0]['value_coin']) ? $_get['callbacks'][0]['value_coin'] : null,
            'pending' => isset($_get['notify_pending']) ? $_get['notify_pending'] : false,
        ];

        return $params;
	}

    private function payment_confirmation( $verify_cb_url ) {

		$ca_curl = curl_init();
        curl_setopt( $ca_curl, CURLOPT_HEADER, false );
        curl_setopt( $ca_curl, CURLOPT_HTTPHEADER, array( 'Accept: application/json', 'Accept-Language: en_US' ) );
        curl_setopt( $ca_curl, CURLOPT_VERBOSE, 1 );
        curl_setopt( $ca_curl, CURLOPT_TIMEOUT, 30 );
        curl_setopt( $ca_curl, CURLOPT_URL, $verify_cb_url );
        curl_setopt( $ca_curl, CURLOPT_RETURNTRANSFER, true );
        curl_setopt( $ca_curl, CURLOPT_SSLVERSION, 6 );
        $ca_curl_response = curl_exec( $ca_curl );
        curl_close( $ca_curl );
		$ca_curl_response_array = json_decode( $ca_curl_response, true );
		return $ca_curl_response_array;
	}


    public function withdraw() {
        
        $crypto_amount = $_POST['amount'];
        $crypto_coin_address = $_POST['coin_address'];
        $crypto_username = $_POST['username'];
        $crypto_currency = $_POST['currency'];
        $crypto_uniqueTransactionId = $_POST['uniqueTransactionId'];
        $crypto_selected_coin = $_POST['cryptapi_selected_coin'];
        $success_url = $this->content_data["success_url"];

        $customproduct = array(
            "currency" => $crypto_currency,
            "amount" => $crypto_amount
          );

        $this->content_data["uniquetid"] = $crypto_uniqueTransactionId;
        $this->content_data["uniqueuserid"] = $crypto_username;
        $this->content_data["crypto_selected_coin"] = $crypto_selected_coin;
        $this->content_data["crypto_coin_address"] = $crypto_coin_address;
        $this->content_data["customproduct"] = $customproduct;

        if( isset( $crypto_selected_coin ) && !empty( $crypto_selected_coin ) && isset( $crypto_coin_address ) && !empty( $crypto_coin_address ) && isset( $crypto_amount ) && !empty( $crypto_amount ) ) {

            $base_url = 'https://api.cryptapi.io/';
            $prefix_url = "{$base_url}{$crypto_selected_coin}/create/";

            $nonce = $this->generate_nonce();

            if( in_array( $crypto_selected_coin, $this->erc20_tokens) ) {
                $prefix_url = "{$base_url}erc20/{$crypto_selected_coin}/create/";
            }

            $invoice_id = $crypto_uniqueTransactionId;

            if( isset( $prefix_url ) && !empty( $prefix_url ) ) {

                $send_first_url = "{$prefix_url}?address={$crypto_coin_address}&callback={$success_url}&invoice={$invoice_id}&nonce={$nonce}";

                $callback_url_with_nonce = "{$success_url}&invoice={$invoice_id}&nonce={$nonce}";

                $this->content_data["callback_url_with_nonce"] = $callback_url_with_nonce;

                $this->content_data["send_first_url"] = $send_first_url;
             
                $this->initiliaze_url = isset( $send_first_url ) ? $send_first_url : "";

                $total = $crypto_amount;

                $this->info_url = 'https://api.cryptapi.io/'.$crypto_selected_coin.'/info/';

                if( in_array( $crypto_selected_coin, $this->erc20_tokens ) ) {
                    $this->info_url = 'https://api.cryptapi.io/erc20/'.$crypto_selected_coin.'/info/';
                }

                $info = $this->cryptapi_use_curl('info');

                $currency = $crypto_currency;

                $price = floatval($info->prices->USD);

                if ( isset( $info->prices->{$currency} ) ) {
                    $price = floatval( $info->prices->{$currency} );
                }

                $crypto_total = $this->round_sig( $total / $price, 5 );

                $min_tx = $this->convert_div( $info->minimum_transaction, $crypto_selected_coin );

                if ($crypto_total < $min_tx) {
                    echo('Payment error: ') .('Value too low, minimum is') . ' ' . $min_tx . ' ' . strtoupper($crypto_selected_coin);
                    return null;
                }
                $curl_req_for_add = $this->cryptapi_use_curl( 'address' );
                
                if( isset( $curl_req_for_add ) && !empty( $curl_req_for_add ) && ( $curl_req_for_add == 'Success' ) ) {
                    
                    $this->content_data["cryptapi_nonce"] = $nonce;
                    $this->content_data["invoice_id"] = $invoice_id;
                    $this->content_data["cryptapi_address_in"] = $this->address_in;
                    $this->content_data["cryptapi_total"] = $crypto_total;
                    $this->content_data["cryptapi_currency"] = $crypto_selected_coin;
                    $this->content_data["cryptapi_coin_n_tokens"] = $this->cryptapi_coin_n_tokens;
                    $this->content_data["ERC_TOKENS_URI"] = $this->ERC_TOKENS_URI;
                    $this->content_data["COIN_MULTIPLIERS"] = Crypto_controller::$COIN_MULTIPLIERS;
                    $this->content_data["cryptapi_currency_full_name"] = $this->cryptapi_coin_n_tokens[$crypto_selected_coin];

                    $this->quick_page_setup(Settings_model::$db_config['genome_theme'], '', 'CryptApi Withdraw Checkout', 'crypto_withdraw_checkout', 'header', 'footer', '', $this->content_data);

                    $this->crypto_logs->insert_withdraw_log( $crypto_username, $total, $currency, $crypto_selected_coin, $crypto_total, $invoice_id, $nonce, $crypto_coin_address, $this->address_in, $prefix_url );

                }
                else if( isset( $curl_req_for_add ) && !empty( $curl_req_for_add ) && ( $curl_req_for_add == 'error' ) ) {
                    echo(( 'Payment error: ' ) . ( $this->error_msg ));
                    return null;
                }
                else {
                    echo(( 'Payment error: ' ) . ( 'Payment Could not be proccessed, try Again' ));
                    return null;
                }
            }
        }
    }

    /**
	 * Function is used to generate nonce
	 *
	 * @since    1.0.0
	 */
    private function generate_nonce()
    {
        $len = 32;

        $data = str_split('abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789');

        $nonce = [];
        for ($i = 0; $i < $len; $i++) {
            $nonce[] = $data[mt_rand(0, sizeof($data) - 1)];
        }

        return implode('', $nonce);
    }

    /**
	 * Function is used to calculate division of the values.
	 *
	 * @since    1.0.0
	 */
    private function convert_div( $val, $coin ) {
        return $val / Crypto_controller::$COIN_MULTIPLIERS[$coin];
    }

    /**
	 * Function is used to calculate rounds of number for currencies.
	 *
	 * @since    1.0.0
	 */
    private function round_sig($number, $sigdigs = 5)
    {
        $multiplier = 1;
        while ($number < 0.1) {
            $number *= 10;
            $multiplier /= 10;
        }
        while ($number >= 1) {
            $number /= 10;
            $multiplier *= 10;
        }
        return round($number, $sigdigs) * $multiplier;
    }

    /**
	 * Function is used send and retrieve data from the server by using curl
	 *
	 * @since    1.0.0
	 */
    private function cryptapi_use_curl( $ca_for )
    {
        $ca_curl = curl_init();
        curl_setopt( $ca_curl, CURLOPT_HEADER, false );
        curl_setopt( $ca_curl, CURLOPT_HTTPHEADER, array( 'Accept: application/json', 'Accept-Language: en_US' ) );
        curl_setopt( $ca_curl, CURLOPT_VERBOSE, 1 );
        curl_setopt( $ca_curl, CURLOPT_TIMEOUT, 30 );
        if( $ca_for == 'address' ) {
            curl_setopt( $ca_curl, CURLOPT_URL, $this->initiliaze_url );
        }
        else if( $ca_for == 'info' ) {
            curl_setopt( $ca_curl, CURLOPT_URL, $this->info_url );
        }
        curl_setopt( $ca_curl, CURLOPT_RETURNTRANSFER, true );
        curl_setopt( $ca_curl, CURLOPT_SSLVERSION, 6 );
        $ca_curl_response = curl_exec( $ca_curl );
        curl_close( $ca_curl );
        if( $ca_for == 'address' ) {
            $ca_curl_response_array = json_decode( $ca_curl_response, true );
            if( isset( $ca_curl_response_array['status'] ) && !empty( $ca_curl_response_array['status'] ) ) {
                if( $ca_curl_response_array['status'] == 'success' ) {
                    $this->address_in = isset( $ca_curl_response_array['address_in'] ) ? $ca_curl_response_array['address_in'] : "";
                    $this->address_out = isset( $ca_curl_response_array['address_out'] ) ? $ca_curl_response_array['address_out'] : "";
                    $this->callback_url = isset( $ca_curl_response_array['callback_url'] ) ? $ca_curl_response_array['callback_url'] : "";
                    $this->priority = isset( $ca_curl_response_array['priority'] ) ? $ca_curl_response_array['priority'] : "";
                    return 'Success';
                }
                else if( $ca_curl_response_array['status'] == 'error' ) {
                    $this->error_msg = isset( $ca_curl_response_array['error'] ) ? $ca_curl_response_array['error'] : "" ;
                    return 'error';
                }
                else {
                    return 'False';
                }
            }
        }
        else if( $ca_for == 'info' ) {
            return json_decode( $ca_curl_response );
        }
    }


    public function success()
    {
        $data = $_REQUEST;
        /* This is an example of genome payment ....
            Update this code as need  on crypto ....
        
        $this->content_data["transactionId"] = $data["transactionId"];
        $this->content_data["uniqueUserId"] = $data["uniqueUserId"];
        $this->content_data["totalAmount"] = $data["totalAmount"];
        $this->content_data["currency"] = $data["currency"];
        $this->content_data["message"] = $data["message"];

        //Update success response
        $columnArray = array(
          "response_text"=>json_encode($data),
          "transaction_status"=>"SUCCESS"
        );
        $this->crypto_logs->update_deposit($columnArray, $data["transactionId"]);

        $this->quick_page_setup(Settings_model::$db_config['genome_theme'], '', 'Genome Success', 'success', 'header', 'footer', '', $this->content_data);
        */
    }

    public function decline()
    {
        $data = $_REQUEST;
        /* This is an example of genome payment ....
            Update this code as need  on crypto ....
        
        $this->content_data["transactionId"] = $data["transactionId"];
        $this->content_data["uniqueUserId"] = $data["uniqueUserId"];
        $this->content_data["totalAmount"] = $data["totalAmount"];
        $this->content_data["currency"] = $data["currency"];
        $this->content_data["message"] = $data["message"];
        
        //Update success response
        $columnArray = array(
          "response_text"=>json_encode($data),
          "transaction_status"=>"FAILED"
        );
        $this->genome_model->update_deposit($columnArray, $data["transactionId"]);

        $this->quick_page_setup(Settings_model::$db_config['genome_theme'], '', 'Genome Decline', 'decline', 'header', 'footer', '', $this->content_data);
        */
    }
    public function cancel()
    {
        /* This is an example of genome payment ....
            Update this code as need  on crypto ....
          $this->content_data["transactionId"] = "NA";
          $this->content_data["uniqueUserId"] = "NA";
          $this->content_data["totalAmount"] = "NA";
          $this->content_data["currency"] = "NA";
          $this->content_data["message"] = "NA";
          
          //Update success response
          $columnArray = array(
            "response_text"=>"Cancelled",
            "transaction_status"=>"CANCELLED"
          );
          $this->crypto_logs->update_deposit($columnArray, $data["transactionId"]);

          $this->quick_page_setup(Settings_model::$db_config['genome_theme'], '', 'Perfectmoney Decline', 'decline', 'header', 'footer', '', $this->content_data);
        */
    }
}