<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once APPPATH.'vendor/autoload.php';
use AyubIRZ\PerfectMoneyAPI\PerfectMoneyAPI;

class Perfectmoney_controller extends Gateway_Controller {

    public $content_data;

    private $PMAccountID = '4014188';   
    private $PMPassPhrase = 'perfect123';   

    public function __construct()
    {
        parent::__construct();
		/* Set current timezone */
        date_default_timezone_set("Asia/Kolkata");
        $this->load->helper('form');
        $this->load->library('form_validation');
        $this->load->model('perfectmoney_model');

        $this->content_data["success_url"] = base_url()."index.php/perfectmoney/success";
        $this->content_data["decline_url"] = base_url()."index.php/perfectmoney/decline";
        $this->content_data["backUrl"] = base_url()."index.php/perfectmoney/cancel";

    }

    public function getAccountName($account)
    {
        // trying to open URL to process PerfectMoney getAccountName request
        $this->PMAccountID = '4014188';
        $this->PMPassPhrase = 'Perfect123';
        //$data = file_get_contents("https://perfectmoney.com/acct/balance.asp?AccountID=".$this->PMAccountID."&PassPhrase=".$this->PMPassPhrase."&Account=".$account."", 'rb');

        $data = file_get_contents("https://perfectmoney.com/acct/confirm.asp?AccountID=".$this->PMAccountID."&PassPhrase=".$this->PMPassPhrase."&Payer_Account=U987654&Payee_Account=U1234567&Amount=1&PAY_IN=1&PAYMENT_ID=1223", 'rb');

        if($data == 'ERROR: Can not login with passed AccountID and PassPhrase'){

            throw new Exception('Invalid PerfectMoney Username or Password.', 500);

        }elseif($data == 'ERROR: Invalid Account'){

            throw new Exception('Invalid PerfectMoney Account specified.', 500);

        }

        return $data;
    }

    public function index() {

        $this->quick_page_setup(Settings_model::$db_config['genome_theme'], '', 'Genome Home', 'perfectmoney', 'header', 'footer', '', $this->content_data);
    }

    public function deposit()
    {
        $post = $this->input->post();

        //calculate signature
        $uniqueuserid = $post["username"];
        $uniqueTransactionId = $post["uniqueTransactionId"];
        $pmAccountID = "U23568901";
        $amount = $post["amount"];
        $currency = $post["currency"];
        $customproduct = array(
                    "currency"=>$currency,
                    "amount"=>$amount
                  );

        $this->content_data["uniquetid"] = $uniqueTransactionId;
        $this->content_data["uniqueuserid"] = $uniqueuserid;
        $this->content_data["pmAccountID"] = $pmAccountID;
        $this->content_data["customproduct"] = $customproduct;

        

        //Insert request data
        $columnArray = array(
            "pm_accountid"=>$pmAccountID,
            "userid"=>$uniqueuserid,
            "transaction_id"=>$uniqueTransactionId,
            "amount"=>$amount,
            "currency"=>$currency,
            "customproduct"=>json_encode($customproduct),
            "success_url"=>$this->content_data["success_url"],
            "decline_url"=>$this->content_data["decline_url"],
            "back_url"=>$this->content_data["backUrl"],
            "transaction_status"=>"PENDING",
            "entry_ip"=>$_SERVER['REMOTE_ADDR']
          );
        $this->perfectmoney_model->insert_deposit($columnArray);

        $this->quick_page_setup(Settings_model::$db_config['genome_theme'], '', 'PM Checkout', 'perfectmoney_checkout', 'header', 'footer', '', $this->content_data);

    }

    public function success()
    {
        $data = $_REQUEST;
        #print_r($data);
        $this->content_data["transactionId"] = $data["PAYMENT_ID"];
        $this->content_data["uniqueUserId"] = $data["UserId"];
        $this->content_data["totalAmount"] = $data["PAYMENT_AMOUNT"];
        $this->content_data["currency"] = $data["PAYMENT_UNITS"];
        $this->content_data["message"] = "NA";

        //Update success response
        $columnArray = array(
          "response_text"=>json_encode($data),
          "transaction_status"=>"SUCCESS"
        );
        $this->perfectmoney_model->update_deposit($columnArray, $data["PAYMENT_ID"]);

        $this->quick_page_setup(Settings_model::$db_config['genome_theme'], '', 'Perfectmoney Success', 'success', 'header', 'footer', '', $this->content_data);
    }
    public function decline()
    {
        $data = $_REQUEST;
        #print_r($data);
        $this->content_data["transactionId"] = $data["PAYMENT_ID"];
        $this->content_data["uniqueUserId"] = $data["UserId"];
        $this->content_data["totalAmount"] = $data["PAYMENT_AMOUNT"];
        $this->content_data["currency"] = $data["PAYMENT_UNITS"];
        $this->content_data["message"] = "NA";

        //Update success response
        $columnArray = array(
          "response_text"=>json_encode($data),
          "transaction_status"=>"FAILED"
        );
        $this->perfectmoney_model->update_deposit($columnArray, $data["PAYMENT_ID"]);

        $this->quick_page_setup(Settings_model::$db_config['genome_theme'], '', 'Perfectmoney Decline', 'decline', 'header', 'footer', '', $this->content_data);
    }
    public function cancel()
    {
      $this->content_data["transactionId"] = "NA";
      $this->content_data["uniqueUserId"] = "NA";
      $this->content_data["totalAmount"] = "NA";
      $this->content_data["currency"] = "NA";
      $this->content_data["message"] = "NA";
      
      //Update success response
      $columnArray = array(
        "response_text"=>"Cancelled",
        "transaction_status"=>"CANCELLED"
      );
      $this->genome_model->update_deposit($columnArray, $data["transactionId"]);

      $this->quick_page_setup(Settings_model::$db_config['genome_theme'], '', 'Perfectmoney Decline', 'decline', 'header', 'footer', '', $this->content_data);
    }

}