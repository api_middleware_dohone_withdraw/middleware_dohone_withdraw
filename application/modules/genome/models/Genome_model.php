<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Genome_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    //save genome deposit transaction
    public function insert_deposit($columnArray)
    {
        $table = "genome_deposit";
        return $this->sql_helper->insert_data($table, $columnArray);
    }

    //update genome deposit resposne
    public function update_deposit($columnArray, $transaction_id)
    {
        $table = "genome_deposit";
        $whereArray = array("transaction_id"=>$transaction_id);
        return $this->sql_helper->update_data($table, $columnArray, $whereArray);
    }

    public function deposit_log($where_val, $where_val2=NULL) {
        $table = 'genome_deposit';
        $columns = 'user_id,transaction_id,amount,genome_key,resulting,signature,response_text,transaction_status,entry_date';
        $where = array();
        $orderBy = array('entry_date'=>'asc');
        return $this->sql_helper->getResult($table, $columns, $where, $orderBy);
    }

}