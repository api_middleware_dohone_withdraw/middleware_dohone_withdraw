<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Perfectmoney_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    //save perfectmoney deposit transaction
    public function insert_deposit($columnArray)
    {
        $table = "pmoney_deposit";
        return $this->sql_helper->insert_data($table, $columnArray);
    }

    //update perfectmoney deposit resposne
    public function update_deposit($columnArray, $transaction_id)
    {
        $table = "pmoney_deposit";
        $whereArray = array("transaction_id"=>$transaction_id);
        return $this->sql_helper->update_data($table, $columnArray, $whereArray);
    }

    public function deposit_log($where_val, $where_val2=NULL) {
        $table = 'pmoney_deposit';
        $columns = 'user_id,transaction_id,amount,pm_accountid,customproduct,suucess_url,decline_url,back_url,response_text,transaction_status,entry_date';
        $where = array();
        $orderBy = array('entry_date'=>'asc');
        return $this->sql_helper->getResult($table, $columns, $where, $orderBy);
    }

}