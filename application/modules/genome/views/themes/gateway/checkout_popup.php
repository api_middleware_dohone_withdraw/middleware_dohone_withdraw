
<div>
  <script src="https://hpp-service.genome.eu/paymentPage.js"
    class="pspScript"
    data-iframesrc="https://hpp-service.genome.eu/hpp"
    data-buttontext="Pay!"
    data-name="Payment Page"
    data-key="<?php echo $key; ?>"
    data-customproduct='[<?php echo json_encode($customproduct) ?>]'
    data-signature="<?php echo $signature; ?>"
    data-uniqueuserid="<?php echo $uniqueuserid; ?>"
    data-uniqueTransactionId="<?php echo $uniquetid; ?>"
    data-success_url='<?php echo $success_url; ?>'
    data-decline_url='<?php echo $decline_url; ?>'
    data-backUrl='<?php echo $backUrl; ?>'
    data-type="integrated" 
    data-width="auto" 
    data-height="auto">
  </script>
  <form class='pspPaymentForm'></form>
  <iframe id='psp-hpp-<?php echo $signature; ?>'></iframe>
</div>
          

<?php /*
<form id="paymentForm" action='https://hpp-service.genome.eu/hpp' class='redirect_form' method='post'>
    <input type='hidden' name='key' value='<?php echo $key; ?>'>
    <input type='hidden' name='name' value='Payment Page'>
    <input type='hidden' name='signature' value='<?php echo $signature; ?>'>
    <input type='hidden' name='customproduct' value='[<?php echo json_encode($customproduct) ?>]'>
    <input type='hidden' name='uniqueuserid' value='<?php echo $uniqueuserid; ?>'>
    <input type='hidden' name='uniqueTransactionId' value='<?php echo $uniquetid; ?>'>
    <input type='hidden' name='success_url' value='<?php echo $success_url; ?>'>
    <input type='hidden' name='decline_url' value='<?php echo $decline_url; ?>'>
    <input type='hidden' name='backUrl' value='<?php echo $backUrl; ?>'>
</form>


<script>
	window.onload = function(){
	  document.forms['paymentForm'].submit();
	}
</script>

*/ ?>