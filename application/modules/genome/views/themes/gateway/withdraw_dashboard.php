<?php if (!defined('BASEPATH')) exit('No direct script access allowed'); ?>

<?php $this->load->view('themes/' . Settings_model::$db_config['active_theme'] . '/partials/content_head.php'); ?>

<?php $this->load->view('generic/flash_error'); ?>


<style>
* {box-sizing: border-box}
body {font-family: "Lato", sans-serif;}

/* Style the tab */
.tab {
  float: left;
  border: 1px solid #ccc;
  background-color: #f1f1f1;
  width: 20%;
  height: 100%;
}

/* Style the buttons inside the tab */
.tab button {
  display: block;
  background-color: inherit;
  color: black;
  padding: 22px 16px;
  width: 100%;
  border: none;
  outline: none;
  text-align: left;
  cursor: pointer;
  transition: 0.3s;
  font-size: 17px;
}

/* Change background color of buttons on hover */
.tab button:hover {
  background-color: #ddd;
}

/* Create an active/current "tab button" class */
.tab button.active {
  background-color: #000;
  color: #fff;
}

/* Style the tab content */
.tabcontent {
  float: left;
  background-color: #fff;
  padding: 0px 12px;
  border: 1px solid #ccc;
  width: 80%;
  border-left: none;
  height: 100%;
}

#error_full_name_backend{
        margin-top:5px !important;
        display: none;
        color:red;
        font-size:11px;
}

#inside_heading{

     color:red;
     font-size:14px;
}

</style>

<div class="row">
    <div class="panel-footer" style="background-color:#222d32;">
        <div class="row">
            <div class="box-header" style="width:100%;">
                <div class="col-md-12">
                    <p style="text-align: right; color:#fff;">Bitlogik Withdrawal Payment Service</p>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <h2>Select a Payment Service &nbsp;&nbsp; <span id="inside_heading"><?php if (isset($form_validation_errors)) {

              echo "Sorry please fill in the Dohone Withdraw Form properly and move to Dohone Payment Section to Identify Errors !!";
   }
  else{

  } ?></span></h2>
        <div class="tab">
          <button class="tablinks" onclick="openPage(event, 'Genome')" id="defaultOpen">Genome</button>
          <button class="tablinks" onclick="openPage(event, 'PerfectMoney')">Perfect Money</button>
          <button class="tablinks" onclick="openPage(event, 'CryptoPayment')">Crypto Payment</button>
          <button class="tablinks" onclick="openPage(event, 'DohonePayment')">Dohone Payment</button>
        </div>

        <!--
          Code for Genome Payment Gateway Design
          Pass query string as ?username=XXXXXX&currency=USD
        -->
        <div id="Genome" class="tabcontent">
          <p>

            <div class="container">
              <h2>Genome Payment Service Test Page</h2>
              <hr/>

              <?php if($username == "" || $currency == "") {?>


                <div class="alert alert-danger" role="alert">
                  Username and currency not found.
                </div>

              <?php } else { ?>
              <?php print form_open('genome/genome_controller/withdraw', array('id' => 'payment_form', 'class' => 'js-parsley', 'data-parsley-submit' => 'pay_now', 'onsubmit' => "return submitform();")) . "\r\n"; ?>


                <div class="form-group row">
                  <label for="username">User Name: <span style="margin-left: 20px;text-transform: capitalize;"> <?php echo $username; ?></span></label>
                </div>

                <div class="form-group row">
                  <label for="username">Transaction ID: <span style="margin-left: 20px;"> <?php echo $uniqueTransactionId; ?></span>
                    <span style="margin-left: 20px; text-decoration: none; font-weight: 100;">(Kindly write this transaction id somewhere, for further reference. It helps if transaction fail.)</span>
                  </label>
                </div>

                <div class="form-group row">
                  <label for="amount">Amount: </label>
                  <div class="input-group input-group-sm">
                    <div class="input-group-btn">
                      <button class="btn btn-default"><?php echo $currency; ?></button>
                    </div>
                    <input type="number" class="form-control" placeholder="Enter amount" name="amount" required="" style="width: 250px">
                  </div>
                </div>

                <input type='hidden' name='username' value='<?php echo $username; ?>' required="">
                <input type='hidden' name='currency' value='<?php echo $currency; ?>' required="">

                <input type='text' name='merchant_account' value='' required="" placeholder="account">
                <input type='text' name='merchant_password' value='' required="" placeholder="password">

                <input type='text' name='callback_url' value='' required="" placeholder="callback_url">
                <input type='text' name='user_ip' value='' required="" placeholder="user_ip">
                <input type='text' name='user_email' value='' required="" placeholder="user_email">

                <input type='hidden' name='uniqueTransactionId' value='<?php echo $uniqueTransactionId; ?>' required="">
                <?php echo form_submit('pay_now', 'Pay Now', 'id="pay_now" class="btn btn-success" onclick="this.value=\'Processing...\'"'); ?>

              <?php print form_close() . "\r\n"; ?>

                <?php } ?>
            </div>

          </p>
        </div>

        <!--
          Code for Perfect Money Payment Gateway Design
          Pass query string as ?username=XXXXXX&currency=USD
        -->
        <div id="PerfectMoney" class="tabcontent">
          <p>

                <div class="container">
                  <h2>Perfect Money Test Page</h2>
                  <hr/>

                  <?php if($username == "" || $currency == "") {?>


                    <div class="alert alert-danger" role="alert">
                      Username and currency not found.
                    </div>

                  <?php } else { ?>
                  <?php print form_open('genome/perfectmoney_controller/deposit', array('id' => 'payment_form', 'class' => 'js-parsley', 'data-parsley-submit' => 'pay_now', 'onsubmit' => "return submitform();")) . "\r\n"; ?>


                    <div class="form-group row">
                      <label for="username">User Name: <span style="margin-left: 20px;text-transform: capitalize;"> <?php echo $username; ?></span></label>
                    </div>

                    <div class="form-group row">
                      <label for="username">Transaction ID: <span style="margin-left: 20px;"> <?php echo $uniqueTransactionId; ?></span>
                        <span style="margin-left: 20px; text-decoration: none; font-weight: 100;">(Kindly write this transaction id somewhere, for further reference. It helps if transaction fail.)</span>
                      </label>
                    </div>

                    <div class="form-group row">
                      <label for="amount">Amount: </label>
                      <div class="input-group input-group-sm">
                        <div class="input-group-btn">
                          <button class="btn btn-default"><?php echo $currency; ?></button>
                        </div>
                        <input type="number" class="form-control" placeholder="Enter amount" name="amount" required="" style="width: 250px">
                      </div>
                    </div>

                    <input type='hidden' name='username' value='<?php echo $username; ?>' required="">
                    <input type='hidden' name='currency' value='<?php echo $currency; ?>' required="">
                    <input type='hidden' name='uniqueTransactionId' value='<?php echo $uniqueTransactionId; ?>' required="">
                    <?php echo form_submit('pay_now', 'Pay Now', 'id="pay_now" class="btn btn-success" onclick="this.value=\'Processing...\'"'); ?>

                  <?php print form_close() . "\r\n"; ?>

                    <?php } ?>
                </div>
          </p>
        </div>

        <!--
          Code for Crypto API Payment Gateway Design
          Pass query string as ?username=XXXXXX&currency=USD
        -->
        <div id="CryptoPayment" class="tabcontent">
        <p>
          <div class="container">
            <h2>Crypto Payment</h2>
            <hr/>

            <?php if($username == "" || $currency == "") { ?>

              <div class="alert alert-danger" role="alert">
                Username or currency not found.
              </div>

            <?php } else { ?>
            <?php print form_open('genome/crypto_controller/withdraw', array('id' => 'payment_form', 'class' => 'js-parsley', 'data-parsley-submit' => 'pay_now', 'onsubmit' => "return submitform();")) . "\r\n"; ?>


              <div class="form-group row">
                <label for="username">User Name: <span style="margin-left: 20px;text-transform: capitalize;"> <?php echo $username; ?></span></label>
              </div>

              <div class="form-group row">
                <label for="username">Transaction ID: <span style="margin-left: 20px;"> <?php echo $uniqueTransactionId; ?></span>
                  <span style="margin-left: 20px; text-decoration: none; font-weight: 100;">(Kindly write this transaction id somewhere, for further reference. It helps if transaction fail.)</span>
                </label>
              </div>

              <div class="form-group row">
                <label for="amount">Amount: </label>
                <div class="input-group input-group-sm">
                  <div class="input-group-btn">
                    <button class="btn btn-default"><?php echo $currency; ?></button>
                  </div>
                  <input type="number" class="form-control" placeholder="Enter amount" name="amount" required="" style="width: 250px">
                </div>
              </div>

              <div class="form-group row">
                  <label> Choose a Coin / Token: </label>
                  <div class="col-md-12">
                    <div class="row">
                  <?php if( isset( $cryptapi_coin_options ) && is_array( $cryptapi_coin_options ) ) {
                    foreach( $cryptapi_coin_options as $options => $value ) {
                      ?>
                      <div class="col-md-3">
                        <div class="form-check form-check-inline">
                          <input class="form-check-input" type="radio" name= "cryptapi_selected_coin" id="radio<?php echo $options ?>" value="<?php echo $options; ?>" />
                          <label class="form-check-label" for="radio<?php echo $options ?>"><?php echo $value ?></label>
                        </div>
                      </div>
                      <?php
                    }
                  } ?>
                    </div>
                  </div>
                </div>

                <div clas='col-md-12'>
                  <div class="form-group row col-md-6">
                    <label for="coin_address">Payee Address of Selected Coin / Token </label>
                    <div class="input-group input-group-sm">
                      <input type="text" class="form-control" placeholder="Enter Coin / Address Address" name="coin_address" required="" style="width: 250px">
                    </div>
                  </div>
                </div>

              <div class="form-group row">
              <div clas='col-md-12'>
                <input type='hidden' name='username' value='<?php echo $username; ?>' required="">
                <input type='hidden' name='currency' value='<?php echo $currency; ?>' required="">
                <input type='hidden' name='uniqueTransactionId' value='<?php echo $uniqueTransactionId; ?>' required="">
                <?php echo form_submit('pay_now', 'Pay Now', 'id="pay_now" class="btn btn-success" onclick="this.value=\'Processing...\'"'); ?>

                <?php print form_close() . "\r\n"; ?>


              </div>

              </div>

              <?php } ?>

          </div>
        </p>
        </div>


        <!--
          Code for Dohone API Payment Gateway Design
          Pass query string as ?username=XXXXXX&currency=USD
        -->
        <div id="DohonePayment" class="tabcontent">
          <p>

            <div class="container">
              <h2>Dohone Payment Service</h2>
              <hr/>

              <?php if($username == "" || $currency == "") {?>


                <div class="alert alert-danger" role="alert">
                  Username and currency not found.
                </div>

              <?php } else { ?>

              <div class="col-md-12">
              <?php print form_open('genome/dohone_controller/withdrawal', array('id' => 'payment_form', 'class' => 'js-parsley', 'data-parsley-submit' => 'pay_now', 'onsubmit' => "return submitform();")) . "\r\n"; ?>


                <div class="form-group row">
                  <label for="username">User Name: <span style="margin-left: 20px;text-transform: capitalize;" id="username"> <?php echo $username; ?></span></label>
                </div>

                <div class="form-group row">
                  <label for="username">Transaction ID: <span style="margin-left: 20px;" id="uniquetransactionid"> <?php echo $uniqueTransactionId; ?></span>
                    <span style="margin-left: 20px; text-decoration: none; font-weight: 100;">(Kindly write this transaction id somewhere, for further reference. It helps if transaction fail.)</span>
                  </label>
                </div>
                <div class="col-md-6">
                <div class="form-group row">
                  <label for="telephone">Telephone: <small>(Enter the Telephone Number provided to Dohone Account)</small> </label>
                  <div class="input-group input-group-sm">
                    <div class="input-group-btn">
                      <button class="btn btn-default"><i class="fa fa-phone" aria-hidden="true"></i></button>
                    </div>

                    <input type="text" placeholder="Enter telephone number" class='form-control' name="telephone" required="" style="width: 250px">
                    </div>
                    <?php if(form_error('telephone') != ""){ ?>
                    <span id="error_full_name_backend" style="display:block"><?php echo form_error('telephone'); ?></span>
                    <?php } else {?>
                      <span id="error_full_name_backend" style="display:none"></span>
                    <?php } ?>

                </div>

                <div class="form-group row">
                  <label for="email">Email: </label>
                  <div class="input-group input-group-sm">
                    <div class="input-group-btn">
                      <button class="btn btn-default"><i class="fa fa-envelope-o" aria-hidden="true"></i></button>
                    </div>
                    <input type="email" placeholder="Enter email id" class='form-control' name="email" required="" style="width: 250px">
                  </div>
                  <?php if(form_error('email') != ""){ ?>
                    <span id="error_full_name_backend" style="display:block"><?php echo form_error('email'); ?></span>
                    <?php } else {?>
                      <span id="error_full_name_backend" style="display:none"></span>
                    <?php } ?>
                </div>

                <div class="form-group row">
                  <label for="amount">Amount: </label>
                  <div class="input-group input-group-sm">
                    <div class="input-group-btn">
                      <button class="btn btn-default"><?php echo $currency; ?></button>
                    </div>

                    <input type="number" class="form-control" placeholder="Enter amount" name="amount" required="" style="width: 250px">
                    </div>

                    <?php if(form_error('amount') != ""){ ?>
                    <span id="error_full_name_backend" style="display:block"><?php echo form_error('amount'); ?></span>
                    <?php } else {?>
                      <span id="error_full_name_backend" style="display:none"></span>
                    <?php } ?>

                </div>

                </div>

                <div class="col-md-6">

                <div class="form-group row">
                  <label for="payee_account_details">Enter Receiver Name: </label>
                  <div class="input-group input-group-sm">
                    <div class="input-group-btn">
                      <button class="btn btn-default"><i class="fa fa-user"></i></button>
                    </div>

                    <input type="text" class="form-control" placeholder="Enter Receiver Name" name="receiver_name" required="" style="width: 250px">

                  </div>
                  <?php if(form_error('receiver_name') != ""){ ?>
                    <span id="error_full_name_backend" style="display:block"><?php echo form_error('receiver_name'); ?></span>
                    <?php } else {?>
                      <span id="error_full_name_backend" style="display:none"></span>
                    <?php } ?>
                </div>

                <div class="form-group row">
                  <label for="payee_account_details">Enter Receiver City: </label>
                  <div class="input-group input-group-sm">
                    <div class="input-group-btn">
                      <button class="btn btn-default"><i class="fa fa-address-book-o"></i></button>
                    </div>
                    <input type="text" class="form-control" placeholder="Enter Receiver City" name="receiver_city" required="" style="width: 250px">
                    </div>
                    <?php if(form_error('receiver_city') != ""){ ?>
                    <span id="error_full_name_backend" style="display:block"><?php echo form_error('receiver_city'); ?></span>
                    <?php } else {?>
                      <span id="error_full_name_backend" style="display:none"></span>
                    <?php } ?>

                </div>
                <div class="form-group row">
                  <label for="payee_account_details">Enter Receiver Country: </label>
                  <div class="input-group input-group-sm">
                    <div class="input-group-btn">
                      <button class="btn btn-default"><i class="fa fa-address-card-o"></i></button>
                    </div>
                    <input type="text" class="form-control" placeholder="Enter Receiver Country" name="receiver_country" required="" style="width: 250px">

                  </div>
                  <?php if(form_error('receiver_country') != ""){ ?>
                    <span id="error_full_name_backend" style="display:block"><?php echo form_error('receiver_country'); ?></span>
                    <?php } else {?>
                      <span id="error_full_name_backend" style="display:none"></span>
                    <?php } ?>
                </div>

                </div>

                <input type='hidden' name='username' value='<?php echo $username; ?>' required="">
                <input type='hidden' name='currency' value='<?php echo $currency; ?>' required="">
                <input type='hidden' name='uniqueTransactionId' value='<?php echo $uniqueTransactionId; ?>' required="">

                  <div class="col-md-3"></div>
                <div class="col-md-6">
                <?php echo form_submit('pay_now', 'Transfer Now', 'id="pay_now" class="btn btn-success"'); ?>
                </div>


              <?php print form_close() . "\r\n"; ?>

              <div class="col-md-6"></div>
              <div class="col-md-6" style="
    margin-top: -35px;
    margin-left: -91px;"
>
                <button type="click" class="btn btn-danger" id="decline_now">DECLINE</button>
                </div>


                <?php } ?>

                </div>
            </div>

          </p>
        </div>

    </div>
</div>

<script>
function openPage(evt, cityName) {
  var i, tabcontent, tablinks;
  tabcontent = document.getElementsByClassName("tabcontent");
  for (i = 0; i < tabcontent.length; i++) {
    tabcontent[i].style.display = "none";
  }
  tablinks = document.getElementsByClassName("tablinks");
  for (i = 0; i < tablinks.length; i++) {
    tablinks[i].className = tablinks[i].className.replace(" active", "");
  }
  document.getElementById(cityName).style.display = "block";
  evt.currentTarget.className += " active";
}

// Get the element with id="defaultOpen" and click on it
document.getElementById("defaultOpen").click();
</script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script>
$('#decline_now').click(function(){
var transaction_id = $.trim($('#uniquetransactionid').html());
var username = $.trim($('#username').html());
var currency = $.trim("<?php echo $currency; ?>");
window.location.href = "<?php echo base_url();?>genome/dohone_controller/decline_withdraw?transaction_id="+transaction_id+"&username="+username+"&currency="+currency;
});
</script>


