<?php
    $total = $customproduct['amount'];
    $currency_symbol = $customproduct['currency'];
    $crypto_coin = $cryptapi_currency;
    $address_in = $cryptapi_address_in;
    $crypto_value = $cryptapi_total;
    $currency_uri_first = str_replace(' ','',strtolower($cryptapi_coin_n_tokens[$cryptapi_currency]));
    $currency_amount_in = 'amount';
    
    $show_crypto_coin = $crypto_coin;
    if ($show_crypto_coin == 'iota') $show_crypto_coin = 'miota';

    $COIN_MULTIPLIERS = $COIN_MULTIPLIERS;

    $qr_value = $crypto_value;
    if (in_array( $crypto_coin, array('eth', 'iota') ) ) $qr_value = ($crypto_value * $COIN_MULTIPLIERS[$crypto_coin]);

    if (in_array( $crypto_coin, array('usdt', 'usdc', 'busd', 'pax', 'tusd', 'bnb', 'link', 'cro', 'mkr', 'nexo', 'bcz' ) ) ) {
        $qr_value = ($crypto_value * $COIN_MULTIPLIERS[$crypto_coin]);
        $crypto_erc_address = $ERC_TOKENS_URI[$crypto_coin];
        $currency_uri_first = 'ether';
       }
    
    if( $crypto_coin == 'bch' ) {
        if (strpos($address_in, $currency_uri_first.':') !== false) {
            $address_in = str_replace($currency_uri_first.':','',$address_in);
        }
    }
    if( $crypto_coin == 'eth' ) {
        $qr_value = number_format($qr_value);
        if (strpos($qr_value, ',') !== false) {
            $qr_value = str_replace(',','',$qr_value);
        }
        $currency_amount_in = 'value';
    }

    if( $qr_value > 1000 ) {

        $qr_value = number_format($qr_value);
        if (strpos($qr_value, ',') !== false) {
            $qr_value = str_replace(',','',$qr_value);
        }
    }
    
?>
    <div class="row">
        <div class="panel-footer" style="background-color:#222d32;height:40px;">
            <div class="row">
                <div class="box-header" style="width:100%;">
                    <div class="col-md-12">
                        <p style="text-align: right; color:#fff;padding-top: 9px;">Bitlogik Deposit Payment Service</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div style='text-align:center'>
        <h3>We have Recieved your Payment request, It will be approved after reviewing of Bitlogic Team.</h3>
        <p><strong>Amount</strong> : <?php echo $total; ?></p>
        <p><span><strong>Currency</strong> : <?php echo $currency_symbol; ?></span></p>
        <p><strong>Coin</strong> : <?php echo $cryptapi_currency . ' ( ' .( $cryptapi_coin_n_tokens[$cryptapi_currency] ) . ' ) '; ?></p>
        <p><span><strong>Payee(Your) Address</strong> : <?php echo $crypto_coin_address; ?></span></p>
        <p><strong>Amount in Crypto Currency</strong> : <?php echo $crypto_value; ?></p>
        <p><span><strong>Transaction Id (Note it down for future references )</strong> : <?php echo $invoice_id; ?></span></p>
    </div>
    <div class="row wrap header-fixed">
        <div class="panel-footer" style="background-color:#222d32;height:40px;width:100%;bottom:0;position: inherit;margin-top:50px;"> 
            <div class="row">
                <div class="box-header" style="width:100%;">
                    <div class="col-md-12">
                        <p style="text-align: center;color:#fff;padding-top: 9px; ">Designed &amp; Developed By Dezzex Technology Pvt. Ltd. </p>
                    </div>
                </div>
            </div>
        </div>
    </div>