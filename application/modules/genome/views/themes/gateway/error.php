
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
<link href="https://fonts.googleapis.com/css?family=Nunito+Sans:400,400i,700,900&display=swap" rel="stylesheet">
<style>
      body {
        text-align: center;
        padding: 40px 0;
        background: #EBF0F5;
      }
        h1 {
          color: #f0414c;
          font-family: "Nunito Sans", "Helvetica Neue", sans-serif;
          font-weight: 900;
          font-size: 40px;
          margin-bottom: 10px;
        }
        p {
          color: #404F5E;
          font-family: "Nunito Sans", "Helvetica Neue", sans-serif;
          font-size:20px;
          margin: 0;
        }
      i {
        color: #f0414c;
        font-size: 100px;
        line-height: 200px;
        margin-left:-15px;
      }
      .card {
        background: white;
        padding: 60px;
        border-radius: 4px;
        box-shadow: 0 2px 3px #C8D0D8;
        display: inline-block;
        margin: 0 auto;
        width: 440px;
        height: 450px;
      }
    </style>



<div class="container">
    <div class="form-group row">
      <div class="col-sm-6">

      <div class="card">
          <table class="table table" style="width: 100%">
            <tr>
            <th>Transaction No.</th>
            <td><?php echo $transactionId;?></td>
          </tr>
          <tr>
            <th>User ID</th>
            <td><?php echo $uniqueUserId;?></td>
          </tr>
          <tr>
            <th>Amount</th>
            <td><?php echo $totalAmount;?></td>
          </tr>
          <tr>
            <th>Currency</th>
            <td><?php echo $currency;?></td>
          </tr>
          </table>
        </div>

      </div>
      <div class="col-sm-6">

        <div class="card">
            <div style="border-radius:200px; height:200px; width:200px; background: #f5e4e7; margin:0 auto;">
              <i class="checkmark">✘</i>
            </div>
            <h1>Failed</h1>
            <p><?php echo $message;?>.<br/><br/> <a href="<?php echo base_url(); ?>index.php/withdraw/dashboard?username=<?php echo $uniqueUserId;?>&currency=<?php echo $currency;?>" class="btn btn-primary">Return to home</a></p>
        </div>

      </div>
    </div>
</div>
