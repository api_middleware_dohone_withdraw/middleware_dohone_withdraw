<?php if (!defined('BASEPATH')) exit('No direct script access allowed'); ?>

<?php $this->load->view('themes/' . Settings_model::$db_config['active_theme'] . '/partials/content_head.php'); ?>

<?php $this->load->view('generic/flash_error'); ?>

<style>
* {box-sizing: border-box}
body {font-family: "Lato", sans-serif;}

/* Style the tab */
.tab {
  float: left;
  border: 1px solid #ccc;
  background-color: #f1f1f1;
  width: 20%;
  height: 100%;
}

/* Style the buttons inside the tab */
.tab button {
  display: block;
  background-color: inherit;
  color: black;
  padding: 22px 16px;
  width: 100%;
  border: none;
  outline: none;
  text-align: left;
  cursor: pointer;
  transition: 0.3s;
  font-size: 17px;
}

/* Change background color of buttons on hover */
.tab button:hover {
  background-color: #ddd;
}

/* Create an active/current "tab button" class */
.tab button.active {
  background-color: #000;
  color: #fff;
}

/* Style the tab content */
.tabcontent {
  float: left;
  background-color: #fff;
  padding: 0px 12px;
  border: 1px solid #ccc;
  width: 80%;
  border-left: none;
  height: 100%;
}
</style>

<div class="row">
    <div class="panel-footer" style="background-color:#222d32;">
        <div class="row">
            <div class="box-header" style="width:100%;">
                <div class="col-md-12">
                    <p style="text-align: right; color:#fff;">Bitlogik Deposit Payment Service</p>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-sm-12">
        <h2>Select a Payment Service</h2>
        <div class="tab">
          <button class="tablinks" onclick="openPage(event, 'Genome')" id="defaultOpen">Genome</button>
          <button class="tablinks" onclick="openPage(event, 'PerfectMoney')">Perfect Money</button>
          <button class="tablinks" onclick="openPage(event, 'CryptoPayment')">Crypto Payment</button>
          <button class="tablinks" onclick="openPage(event, 'DohonePayment')">Dohone Payment</button>
        </div>

        <!--
          Code for Genome Payment Gateway Design
          Pass query string as ?username=XXXXXX&currency=USD
        -->
        <div id="Genome" class="tabcontent">
          <p>

            <div class="container">
              <h2>Genome Payment Service Test Page</h2>
              <hr/>

              <?php if($username == "" || $currency == "") {?>


                <div class="alert alert-danger" role="alert">
                  Username and currency not found.
                </div>

              <?php } else { ?>
              <?php print form_open('genome/genome_controller/deposit', array('id' => 'payment_form', 'class' => 'js-parsley', 'data-parsley-submit' => 'pay_now', 'onsubmit' => "return submitform();")) . "\r\n"; ?>


                <div class="form-group row">
                  <label for="username">User Name: <span style="margin-left: 20px;text-transform: capitalize;"> <?php echo $username; ?></span></label>
                </div>

                <div class="form-group row">
                  <label for="username">Transaction ID: <span style="margin-left: 20px;"> <?php echo $uniqueTransactionId; ?></span>
                    <span style="margin-left: 20px; text-decoration: none; font-weight: 100;">(Kindly write this transaction id somewhere, for further reference. It helps if transaction fail.)</span>
                  </label>
                </div>

                <div class="form-group row">
                  <label for="amount">Amount: </label>
                  <div class="input-group input-group-sm">
                    <div class="input-group-btn">
                      <button class="btn btn-default"><?php echo $currency; ?></button>
                    </div>
                    <input type="number" class="form-control" placeholder="Enter amount" name="amount" required="">
                  </div>
                </div>

                <input type='hidden' name='username' value='<?php echo $username; ?>' required="">
                <input type='hidden' name='currency' value='<?php echo $currency; ?>' required="">
                <input type='hidden' name='uniqueTransactionId' value='<?php echo $uniqueTransactionId; ?>' required="">
                <?php echo form_submit('pay_now', 'Pay Now', 'id="pay_now" class="btn btn-success" onclick="this.value=\'Processing...\'"'); ?>

              <?php print form_close() . "\r\n"; ?>

                <?php } ?>
            </div>

          </p>
        </div>

        <!--
          Code for Perfect Money Payment Gateway Design
          Pass query string as ?username=XXXXXX&currency=USD
        -->
        <div id="PerfectMoney" class="tabcontent">
          <p>

                <div class="container">
                  <h2>Perfect Money Test Page</h2>
                  <hr/>

                  <?php if($username == "" || $currency == "") {?>


                    <div class="alert alert-danger" role="alert">
                      Username and currency not found.
                    </div>

                  <?php } else { ?>
                  <?php print form_open('genome/perfectmoney_controller/deposit', array('id' => 'payment_form', 'class' => 'js-parsley', 'data-parsley-submit' => 'pay_now', 'onsubmit' => "return submitform();")) . "\r\n"; ?>


                    <div class="form-group row">
                      <label for="username">User Name: <span style="margin-left: 20px;text-transform: capitalize;"> <?php echo $username; ?></span></label>
                    </div>

                    <div class="form-group row">
                      <label for="username">Transaction ID: <span style="margin-left: 20px;"> <?php echo $uniqueTransactionId; ?></span>
                        <span style="margin-left: 20px; text-decoration: none; font-weight: 100;">(Kindly write this transaction id somewhere, for further reference. It helps if transaction fail.)</span>
                      </label>
                    </div>

                    <div class="form-group row">
                      <label for="amount">Amount: </label>
                      <div class="input-group input-group-sm">
                        <div class="input-group-btn">
                          <button class="btn btn-default"><?php echo $currency; ?></button>
                        </div>
                        <input type="number" class="form-control" placeholder="Enter amount" name="amount" required="">
                      </div>
                    </div>

                    <input type='hidden' name='username' value='<?php echo $username; ?>' required="">
                    <input type='hidden' name='currency' value='<?php echo $currency; ?>' required="">
                    <input type='hidden' name='uniqueTransactionId' value='<?php echo $uniqueTransactionId; ?>' required="">
                    <?php echo form_submit('pay_now', 'Pay Now', 'id="pay_now" class="btn btn-success" onclick="this.value=\'Processing...\'"'); ?>

                  <?php print form_close() . "\r\n"; ?>

                    <?php } ?>
                </div>
          </p>
        </div>

        <!--
          Code for Crypto API Payment Gateway Design
          Pass query string as ?username=XXXXXX&currency=USD
        -->
        <div id="CryptoPayment" class="tabcontent">
        <p>
          <div class="container">
            <h2>Crypto Payment</h2>
            <hr/>

            <?php if($username == "" || $currency == "") { ?>

              <div class="alert alert-danger" role="alert">
                Username or currency not found.
              </div>

            <?php } else { ?>
            <?php print form_open('genome/crypto_controller/deposit', array('id' => 'payment_form', 'class' => 'js-parsley', 'data-parsley-submit' => 'pay_now', 'onsubmit' => "return submitform();")) . "\r\n"; ?>


              <div class="form-group row">
                <label for="username">User Name: <span style="margin-left: 20px;text-transform: capitalize;"> <?php echo $username; ?></span></label>
              </div>

              <div class="form-group row">
                <label for="username">Transaction ID: <span style="margin-left: 20px;"> <?php echo $uniqueTransactionId; ?></span>
                  <span style="margin-left: 20px; text-decoration: none; font-weight: 100;">(Kindly write this transaction id somewhere, for further reference. It helps if transaction fail.)</span>
                </label>
              </div>

              <div class="form-group row">
                <label for="amount">Amount: </label>
                <div class="input-group input-group-sm">
                  <div class="input-group-btn">
                    <button class="btn btn-default"><?php echo $currency; ?></button>
                  </div>
                  <input type="number" class="form-control" placeholder="Enter amount" name="amount" required="">
                </div>
              </div>

              <div class="form-group row">
                  <label> Choose a Coin / Token: </label>
                  <div class="col-md-12">
                    <div class="row">
                  <?php if( isset( $cryptapi_coin_options ) && is_array( $cryptapi_coin_options ) ) {
                    foreach( $cryptapi_coin_options as $options => $value ) {
                      ?>
                      <div class="col-md-3">
                        <div class="form-check form-check-inline">
                          <input class="form-check-input" type="radio" name= "cryptapi_selected_coin" id="radio<?php echo $options ?>" value="<?php echo $options; ?>" />
                          <label class="form-check-label" for="radio<?php echo $options ?>"><?php echo $value ?></label>
                        </div>
                      </div>
                      <?php
                    }
                  } ?>
                    </div>
                  </div>
                </div>

              <div class="form-group row">
                <div clas='col-md-12'>
                    <?php $invoice_id = rand(10,1000); ?>
                    <input type='hidden' name='invoice_id' value='<?php echo $invoice_id; ?>' required="">
                    <input type='hidden' name='username' value='<?php echo $username; ?>' required="">
                    <input type='hidden' name='currency' value='<?php echo $currency; ?>' required="">
                    <input type='hidden' name='uniqueTransactionId' value='<?php echo $uniqueTransactionId; ?>' required="">
                    <?php echo form_submit('pay_now', 'Pay Now', 'id="pay_now" class="btn btn-success" onclick="this.value=\'Processing...\'"'); ?>

                  <?php print form_close() . "\r\n"; ?>

              </div>
            </div>
              <?php } ?>

              </div>
        </p>
        </div>


         <!--
          Code for Dohone API Payment Gateway Design
          Pass query string as ?username=XXXXXX&currency=USD
        -->
        <div id="DohonePayment" class="tabcontent">
          <p>

            <div class="container">
              <h2>Dohone Payment Service Test Page</h2>
              <hr/>

              <?php if($username == "" || $currency == "") {?>


                <div class="alert alert-danger" role="alert">
                  Username and currency not found.
                </div>

              <?php } else { ?>
              <?php print form_open('genome/dohone_controller/deposit', array('id' => 'payment_form', 'class' => 'js-parsley', 'data-parsley-submit' => 'pay_now', 'onsubmit' => "return submitform();")) . "\r\n"; ?>

                <div class="form-group row">
                  <label for="username">User Name: <span style="margin-left: 20px;text-transform: capitalize;"> <?php echo $username; ?></span></label>
                </div>

                <div class="form-group row">
                  <label for="username">Transaction ID: <span style="margin-left: 20px;"> <?php echo $uniqueTransactionId; ?></span>
                    <span style="margin-left: 20px; text-decoration: none; font-weight: 100;">(Kindly write this transaction id somewhere, for further reference. It helps if transaction fail.)</span>
                  </label>
                </div>

                <div class="form-group row">
                  <label for="telephone">Telephone: </label>
                  <div class="input-group input-group-sm">
                    <div class="input-group-btn">
                      <button class="btn btn-default"><i class="fa fa-phone" aria-hidden="true"></i></button>
                    </div>
                    <input type="text" placeholder="Enter telephone number" class='form-control' name="telephone" required="" style="width: 250px">
                    <input type="hidden" value='693467308'>
                  </div>
                </div>

                <div class="form-group row">
                  <label for="email">Email: </label>
                  <div class="input-group input-group-sm">
                    <div class="input-group-btn">
                      <button class="btn btn-default"><i class="fa fa-envelope-o" aria-hidden="true"></i></button>
                    </div>
                    <input type="email" placeholder="Enter email id" class='form-control' name="email" required="" style="width: 250px">
                  </div>
                </div>

                <div class="form-group row">
                  <label for="amount">Amount: </label>
                  <div class="input-group input-group-sm">
                    <div class="input-group-btn">
                      <button class="btn btn-default"><?php echo $currency; ?></button>
                    </div>
                    <input type="number" class="form-control" placeholder="Enter amount" name="amount" required="" style="width: 250px">
                  </div>
                </div>


                <input type='hidden' name='username' value='<?php echo $username; ?>' required="">
                <input type='hidden' name='currency' value='<?php echo $currency; ?>' required="">
                <input type='hidden' name='uniqueTransactionId' value='<?php echo $uniqueTransactionId; ?>' required="">
                <?php echo form_submit('pay_now', 'Pay Now', 'id="pay_now" class="btn btn-success" onclick="this.value=\'Processing...\'"'); ?>

              <?php print form_close() . "\r\n"; ?>

                <?php } ?>
            </div>

          </p>
        </div>

    </div>
</div>


<script>
function openPage(evt, cityName) {
  var i, tabcontent, tablinks;
  tabcontent = document.getElementsByClassName("tabcontent");
  for (i = 0; i < tabcontent.length; i++) {
    tabcontent[i].style.display = "none";
  }
  tablinks = document.getElementsByClassName("tablinks");
  for (i = 0; i < tablinks.length; i++) {
    tablinks[i].className = tablinks[i].className.replace(" active", "");
  }
  document.getElementById(cityName).style.display = "block";
  evt.currentTarget.className += " active";
}

// Get the element with id="defaultOpen" and click on it
document.getElementById("defaultOpen").click();
</script>


