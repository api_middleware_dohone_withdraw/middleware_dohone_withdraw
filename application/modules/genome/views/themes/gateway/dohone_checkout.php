<form method="post" action="https://www.my-dohone.com/dohone/pay" id='paymentForm'>
    <input type="hidden" name="cmd" value="start">
    <input type="hidden" name="rN" value="<?php echo $username ?>">
    <input type="hidden" name="rT" value="<?php echo $telephone ?>">
    <input type="hidden" name="rE" value="<?php echo $email ?>">
    <input type="hidden" name="rH" value="<?php echo $merchant_code; ?>">
    <input type="hidden" name="rI" value="<?php echo $uniqueTransactionId ?>">
    <input type="hidden" name="rMt" value="<?php echo $amount ?>">
    <input type="hidden" name="rDvs" value="<?php echo $currency ?>">
    <input type="hidden" name="source" value="BitLogic Middleware">
    <input type="hidden" name="endPage" value="<?php echo $endPage ?>">
    <input type="hidden" name="notifyPage" value="<?php echo $notifyPage ?>">
    <input type="hidden" name="cancelPage" value="<?php echo $cancelPage ?>">
</form>

<script>
	window.onload = function(){
	  document.forms['paymentForm'].submit();
	}
</script>