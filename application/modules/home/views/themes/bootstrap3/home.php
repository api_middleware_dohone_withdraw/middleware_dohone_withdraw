<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>
<div class="row" style="margin-top: -18px;">  
    <div class="jquery-script-clear"></div>
    <div id="container">    
        <div class="slider_container">
          <ul>
            <li class="current_slide"><img src="<?php print base_url(); ?>assets/slider/slides/1.jpg" alt="slide image"></li>
            <li><img src="<?php print base_url(); ?>assets/slider/slides/2.jpg" alt="slide image"></li>
            <li><img src="<?php print base_url(); ?>assets/slider/slides/3.jpg" alt="slide image"></li>
            <li><img src="<?php print base_url(); ?>assets/slider/slides/4.jpg" alt="slide image"></li>
            <li><img src="<?php print base_url(); ?>assets/slider/slides/5.jpg" alt="slide image"></li>
            <li><img src="<?php print base_url(); ?>assets/slider/slides/3.jpg" alt="slide image"></li>
        </ul>

        <span class="prev_slide"><i class="fa fa-angle-left" aria-hidden="true"></i></span>
        <span class="next_slide"><i class="fa fa-angle-right" aria-hidden="true"></i></span>

        <div class="buttons"></div>

        <span class="play_pause"><i class="fa fa-pause"></i></span>
    </div>
</div>

</div>

</div>
<div class="row" style="margin-top:10px;">

    <nav class="navbar navbar-default">

        <marquee behavior="scroll" direction="left" onmouseover="this.stop();" onmouseout="this.start();">




         <?php 
		 if($data != null)
         for ($i = 1; $i <= count($data); $i++)
         {
            if ($data[$i]['p_notice_typ'] == 4)    
                                    // are we allowed to see this page_data?
            {
                ?>

                <i class="glyphicon glyphicon-hand-right" style="font-size: 17px;margin:10px;color: red;"></i><?php  echo ''.$data[$i]['p_title'];

            }
        } ?>

    </marquee>

</nav>

</div>
<!--sidebar and content-->
<div class="row" id="content">
    <!--left sidebar-->
    <div class="col-sm-4">
        <div class="panel panel-primary" style="border-color: #337ab7; min-height: 370px;">                    
            <div class="panel-heading" style="padding:1px 0px;text-align: center;">
                <h3 style="margin-top: 10px;">News & Event</h3>
            </div>

            <marquee id="mymarquee" behavior="scroll" direction="up" height="300px" scrollamount="2" onmouseover="this.stop();" onmouseout="this.start();">
              <dl class="marquee" style="color:red; margin-left: 10px;">


                 <?php 
                            /*echo "<pre>";
                            print_r($data);*/
								 if($data != null)
                            for ($i = 1; $i <= count($data); $i++)
                            {
                                if ($data[$i]['p_notice_typ'] == 1)    
                                    // are we allowed to see this page_data?
                                {
                                    ?>

                                    <i class="fa fa-hand-o-right"></i>
                                    <?php 
                                    $link = base_url("home/view_content");
                                    echo ' <a href='.$link.'/'.$data[$i]['p_id'].' target="_blank">'.$data[$i]['p_title'].'</a></br>';

                        /*echo anchor("home/view_content/".$data[$i]['p_id'],
                             $data[$i]['p_title'] ,
                               array('target' => '_blank') // or['target' => '_blank']  
                               ); */?>


                               <?php 
                           }
                       } ?>
                   </dl>
               </marquee>

           </div>
       </div>      
       <div class="col-sm-4">
        <div class="panel panel-primary" style="border-color: #337ab7; min-height: 370px;">                    
            <div class="panel-heading" style="padding:1px 0px;text-align: center;">
                <h3 style="margin-top: 10px;">Notices & Circulars</h3>
            </div>

            <marquee id="mymarquee" behavior="scroll" direction="up" height="300px" scrollamount="2" onmouseover="this.stop();" onmouseout="this.start();">
              <dl class="marquee" style="color:red; margin-left: 10px;">
                         <!-- echo "<pre>";
                         print_r($value); -->
                         <?php 
                            /*echo "<pre>";
                            print_r($data);*/
								 if($data != null)
                            for ($i = 1; $i <= count($data); $i++)
                            {
                                if ($data[$i]['p_notice_typ'] == 2)    
                                    // are we allowed to see this page_data?
                                {
                                    ?>

                                    <i class="fa fa-hand-o-right"></i>
                                    <?php 
                                    $link = base_url("home/view_content");
                                    echo ' <a href='.$link.'/'.$data[$i]['p_id'].' target="_blank">'.$data[$i]['p_title'].'</a></br>';

                        /*echo anchor("home/view_content/".$data[$i]['p_id'],
                             $data[$i]['p_title'] ,
                               array('target' => '_blank') // or['target' => '_blank']  
                               ); */?>


                               <?php 
                           }
                       } ?>
                   </dl>
               </marquee>

           </div>
       </div>                

       <!--right sidebar-->   
       <div class="col-sm-4">
        <div class="panel panel-primary" style=" border-color: #337ab7; height: 370px;margin: 0!important;">                    
            <div class="panel-heading" style="padding:1px 0px;text-align: center;">
                <h3 style="margin-top: 10px;">Schemes</h3>
            </div>

            <marquee id="mymarquee" behavior="scroll" direction="up" height="300px" scrollamount="2" onmouseover="this.stop();" onmouseout="this.start();">
              <dl class="marquee" style="color:red; margin-left: 10px;">

                <?php 
                            /*echo "<pre>";
                            print_r($data);*/
								 if($data != null)
                            for ($i = 1; $i <= count($data); $i++)
                            {
                                if ($data[$i]['p_notice_typ'] == 3)    
                                    // are we allowed to see this page_data?
                                {
                                    ?>
                                    <i class="fa fa-hand-o-right"></i>
                                    <?php 
                                    $link = base_url("home/view_content");
                                    echo ' <a href='.$link.'/'.$data[$i]['p_id'].' target="_blank">'.$data[$i]['p_title'].'</a></br>';

                        /*echo anchor("home/view_content/".$data[$i]['p_id'],
                             $data[$i]['p_title'] ,
                               array('target' => '_blank') // or['target' => '_blank']  
                               ); */?>

                               <?php 
                           }
                       } ?>
                   </dl>
               </marquee>

           </div>
       </div> 


   </div>


