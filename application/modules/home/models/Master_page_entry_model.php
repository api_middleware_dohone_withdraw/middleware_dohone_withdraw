<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Master_page_entry_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public function get_uploaded_document_content($id = FALSE)
    {
        if (!$id) {
            return FALSE;
        }
        $this->db->select('page_id,page_title,CONVERT(page_file USING utf8) as file');
        $this->db->where('page_id',$id);
        $query = $this->db->get(DB_PREFIX .'master_page_entry');
        if ($query->num_rows() > 0) {
            return $query->row();
        }
        return FALSE;

    }

    public function get_uploaded_img($id = FALSE)
    {
        if (!$id) {
            return FALSE;
        }
        $this->db->select('page_id,page_title,CONVERT(page_file USING utf8) as file');
        $this->db->where('page_id',$id);
        $query = $this->db->get(DB_PREFIX .'master_page_entry');
        if ($query->num_rows() > 0) {
            return $query->row();
        }
        return FALSE;

    }


    /**
     *
     * get_page_data
     *
     * @param string $tablename ,$column,$where, $order_by=null 
     * @param string $sort_order = "asc", $limit = 0, $offset = 0 
     * @return mixed
     *
     */

   /* Code for retreieve data in list table */
    public function get_page_data($tablename, $column, $where, $order_by=null, $sort_order = "asc", $limit = 0, $offset = 0)
    {
        $fields = $this->db->list_fields(DB_PREFIX .$tablename);

        $this->db->select($column);
        $this->db->from(DB_PREFIX .$tablename);
        
        if(isset($where))
            $this->db->where($where);

        if(isset($order_by))
            $this->db->order_by($order_by, $sort_order);

        $this->db->limit($limit, $offset);

        $q = $this->db->get();
        
        if($q->num_rows() > 0) {
            return $q;
        }

        return false;
    }


}

