<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dashboard_controller extends Site_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->helper('form');
        $this->load->library('form_validation');
        $this->load->model('Dashboard_model');
    }

    public function index() {
        
        $this->quick_page_setup(Settings_model::$db_config['active_theme'], 'main', 'Dashbord', 'dashboard', 'header', 'footer');
    }
}