<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class API_controller extends Site_Controller {

    public function index()
    {
        $this->load->helper('url');

        $this->load->view('api_server');
    }
}
