<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>

<?php $this->load->view('themes/'. Settings_model::$db_config['active_theme'] .'/partials/content_head.php'); ?>

<script src="<?php print base_url(); ?>assets/js/sha256-profile.js" ></script>

<script type="text/javascript">
    function submitform() {
        var SHA256Password = SHA256(document.getElementById("password").value);
        document.getElementById("password").value = SHA256Password;
        var f = document.getElementsByTagName('form')[0];
        if(f.checkValidity()) {
            f.submit();
        } else {
            document.getElementById('login_submit').value = "Log In";
        }
    }
</script>

<div class="row" style="margin-top: 10%">

    <div class="col-sm-6 col-sm-offset-3 col-md-4 col-md-offset-4">

        <?php
        $this->load->view('generic/flash_error');
        ?>


        <div class="page-title mg-b-20">
            <h2 class="f900 mg-0 pd-t-20 pd-b-5"><?php print $template['title']; ?></h2>
        </div>
        <?php print form_open('auth/login/validate', array('id' => 'login_form', 'class' => 'js-parsley mg-b-15', 'data-parsley-submit' => 'login_submit', 'onsubmit' => "return submitform();")) . "\r\n"; ?>


        <div class="form-group">
            <input type="text"
            name="identification"
            id="identification"
            class="form-control input-lg"
            placeholder="<?php print sprintf((Settings_model::$db_config['allow_login_by_email'] == true ? $this->lang->line('login_identification') : $this->lang->line('login_username')), (Settings_model::$db_config['allow_login_by_email'] == true ? " or email" : "")); ?>"
            data-parsley-trigger="change keyup"
            data-parsley-minlength="5"
            data-parsley-maxlength="255"
            required>
        </div>

        <div class="form-group">
            <input type="password"
            name="password"
            id="password"
            class="form-control input-lg"
            placeholder="<?php print $this->lang->line('login_password'); ?>"
            required>
        </div>

        <div class="form-group row">
            <div class="input-group">
                <div class="input-group-addon" style="color: black; font-size: 20px; font-family: monospace;">Enter Result: <?php echo $captcha; ?> =</div>
                <input type="text"
                    name="captext"
                    id="captext"
                    class="form-control input-lg" maxlength="2" placeholder="?" 
                    required>
            </div>
        </div>


        <div class="form-group">
            <?php if (Settings_model::$db_config['remember_me_enabled'] == true) { ?>
                <div class="app-checkbox">
                    <label class="pd-r-10">
                        <?php print form_checkbox(array('name' => 'remember_me', 'id' =>'remember_me', 'class' => 'js-select-all-members', 'value' => 'accept', 'checked' => FALSE)); ?>
                        <span class="fa fa-check"></span> <?php print $this->lang->line('login_remember_me'); ?>
                    </label>
                </div>
                <?php } ?>
            </div>

            <?php
            if ($this->session->userdata('login_attempts') == false) {
                $v = 0;
            }else{
                $v = $this->session->userdata('login_attempts');
            }

            if ($v >= Settings_model::$db_config['login_attempts']) {
                if (Settings_model::$db_config['recaptchav2_enabled'] == true) {
                    ?><div class="mg-b-15 mg-t-15"><?php
                    echo $this->recaptchav2->render();
                    ?></div><?php
                }
            }
            ?>

            <div class="form-group">
                <?php echo form_submit('login_submit', 'Log In', 'id="login_submit" class="login_submit btn btn-primary btn-lg btn-block" onclick="this.value=\'Validate Login...\';"'); ?>
            </div>

            <?php if (Settings_model::$db_config['previous_url_after_login']) { ?>
                <input type="hidden" name="previous_url" value="<?php print base64_encode($this->session->flashdata('previous_url')); ?>">
                <?php } ?>

                <?php print form_close() ."\r\n"; ?>
            </div>

        </div>