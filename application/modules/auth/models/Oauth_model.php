<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class OAuth_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    /**
     *
     * get_all_providers
     *
     * @return mixed
     *
     */

    public function get_all_providers() {
        $this->db->select('id, name, oauth_type, client_id, client_secret')
            ->from(DB_PREFIX .'oauth_providers')
            ->where('enabled', true)
            ->order_by('order', 'asc');
        $q = $this->db->get();

        if ($q->num_rows() > 0) {
            return $q->result();
        }

        return false;
    }

    /**
     *
     * get_provider_data
     *
     * @param string $provider
     * @return mixed
     *
     */

    public function get_provider_data($provider) {
        $this->db->select('client_id, client_secret, enabled')->from(DB_PREFIX .'oauth_providers')->where('name', $provider);
        $q = $this->db->get();

        if ($q->num_rows() == 1) {
            return $q->row();
        }

        return false;
    }

    /**
     *
     * get_userdata_by_email
     *
     * @param string $email
     * @return mixed
     *
     */

    public function get_userdata_by_email($email) {
        $this->db->select('u.user_id, u.username, u.active, u.banned, ucp.cookie_part')
            ->from(DB_PREFIX .'user u')
            ->join(DB_PREFIX .'user_cookie_part ucp', 'ucp.user_id = u.user_id')
            ->where('u.email', $email);
        $q = $this->db->get();
        if ($q->num_rows() == 1) {
            return $q->row();
        }

        return false;
    }

}