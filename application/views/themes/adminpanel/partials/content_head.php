<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>

<?php $breadcrumb =  array();

$breadcrumb = $template['breadcrumbs'];

if (!empty($breadcrumb)) { ?>
<div class="page-title mg-b-20">
	<h2 class="f000 mg-0 pd-t-20 pd-b-5" style="color:#000"><?php print $template['title']; ?></h2>
	<ol class="breadcrumb pd-0 mg-b-5">
	<?php	foreach ($breadcrumb as $key => $value) { ?>
			<li><a href="<?php print base_url($value['uri']); ?>"><?php print $value['name'] ?></a></li>
	<?php } ?>
			
	</ol>
</div>	
<?php } ?>
