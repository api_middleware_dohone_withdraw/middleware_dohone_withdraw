<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>

<ul id="demo1" class="menu">

<?php
$base_url = base_url() . "index.php/";
?>

<!--
=================== ADMIN LEVEL ==================================
-->

<?php if (Site_Controller::check_roles(array(1))) {  ?>

    <li<?php print ((Site_Controller::$page == "dashboard") ? ' class="open"' : ""); ?>><a href="<?php print $base_url; ?>adminpanel/dashboard"><span class="menu-link-icon fa fa-dashboard"></span> <span class="menu-link-title"><?php print $this->lang->line('menu_dashboard'); ?></span></a></li>
    <li<?php print ((Site_Controller::$page == "dashboard2") ? ' class="open"' : ""); ?>><a href="<?php print $base_url; ?>adminpanel/dashboard/dashboard2"><span class="menu-link-icon fa fa-cogs"></span> <span class="menu-link-title"><?php print $this->lang->line('menu_dashboard'); ?>-2</span></a>
    <li<?php print ((Site_Controller::$page == "site_settings") ? ' class="open"' : ""); ?>><a href="<?php print $base_url; ?>adminpanel/site_settings" style="display:none" ><span class="menu-link-icon fa fa-cogs"></span> <span class="menu-link-title"><?php print $this->lang->line('menu_site_settings'); ?></span></a>

    <li><a href="javascript:"><span class="menu-link-icon fa fa-users"></span> <span class="menu-link-title"><?php print $this->lang->line('menu_membership'); ?></span></a>
        <ul>
            <li<?php print ((Site_Controller::$page == "list_members") ? ' class="open"' : ""); ?>><a href="<?php print $base_url; ?>adminpanel/list_members"><span class="menu-link-icon fa fa-angle-right"></span> <?php print $this->lang->line('menu_list_members'); ?></a>
            <li<?php print ((Site_Controller::$page == "add_member") ? ' class="open"' : ""); ?>><a href="<?php print $base_url; ?>adminpanel/add_member"><span class="menu-link-icon fa fa-angle-right"></span> <?php print $this->lang->line('menu_add_member'); ?></a>
            <li<?php print ((Site_Controller::$page == "roles") ? ' class="open"' : ""); ?>><a href="<?php print $base_url; ?>adminpanel/roles"><span class="menu-link-icon fa fa-angle-right"></span> <?php print $this->lang->line('menu_roles'); ?></a>
            <li<?php print ((Site_Controller::$page == "permissions") ? ' class="open"' : ""); ?>><a href="<?php print $base_url; ?>adminpanel/permissions"><span class="menu-link-icon fa fa-angle-right"></span> <?php print $this->lang->line('menu_permissions'); ?></a>
        </ul>
    </li>

    <!-- =================== START Add Menu Master ================================== -->
    <li><a href="javascript:"><span class="menu-link-icon fa fa-circle-o"></span> <span class="menu-link-title">Menu Master Entry</span></a>
        <ul>
            <li<?php print ((Site_Controller::$page == "add_menu") ? ' class="open"' : ""); ?>><a href="<?php print $base_url; ?>adminpanel/add_menu"><span class="menu-link-icon fa fa-angle-right"></span> Add Menu</a>

            <li<?php print ((Site_Controller::$page == "view_menu") ? ' class="open"' : ""); ?>><a href="<?php print $base_url; ?>adminpanel/add_menu/view_menu"><span class="menu-link-icon fa fa-angle-right"></span> View Menu</a>
        </ul>
    </li>
    <!-- ======================END Add Menu Master ================================== -->

    <!-- =================== START Crypto payment logs ================================== -->

    <li><a href="javascript:"><span class="menu-link-icon fa fa-circle-o"></span> <span class="menu-link-title">Crypto Payment Log</span></a>
        <ul>
            <li<?php print ((Site_Controller::$page == "deposit") ? ' class="open"' : ""); ?>><a href="<?php print $base_url; ?>adminpanel/crypto_log/deposit"><span class="menu-link-icon fa fa-angle-right"></span> Deposit Log</a>

            <li<?php print ((Site_Controller::$page == "withdraw") ? ' class="open"' : ""); ?>><a href="<?php print $base_url; ?>adminpanel/crypto_log/withdraw"><span class="menu-link-icon fa fa-angle-right"></span> Withdraw Log</a>
        </ul>
    </li>

    <!-- =================== END Crypto payment logs ================================== -->

    <li<?php print ((Site_Controller::$page == "change_password") ? ' class="open"' : ""); ?>><a href="<?php print $base_url; ?>adminpanel/change_password"><span class="menu-link-icon fa fa-cogs"></span> <span class="menu-link-title"><?php print 'Change Password' ?></span></a>
<?php } ?>

</ul>
