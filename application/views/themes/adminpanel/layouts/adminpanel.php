<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title><?php print Settings_model::$db_config['site_title']; ?>: <?php print $template['title']; ?></title>
	<?php print $template['metadata']; ?>

    <?php /* Set current timezone */
        date_default_timezone_set("Asia/Kolkata");
    ?>
	<!-- Stylesheet -->
    <link href="<?php print base_url(); ?>assets/css/adminpanel/bootstrap.min.css" rel="stylesheet">
    
    <!-- by nitish -->
    <!-- tcal datepicker -->
    <link rel="stylesheet" href="<?php print base_url(); ?>assets/datepicker/datepicker3.css">    
    <link rel="stylesheet" href="<?php print base_url(); ?>assets/datatables/dataTables.bootstrap.css">

    <!-- Google web font -->
    <link href='https://fonts.googleapis.com/css?family=Lato:100,300,400,700,900,100italic,300italic,400italic,700italic,900italic' rel='stylesheet' type='text/css'>

    <!-- Favicons -->
    <meta name="theme-color" content="#ffffff">

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <?php $this->load->view('generic/js_system'); ?>
    <script src="<?php print base_url(); ?>assets/css/adminpanel/jquery-2.2.4.min.js"></script>
    <script>window.jQuery || document.write('<script src="<?php print base_url(); ?>assets/vendor/jquery/jquery-2.2.4.min.js">\x3C/script>')</script>
    <script src="<?php print base_url(); ?>assets/vendor/bootstrap/bootstrap.min.js"></script>
    <script src="<?php print base_url(); ?>assets/vendor/navgoco/jquery.navgoco.js"></script>
    <script src="<?php print base_url(); ?>assets/vendor/slimscroll/jquery.slimscroll.min.js"></script>
    <script src="<?php print base_url(); ?>assets/vendor/bootbox/bootbox.min.js"></script>
    <script src="<?php print base_url(); ?>assets/vendor/cookie-js/js.cookie.js"></script>
    <script src="<?php print base_url(); ?>assets/vendor/parsley/parsley.min.js"></script>
    <?php print $template['js']; ?>
    <script src="<?php print base_url(); ?>assets/js/app.js"></script>

    

    <!-- datepicker -->
      <link rel="stylesheet" href="<?php print base_url(); ?>assets/datetimepicker/jquery-ui.css">
     <!--  <script src="<?php print base_url(); ?>assets/datetimepicker/jquery-1.12.4.js"></script> -->
      <script src="<?php print base_url(); ?>assets/datetimepicker/jquery-ui.js"></script>


    <!-- Rich Text Editor CSS AND JS -->    
    <link rel="stylesheet" href="<?php print base_url(); ?>assets/richEditor/editor.css">
    <script src="<?php print base_url(); ?>assets/richEditor/editor.js"></script>
    
    
    <script>
         function PopupCenter(url, w, h, title='Growth Chart') {
            // Fixes dual-screen position                         Most browsers      Firefox  
            var dualScreenLeft = window.screenLeft != undefined ? window.screenLeft : screen.left;
            var dualScreenTop = window.screenTop != undefined ? window.screenTop : screen.top;

            width = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width;
            height = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height;

            var left = ((width / 2) - (w / 2)) + dualScreenLeft;
            var top = ((height / 2) - (h / 2)) + dualScreenTop;
            var newWindow = window.open(url, title, 'scrollbars=yes, width=' + w + ', height=' + h + ', top=' + top + ', left=' + left);

            // Puts focus on the newWindow  
            if (window.focus) {
                newWindow.focus();
            }
        }
    </script>
    <!--PDF PRINT STYLE-->
    <style type="text/css" media="print">
    @page { size: landscape; }
    @media print {
    a[href]:after {
        content: none !important;
    }
}

        
</style>
<!--PDF PRINT SCRIPT-->
<script >
    function printpdf(divName) {
       var printContents = document.getElementById(divName).innerHTML;
       var originalContents = document.body.innerHTML;

       document.body.innerHTML = printContents;

       window.print();

       document.body.innerHTML = originalContents;
   }
</script>
    <style>
        /* Paste this css to your style sheet file or under head tag */
        /* This only works with JavaScript, 
        if it's not present, don't show loader */
        .no-js #loader { display: none;  }
        .js #loader { display: block; position: absolute; left: 100px; top: 0; }
        .se-pre-con {
            position: fixed;
            left: 0px;
            top: 0px;
            width: 100%;
            height: 100%;
            z-index: 9999;
            background: url(<?php print base_url(); ?>assets/loader/loader4.gif) center no-repeat #fff;
        }
    </style>

    <!-- 
    <script src="<?php print base_url(); ?>assets\loader\jquery.min.js"></script> -->
<script src="<?php print base_url(); ?>assets\loader\modernizr.js"></script>

    <script>
        //paste this code under head tag or in a seperate js file.
        // Wait for window load
        $(window).load(function () {
            // Animate loader off screen
            $(".se-pre-con").fadeOut("slow");
        });
    </script>

    <style>
    /* Center the loader */
    #inner_loader {
        position: absolute;
        left: 50%;
        top: 50%;
        z-index: 99999;
        margin: -75px 0 0 -75px;
        border: 16px solid #f3f3f3;
        border-top: 16px solid blue;
        border-right: 16px solid green;
        border-bottom: 16px solid red;
        border-radius: 50%;
        border-top: 16px solid #3498db;
        width: 120px;
        height: 120px;
        -webkit-animation: spin 2s linear infinite;
        animation: spin 2s linear infinite;
    }

    @-webkit-keyframes spin {
        0% {
            -webkit-transform: rotate(0deg);
        }

        100% {
            -webkit-transform: rotate(360deg);
        }
    }

    @keyframes spin {
        0% {
            transform: rotate(0deg);
        }

        100% {
            transform: rotate(360deg);
        }
    }

    /* Add animation to "page content" */
    .animate-bottom {
        position: relative;
        -webkit-animation-name: animatebottom;
        -webkit-animation-duration: 1s;
        animation-name: animatebottom;
        animation-duration: 1s;
    }

    @-webkit-keyframes animatebottom {
        from {
            bottom: -100px;
            opacity: 0;
        }

        to {
            bottom: 0px;
            opacity: 1;
        }
    }

    @keyframes animatebottom {
        from {
            bottom: -100px;
            opacity: 0;
        }

        to {
            bottom: 0;
            opacity: 1;
        }
    }

    #inner_loader_bg {
        background: #666666;
        opacity: 0.5;
        position: absolute;
        z-index: 9999;
        top: 0px;
        width: 100%;
        height: 100%;
    }

    .loader_up {
        position: absolute;
        z-index: 99999;
    }
</style>
</head>

<body class="notransition<?php print " ". Site_controller::$page; ?>">
<div class="se-pre-con"></div>
<div class="wrap header-fixed">

    <?php print $template['partials']['header']; ?>

    <?php $this->load->view('themes/'. Settings_model::$db_config['adminpanel_theme'] .'/partials/aside.php'); ?>
    
    <div class="content">
        <?php print $template['body']; ?>
    </div>


    <footer class="footer">
        <?php print $template['partials']['footer']; ?>
    </footer>
    <script src="<?php print base_url(); ?>assets/datepicker/bootstrap-datepicker.js"></script>
    <script src="<?php print base_url(); ?>assets/datatables/jquery.dataTables.min.js"></script>
    <script src="<?php print base_url(); ?>assets/datatables/dataTables.bootstrap.min.js"></script>

    <!-- InputMask -->
    <script src="<?php print base_url(); ?>assets/inputmask/jquery.inputmask.js"></script>
    <script src="<?php print base_url(); ?>assets/inputmask/jquery.inputmask.date.extensions.js"></script>
    <script src="<?php print base_url(); ?>assets/inputmask/jquery.inputmask.extensions.js"></script>


    <div id="inner_loader" class="loader_up" style="display:none"></div>
    <div id="inner_loader_bg" style="display:none"></div>
    <script>
        function showLoader() {
            document.getElementById("inner_loader").style.display = "block";
            document.getElementById("inner_loader_bg").style.display = "block";
        }
    </script>


</div>

</body>
</html>