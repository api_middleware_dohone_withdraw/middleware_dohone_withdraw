<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'auth/login';
$route['404_override'] = 'utils/page_not_found';
$route['translate_uri_dashes'] = FALSE;

/* Default router */
$route['login'] = 'auth/login';
$route['login/(.+)'] = 'auth/login/$1';
$route['logout'] = 'auth/logout';
$route['oauth2'] = 'auth/oauth2';
$route['oauth2/(.+)'] = 'auth/oauth2/$1';
$route['register'] = 'auth/register';
$route['register/(.+)'] = 'auth/register/$1';
$route['renew_password'] = 'auth/renew_password';
$route['renew_password/(.+)'] = 'auth/renew_password/$1';
$route['new_password'] = 'auth/new_password';
$route['new_password/(.+)'] = 'auth/new_password/$1';
$route['resend_activation'] = 'auth/resend_activation';
$route['resend_activation/(.+)'] = 'auth/resend_activation/$1';
$route['retrieve_username'] = 'auth/retrieve_username';
$route['retrieve_username/(.+)'] = 'auth/retrieve_username/$1';
$route['activate_account'] = 'auth/activate_account';
$route['activate_account/(.+)'] = 'auth/activate_account/$1';

//master form
$route['master'] = 'adminpanel/dashboard';


//payment loading page forr deposit and withdraw
$route['deposit/test'] = 'genome/dashboard/test_deposit';
$route['withdraw/test'] = 'genome/dashboard/test_withdraw';

$route['deposit/dashboard'] = 'genome/dashboard/deposit';
$route['withdraw/dashboard'] = 'genome/dashboard/withdraw';

/*genome router */
$route['genome/home'] = 'genome/genome_controller/home';
$route['genome/deposit'] = 'genome/genome_controller/deposit';
$route['genome/withdraw'] = 'genome/genome_controller/withdraw';

$route['genome/cancel'] = 'genome/genome_controller/cancel';
$route['genome/decline'] = 'genome/genome_controller/decline';
$route['genome/success'] = 'genome/genome_controller/success';


/*perfect money */
$route['perfectmoney/deposit'] = 'genome/perfectmoney_controller/deposit';
$route['perfectmoney/withdraw'] = 'genome/perfectmoney_controller/withdraw';

$route['perfectmoney/cancel'] = 'genome/perfectmoney_controller/cancel';
$route['perfectmoney/decline'] = 'genome/perfectmoney_controller/decline';
$route['perfectmoney/success'] = 'genome/perfectmoney_controller/success';

/*api router*/
$route['api'] = 'webservice/api/apichild/default'; 
$route['api/genome/deposit'] = 'webservice/api/apichild/genomedeposit'; 

/*crypto payments */
$route['crypto/deposit'] = 'genome/crypto_controller/deposit';
$route['crypto/withdraw'] = 'genome/crypto_controller/withdraw';

$route['crypto/cancel'] = 'genome/crypto_controller/cancel';
$route['crypto/decline'] = 'genome/crypto_controller/decline';
$route['crypto/success'] = 'genome/crypto_controller/success';

/*dohone payments */
$route['dohone/deposit'] = 'genome/dohone_controller/deposit';
$route['dohone/withdraw'] = 'genome/dohone_controller/withdraw';

$route['dohone/cancel'] = 'genome/dohone_controller/cancel';
$route['dohone/decline'] = 'genome/dohone_controller/decline';
$route['dohone/success'] = 'genome/dohone_controller/success';
$route['dohone/notify'] = 'genome/dohone_controller/notify';
