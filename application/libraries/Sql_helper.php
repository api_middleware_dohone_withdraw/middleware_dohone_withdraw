<?php
class Sql_helper {
    //put your code here
    
    private $db;
    private $load;
    function __construct()
    {
        $this->db =& get_instance()->db;    // get a reference to CodeIgniter.
        $this->load =& get_instance()->load; 
        $this->load->library('session');
    }
    
    /*Code to insert data into database */
    public function insert_data($table, $columnArray)
    {
        $this->db->insert(DB_PREFIX .$table, $columnArray);
        return $this->db->insert_id();
    }

    /*Code to update data into database */
    public function update_data($table, $columnArray, $whereArray)
    {
        foreach ($whereArray as $key => $value) {
            $this->db->where($key, $value);
        }

        $this->db->update(DB_PREFIX .$table, $columnArray);

        return $this->db->affected_rows();
    }

    /*Code to delete data into database */
    public function delete_data($table, $whereArray)
    {
        foreach ($whereArray as $key => $value) {
            $this->db->where($key, $value);
        }
        $this->db->delete(DB_PREFIX .$table);
        return $this->db->affected_rows();
    }
    
    /*Code to return a value with string where clause*/
    public function getText($table, $retcolumn, $whereString=NULL, $groupArray=NULL)
    {
        $sql = "select ". $retcolumn ." from ". (DB_PREFIX . $table)." ";
        if(isset($whereString)){   
            $sql .= "where " .$whereString;
        }
        
        if(isset($groupArray)){
            $this->db->group_by($groupArray);
        }
        
        $q = $this->db->query($sql);
        $row = $q->row_array();
        if (isset($row))
        {
            return $row[$retcolumn];
        }
        return NULL;
    }
    
    /*Code to return a value with Array where clause*/
    public function retText($table, $retcolumn, $whereArray=NULL, $groupArray=NULL)
    {
        $this->db->select($retcolumn)->from(DB_PREFIX .$table);
        if(isset($whereArray)){
            foreach ($whereArray as $key => $value) {
                $this->db->where($key, $value);
            }
        }
        
        if(isset($groupArray)){
            $this->db->group_by($groupArray);
        }
        
        $q = $this->db->get();
        $row = $q->row_array();
        if (isset($row))
        {
            return $row[$retcolumn];
        }
        return NULL;
    }
    
    /* Code for return nos of rows */
    public function getCount($table, $whereArray=NULL, $groupArray=NULL) {
        $this->db->select('count(id) as cnt')->from(DB_PREFIX .$table);
        
        if(isset($whereArray)){
            foreach ($whereArray as $key => $value) {
                $this->db->where($key, $value);
            }
        }
        
        if(isset($groupArray)){
            $this->db->group_by($groupArray);
        }
        
        $q = $this->db->get();
        $row = $q->row_array();
        if (isset($row))
        {
            return $row["cnt"];
        }
        return 0;
    }

    public function getCounts($table, $countCol, $whereString=NULL)
    {
        $sql = "select count(". $countCol .") as cnt from ". (DB_PREFIX . $table)." ";
        if(isset($whereString)){   
            $sql .= "where " .$whereString;
        }
        
        $q = $this->db->query($sql);
        $row = $q->row_array();
        if (isset($row))
        {
            return $row["cnt"];
        }
        return 0;
    }
    
    public function getResult($table, $columns, $whereArray=NULL, $orderBy=NULL, $groupArray=NULL) {
        
        $this->db->select($columns)->from(DB_PREFIX .$table);
        
        if(isset($whereArray)){
            foreach ($whereArray as $key => $value) {
                $this->db->where($key, $value);
            }
        }
        
        if(isset($groupArray)){
            $this->db->group_by($groupArray);
        }
        
        if(isset($orderBy)){
            foreach ($orderBy as $key => $value) {
                $this->db->order_by($key, $value);
            }
        }

        $q = $this->db->get();
        if($q->num_rows() > 0) {
            return $q;
        }
        return NULL;
    }
    
    //$joinArray = array('join table'=>'join column rule')
    public function getJoinResult($table, $columns, $joinArray, $whereArray=NULL, $orderBy=NULL, $groupArray=NULL) {
        $this->db->select($columns)->from(DB_PREFIX .$table);
        
        foreach ($joinArray as $key => $value) {
            $this->db->join($key, $value);
        }

        if(isset($whereArray)){
            foreach ($whereArray as $key => $value) {
                $this->db->where($key, $value);
            }
        }
        
        if(isset($groupArray)){
            $this->db->group_by($groupArray);
        }
        
        if(isset($orderBy)){
            foreach ($orderBy as $key => $value) {
                $this->db->order_by($key, $value);
            }
        }

        $q = $this->db->get();
        
        if($q->num_rows() > 0) {
            return $q;
        }
        return NULL;
    }
    
    public function getArray($table, $columns, $whereArray=NULL, $orderBy=NULL, $groupArray=NULL) {
        $this->db->select($columns)->from(DB_PREFIX .$table);
        
        if(isset($whereArray)){
            foreach ($whereArray as $key => $value) {
                $this->db->where($key, $value);
            }
        }
        
        if(isset($groupArray)){
            $this->db->group_by($groupArray);
        }
        
        if(isset($orderBy)){
            foreach ($orderBy as $key => $value) {
                $this->db->order_by($key, $value);
            }
        }

        $q = $this->db->get();
    
        if($q->num_rows() > 0) {
            return $q->result_array();
        }
        return NULL;
    }
    
    //$joinArray = array('join table'=>'join column rule')
    public function getJoinArray($table, $columns, $joinArray, $whereArray=NULL, $orderBy=NULL, $groupArray=NULL) {
        $this->db->select($columns)->from(DB_PREFIX .$table);
        
        foreach ($joinArray as $key => $value) {
            $this->db->join($key, $value);
        }

        if(isset($whereArray)){
            foreach ($whereArray as $key => $value) {
                $this->db->where($key, $value);
            }
        }
        
        if(isset($groupArray)){
            $this->db->group_by($groupArray);
        }

        if(isset($orderBy)){
            foreach ($orderBy as $key => $value) {
                $this->db->order_by($key, $value);
            }
        }

        $q = $this->db->get();
        
        if($q->num_rows() > 0) {
            return $q->result_array();
        }
        return NULL;
    }
    
    /* Code for retreieve data in list table */
    public function getDataList($tablename, $column, $whereArray=NULL, $orderBy=NULL, $limit = NULL, $offset = 0, $groupArray=NULL)
    {
        $this->db->select($column);
        $this->db->from(DB_PREFIX .$tablename);
        
        if(isset($whereArray)){
            foreach ($whereArray as $key => $value) {
                $this->db->where($key, $value);
            }
        }

        if(isset($groupArray)){
            $this->db->group_by($groupArray);
        }

        if(isset($orderBy)){
            foreach ($orderBy as $key => $value) {
                $this->db->order_by($key, $value);
            }
        }
        
        if(isset($limit)){
            $this->db->limit($limit, $offset);
        }

        $q = $this->db->get();
        
        if($q->num_rows() > 0) {
            return $q;
        }

        return NULL;
    }
    
    /* Code for retreieve data in list table join condition*/
    public function getJoinDataList($tablename, $column, $joinArray, $whereArray=NULL, $orderBy=NULL, $limit = 50, $offset = 0, $groupArray=NULL)
    {
        $this->db->select($column);
        $this->db->from(DB_PREFIX .$tablename);
        
        foreach ($joinArray as $key => $value) {
            $this->db->join($key, $value);
        }
        
        if(isset($whereArray)){
            foreach ($whereArray as $key => $value) {
                $this->db->where($key, $value);
            }
        }

        if(isset($groupArray)){
            $this->db->group_by($groupArray);
        }

        if(isset($orderBy)){
            foreach ($orderBy as $key => $value) {
                $this->db->order_by($key, $value);
            }
        }

        $this->db->limit($limit, $offset);

        $q = $this->db->get();
        
        if($q->num_rows() > 0) {
            return $q;
        }

        return NULL;
    }
    
    public function executeQuery($sql)
    {
        return $this->db->query($sql);
    }
}
