<?php
/**
 *
 * Dynmic_menu.php
 * Created By AMIT(amie)
 *
 */
class Dynamic_menu {

    private $ci;                // for CodeIgniter Super Global Reference.

    private $id_menu            = 'id="top-menu"';
    

    // --------------------------------------------------------------------

    /**
     * PHP5        Constructor
     *
     */
    function __construct()
    {
        $this->ci =& get_instance();    // get a reference to CodeIgniter.
    }

    // --------------------------------------------------------------------

    /**
     * build_menu($table, $type)
     *
     * Description:
     *
     * builds the Dynaminc dropdown menu
     * $table allows for passing in a MySQL table name for different menu tables.
     * $type is for the type of menu to display ie; topmenu, mainmenu, sidebar menu
     * or a footer menu.
     *
     * @param    string    the MySQL database table name.
     * @param    string    the type of menu to display.
     * @return    string    $html_out using CodeIgniter achor tags.
     */
    function build_menu($table = 'dynamic_menu')
    {
        $menu = array();
        //$urlCustom =print base_url();

        // use active record database to get the menu.

        $query = $this->ci->db->get($table);

        if ($query->num_rows() > 0)
        {
            // `id`, `title`, `link_type`, `page_id`, `module_name`, `url`, `uri`, `dyn_group_id`, `position`, `target`, `parent_id`, `show_menu`
            $i=0;
            foreach ($query->result() as $row => $innerArray)
            {
                $i=$i+1;

                
                $menu[$i]['id']            = $innerArray->id;
                $menu[$i]['title']        = $innerArray->title;
                $menu[$i]['link']            = $innerArray->link_type;
                $menu[$i]['page']            = $innerArray->menu_id;
                $menu[$i]['module']        = $innerArray->module_name;
                $menu[$i]['url']            = $innerArray->url;
                $menu[$i]['uri']            = $innerArray->uri;
                $menu[$i]['dyn_group']    = $innerArray->dyn_group_id;
                $menu[$i]['position']        = $innerArray->position;
                $menu[$i]['target']        = $innerArray->target;
                $menu[$i]['parent']        = $innerArray->parent_id;
                $menu[$i]['is_parent']    = $innerArray->is_parent;
                $menu[$i]['show']            = $innerArray->show_menu;
            }
          
        }
        // The $query result object will no longer be available
        $query->free_result();    


  
        // ----------------------------------------------------------------------     
        // now we will build the dynamic menus.

		$html_out = "\t\t".'<ul '.$this->id_menu.'>'."\n";
        $html_out .= "\t\t\t\t\t\t".'<li>'.anchor('','Home');
        // loop through the $menu array() and build the parent menus.
        for ($i = 1; $i <= count($menu); $i++)
        {
            if (is_array($menu[$i]))    // must be by construction but let's keep the errors home
            {
                if ($menu[$i]['show'] && $menu[$i]['parent'] == 0)    // are we allowed to see this menu?
                {
                    if ($menu[$i]['is_parent'] == TRUE)
                    {
                        // CodeIgniter's anchor(uri segments, text, attributes) tag.
                        $html_out .= "\t\t\t".'<li>'.anchor('#', $menu[$i]['title']);
                    }
                    else
                    {
                        $html_out .= "\t\t\t\t".'<li>'.anchor($menu[$i]['url'], $menu[$i]['title']);
                    }

                    // loop through and build all the child submenus.
                    $html_out .= $this->get_childs($menu, $i);

                    $html_out .= '</li>'."\n";

                }
            }
            else
            {
                exit (sprintf('menu nr %s must be an array', $i));
            }
        }
        $html_out .= "\t\t".'</ul>' . "\n";

        return $html_out;
    }  
	/**
     * get_childs($menu, $parent_id) - SEE Above Method.
     *
     * Description:
     *
     * Builds all child submenus using a recurse method call.
     *
     * @param    mixed    $menu    array()
     * @param    string    $parent_id    id of parent calling this method.
     * @return    mixed    $html_out if has subcats else FALSE
     */
    function get_childs($menu, $parent_id)
    {
        $has_subcats = FALSE;

        $html_out  = '';
        $html_out .= "\t\t\t\t\t".'<ul '.$this->id_menu.'>'."\n";

        for ($i = 1; $i <= count($menu); $i++)
        {

            if ($menu[$i]['show'] && $menu[$i]['parent'] == $parent_id)    // are we allowed to see this menu?
            {
                $has_subcats = TRUE;

                if ($menu[$i]['is_parent'] == TRUE)
                {
                    $html_out .= "\t\t\t\t\t\t".'<li>'.anchor('#', $menu[$i]['title']);
                }
                else
                {
                    $html_out .= "\t\t\t\t\t\t".'<li>'.anchor($menu[$i]['url'],$menu[$i]['title']);
                }

                // Recurse call to get more child submenus.
                $html_out .= $this->get_childs($menu, $i);

                $html_out .= '</li>' . "\n";
            }
        }
        $html_out .= "\t\t\t\t\t".'</ul>' . "\n";

        return ($has_subcats) ? $html_out : FALSE;
    }
}
// ------------------------------------------------------------------------
// End of Dynamic_menu Library Class.

// ------------------------------------------------------------------------
/* End of file Dynamic_menu.php */
/* Location: ../application/libraries/Dynamic_menu.php */  