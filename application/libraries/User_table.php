<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User_table {
    /*pagination*/
    private $CI;
    function __construct()
    {
        /*Load System Pagination Library */
        $this->CI =& get_instance();
        $this->CI->load->library('table');
        $this->CI->load->library('sql_helper');
        
        $template = array(
            'table_open'            => '<table border="0" cellpadding="4" cellspacing="0" class="table table-hover table-striped">',

            'thead_open'            => '<thead>',
            'thead_close'           => '</thead>',

            'heading_row_start'     => '<tr>',
            'heading_row_end'       => '</tr>',
            'heading_cell_start'    => '<th>',
            'heading_cell_end'      => '</th>',

            'tbody_open'            => '<tbody>',
            'tbody_close'           => '</tbody>',

            'row_start'             => '<tr>',
            'row_end'               => '</tr>',
            'cell_start'            => '<td>',
            'cell_end'              => '</td>',

            'row_alt_start'         => '<tr>',
            'row_alt_end'           => '</tr>',
            'cell_alt_start'        => '<td>',
            'cell_alt_end'          => '</td>',

            'table_close'           => '</table>'
        );
        
        $this->CI->table->set_template($template);
    }
    
    public function create_table($heading, $query)
    {
        $this->CI->table->set_heading($heading);
        return $this->CI->table->generate($query);
    }
    
    public function create_datatable($tablename, $viewname, $heading, $column, $whereArray, $orderBy, $sortBy, $is_edit, $btn_edit, $link_edit, $is_view, $btn_view, $link_view, $is_delete, $btn_delete, $limit, $offset)
    {
        $this->CI->table->set_heading($heading);
        
        $edit = NULL;
        $view = NULL;
        $delete = NULL;
        $is_delete=0;
        
        $q = $this->CI->sql_helper->getDataList($viewname, $column, $whereArray, $orderBy, $sortBy, $limit, $offset);
        foreach ($q->result_array() as $row)
        {
            $colmns = array();
            foreach ($q->list_fields() as $field)
            {
                array_push($colmns, $row[$field]);
            }
            if($is_edit == 1)
            {
                $edit = "<a href='" . base_url() . $link_edit . "/" . $row[0] ."' style='color:blue;font-weight: bold;'>".$btn_edit."</a>";
                array_push($colmns, $edit);
            }
            if($is_view == 1)
            {
                $view = "<a href='" . base_url() . $link_view . "/" . $row[0] ."' style='color:blue;font-weight: bold;'>".$btn_view."</a>";
                array_push($colmns, $view);
            }
            if($is_delete == 1)
            {
                $delete = "<a href='" . base_url() . $link_edit . "/" . $row[0] ."' style='color:blue;font-weight: bold;'>".$btn_delete."</a>";
                array_push($colmns, $delete);
            }
            $this->CI->table->add_row($colmns);
        }
        return $this->CI->table->generate();
    }
}
