<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User_pagination {
    /*pagination*/
    private $config;
    private $limit = 10;
    private $numlink = 5;
    private $CI;
    function __construct()
    {
        
    }
    
    public function create_pagination($base_url, $total_rows, $limit=0, $uri_segment=3)
    {
        if($limit>0)
            $this->limit = $limit;
        /*Load System Pagination Library */
        $this->CI =& get_instance();
        $this->CI->load->library('pagination');
        
        /*pagination initialize **/
        $this->config = array();
        $this->config["base_url"] = $base_url;
        $this->config["per_page"] = $this->limit;
        $this->config['num_links'] = $this->numlink;
        $this->config['uri_segment'] = $uri_segment;
        $this->config['use_page_numbers'] = TRUE;
        $this->config['full_tag_open'] = "<ul class='pagination'>";
        $this->config['full_tag_close'] ="</ul>";
        $this->config['num_tag_open'] = '<li>';
        $this->config['num_tag_close'] = '</li>';
        $this->config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
        $this->config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
        $this->config['next_tag_open'] = "<li>";
        $this->config['next_tag_close'] = "</li>";
        $this->config['prev_tag_open'] = "<li>";
        $this->config['prev_tag_close'] = "</li>";
        $this->config['first_tag_open'] = "<li>";
        $this->config['first_tag_close'] = "</li>";
        $this->config['last_tag_open'] = "<li>";
        $this->config['last_tag_close'] = "</li>";
        
        $this->config["total_rows"] = $total_rows;
        $this->CI->pagination->initialize($this->config);
        return  $this->CI->pagination->create_links();
    }
}
