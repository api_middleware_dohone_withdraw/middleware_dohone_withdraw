<?php  if (!defined('BASEPATH')) exit('No direct script access allowed');

require_once(APPPATH.'libraries/OAuth2/OAuth2_Abstract.php');

class Twitter extends OAuth2_Abstract
{
    public function __construct() {
        parent::__construct();
    }

    public function setProvider($data) {
        $this->_provider = new League\OAuth1\Client\Server\Twitter(array(
            'identifier' => 'your-identifier',
            'secret' => 'your-secret',
            'callback_uri' => "http://your-callback-uri/",
        ));
    }
}