<?php
error_reporting(E_ERROR | E_WARNING | E_PARSE | E_NOTICE);
class User_encryption {

    private $key;

    function __construct($user_key = "fe9cee03d0ddb1a02ca2a815fd9dbab2") {
        $this->key = $user_key;
    }

    function Encrypt($plaintext) {
        $plaintext = User_encryption::pkcs5_pad($plaintext, 16);
        return bin2hex(mcrypt_encrypt(MCRYPT_RIJNDAEL_128, $this->hex2bin($this->key), $plaintext, MCRYPT_MODE_ECB));
    }

    function Decrypt($encrypted) {
        $decrypted = mcrypt_decrypt(MCRYPT_RIJNDAEL_128, $this->hex2bin($this->key), $this->hex2bin($encrypted), MCRYPT_MODE_ECB);
        $padSize = ord(substr($decrypted, -1));
        return substr($decrypted, 0, $padSize * -1);
    }

    function hex2bin($str) {
        $sbin = "";
        $len = strlen($str);
        for ($i = 0; $i < $len; $i += 2) {
            $sbin .= pack("H*", substr($str, $i, 2));
        }

        return $sbin;
    }

    public function pkcs5_pad($text, $blocksize) {
        $pad = $blocksize - (strlen($text) % $blocksize);
        return $text . str_repeat(chr($pad), $pad);
    }

}
