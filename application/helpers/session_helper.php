<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if (!function_exists('set_session_data')) {
    function set_session_data($user_id, $first_name, $last_name, $username) {
        $CI = & get_instance();
        $CI->session->set_userdata('user_id', $user_id);
        $CI->session->set_userdata('username', $username);
        $CI->session->set_userdata('first_name', $first_name);
        $CI->session->set_userdata('last_name', $last_name);
    }
}