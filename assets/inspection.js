$(document).ready(function() {
    //code for drinking avaibility
        $("#driniking_avail").change(function() {
            if($("#driniking_avail").val() == "1")
            {

                $('#drinking_funct').attr('required', true);
                $('#drinking_remarks').removeAttr('required');

                $('#davb').show();
                $('#drmks').hide()
            }
            else if($("#driniking_avail").val() == "0")
            {
                $('#drinking_remarks').attr('required', true);
                $('#drinking_funct').removeAttr('required');

                $('#davb').hide();
                $('#drmks').show();
            }
            else
            {

                $("#drinking_funct").removeAttr('required');
                $('#drinking_remarks').removeAttr('required');
                
                $("#davb").hide();
                $("#drmks").hide();
            }
        });

        $("#drinking_funct").change(function() {
            if($("#drinking_funct").val() == "1")
            {
                $('#drinking_remarks').removeAttr('required');
                $("#drmks").hide();
                
            }
            else if($("#drinking_funct").val() == "0")
            {
                $("#drinking_remarks").attr('required', true);
                $("#drmks").show();
            }
            else
            {

               $('#drinking_remarks').removeAttr('required');
               $("#drmks").hide();
            }
        });

//code for toilet avaibility
        $("#toilet_avail").change(function() {
            if($("#toilet_avail").val() == "1")
            {
                $("#toilet_funct").attr('required', true);
                $("#toilet_type").attr('required', true);
                $("#toiletwater_avail").attr('required', true);
                $('#toilet_remarks').removeAttr('required');

                $("#tavb").show();
                $("#ttype").show();
                $("#trmks").hide();
                $("#twaterfacility").show();

            }
            else if($("#toilet_avail").val() == "0")
            {

                $("#toilet_funct").removeAttr('required');
                $("#toilet_type").removeAttr('required');
                $("#toiletwater_avail").removeAttr('required');
                $('#toilet_remarks').attr('required', true);

                $("#tavb").hide();
                $("#ttype").hide();
                $("#trmks").show();
                $("#twaterfacility").hide();

            }
            else
            {

                $("#toilet_funct").removeAttr('required');
                $("#toilet_type").removeAttr('required');
                $("#toiletwater_avail").removeAttr('required');
                $('#toilet_remarks').removeAttr('required');

                $("#tavb").hide();
                $("#ttype").hide();
                $("#trmks").hide();
                $("#twaterfacility").hide();

            }
        });

        $("#toilet_funct").change(function() {
            if($("#toilet_funct").val() == "1")
            {
                $('#toilet_remarks').removeAttr('required');
                $("#trmks").hide();
            }
            else if($("#toilet_funct").val() == "0")
            {
                $('#toilet_remarks').attr('required', true);
                $("#trmks").show();
            }
            else
            {
                $("#trmks").hide();
                $('#toilet_remarks').removeAttr('required');
            }
        });

 //Water facilty avail in toilet or not
        $("#toiletwater_avail").change(function() {
            if($("#toiletwater_avail").val() == "1")
            {

                $("#toiletwater_funct").attr('required', true);
                $("#toiletwater_remarks").removeAttr('required');

                $("#twavb").show();
                $("#twrmks").hide();

            }
            else if($("#toiletwater_avail").val() == "0")
            {

                $("#toiletwater_remarks").attr('required', true);
                $("#toiletwater_funct").removeAttr('required');

                $("#twavb").hide();
                $("#twrmks").show();

            }
            else
            {

                $("#toiletwater_remarks").removeAttr('required');
                $("#toiletwater_funct").removeAttr('required');

                $("#twavb").hide();
                $("#twrmks").hide();
            }
        });

        $("#toiletwater_funct").change(function() {
            if($("#toiletwater_funct").val() == "1")
            {
                $("#toiletwater_remarks").removeAttr('required');
                $("#twrmks").hide();
            }
            else if($("#toiletwater_funct").val() == "0")
            {
                $("#toiletwater_remarks").attr('required', true);
                $("#twrmks").show();
            }
            else
            {
                $("#toiletwater_remarks").removeAttr('required');
                $("#twrmks").hide();
            }
        });

//electricity avail or not
        $("#electricity_avail").change(function() {
            if($("#electricity_avail").val() == "1")
            {
                $("#electricity_funct").attr('required', true);
                $("#electricity_remarks").removeAttr('required');

                $("#eavb").show();
                $("#ermks").hide();

            }
            else if($("#electricity_avail").val() == "0")
            {

                $("#electricity_remarks").attr('required', true);
                $("#electricity_funct").removeAttr('required');

                $("#eavb").hide();
                $("#ermks").show();

            }
            else
            {
                $("#electricity_remarks").removeAttr('required');
                $("#electricity_funct").removeAttr('required');

                $("#eavb").hide();
                $("#ermks").hide();
            }
        });

        $("#electricity_funct").change(function() {
            if($("#electricity_funct").val() == "1")
            {
                $("#electricity_remarks").removeAttr('required');
                $("#ermks").hide();
                
            }
            else if($("#electricity_funct").val() == "0")
            {
                $("#electricity_remarks").attr('required', true);
                $("#ermks").show();
            }
            else
            {
                $("#electricity_remarks").removeAttr('required');
                $("#ermks").hide();
            }
        });

        //vhnd on date
        $("#vhndon_planeddt").change(function() {
            if($("#vhndon_planeddt").val() == "1")
            {
                $("#vhnd_plannedrmrks").removeAttr('required');
                $("#divvhnd_plannedrmrks").hide();
                
            }
            else if($("#vhndon_planeddt").val() == "0")
            {
                $("#vhnd_plannedrmrks").attr('required', true);
                $("#divvhnd_plannedrmrks").show();
            }
            else
            {
                $("#vhnd_plannedrmrks").removeAttr('required');
                $("#divvhnd_plannedrmrks").hide();
            }
        });

        
    });