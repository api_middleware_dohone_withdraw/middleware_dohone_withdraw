<?php

defined('BASEPATH') OR exit('No direct script access allowed');


    /*
     * Start transaction
     *
     * rLocale : le choix de la langue. fr ou en
     *
      method : Ceci est optionnel. Si vous souhaitez que votre API n’affiche que certains opérateurs,
      vous pouvez préciser ces opérateurs ici. 1=MTN, 2=Orange, 3=Express Union, 5=Visa via UBA,
      10=Dohone, 14= Visa via Wari,15=Wari card,16=VISA/MASTERCARD, 17=YUP.
     */

    $config['merchantToken'] = 'MF216E21681053825080002';
    $config['currency'] = 'XAF';
    $config['projectName'] = "Dohone";
    $config['projectLogo'] = 'assets/images/logo.png';
    $config['endPage']= "http://localhost";
    $config['payInNotifyPage'] = "http://localhost";
    $config['cancelPage'] = "http://localhost";
    //$config['language'] = 'fr';

    /*
    |--------------------------------------------------------------------------
    | METHOD OF PAYMENT
    |--------------------------------------------------------------------------
    |
    | Ceci est optionnel. Si vous souhaitez que votre API n’affiche que certains opérateurs,
    | vous pouvez préciser ces opérateurs ici. 1=MTN, 2=Orange, 3=Express Union, 5=Visa via UBA,
    | 10=Dohone, 14= Visa via Wari,15=Wari card,16=VISA/MASTERCARD, 17=YUP.
    |
    */
    $config['method'] = '1, 2, 3, 10, 17';



    $config['numberNotifs'] = 5;
    $config['payInUrl'] = 'https://www.my-dohone.com/dohone-sandbox/pay';

    $config['payOutHashCode'] = '8A36DB3A56A8D27135B56FAB10AD12';
    $config['payOutPhoneAccount'] = '695100003';
    $config['payOutNotifyPage'] = "http://localhost";
    $config['payOutUrl'] = 'https://www.my-dohone.com/dohone-sandbox/transfertDOHONE';
