-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.4.8-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win64
-- HeidiSQL Version:             10.3.0.5771
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for middleware_api
CREATE DATABASE IF NOT EXISTS `middleware_api` /*!40100 DEFAULT CHARACTER SET utf8mb4 */;
USE `middleware_api`;

-- Dumping structure for table middleware_api.ci_crypto_deposit
CREATE TABLE IF NOT EXISTS `ci_crypto_deposit` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `user_id` varchar(255) NOT NULL,
  `amount` varchar(255) NOT NULL,
  `currency` varchar(255) NOT NULL,
  `crypto_coin` varchar(255) NOT NULL,
  `crypto_amount` varchar(255) NOT NULL,
  `crypto_amount_remaining` varchar(255) NOT NULL,
  `status` varchar(255) NOT NULL,
  `order_id` varchar(255) NOT NULL,
  `order_procced_time` timestamp NOT NULL DEFAULT current_timestamp(),
  `payment_received_time` timestamp NOT NULL DEFAULT current_timestamp(),
  `nonce` varchar(255) NOT NULL,
  `coin_address` varchar(255) NOT NULL,
  `coin_address_in` varchar(255) NOT NULL,
  `prefix_url` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=41 DEFAULT CHARSET=utf8mb4;

-- Dumping data for table middleware_api.ci_crypto_deposit: ~6 rows (approximately)
DELETE FROM `ci_crypto_deposit`;
/*!40000 ALTER TABLE `ci_crypto_deposit` DISABLE KEYS */;
INSERT INTO `ci_crypto_deposit` (`id`, `user_id`, `amount`, `currency`, `crypto_coin`, `crypto_amount`, `crypto_amount_remaining`, `status`, `order_id`, `order_procced_time`, `payment_received_time`, `nonce`, `coin_address`, `coin_address_in`, `prefix_url`) VALUES
	(35, '1234', '1', 'USD', 'ltc', '0.002618', '', 'pending', '1620805643', '2021-05-12 13:17:34', '0000-00-00 00:00:00', 'kWgdIiv1BzWXPzUDkVpCeja9p9960TlJ', 'LLGrzcbppKCii4377sMpRiiBeNP8QjWnqt', 'MCgSvXi8MAJBBZeWcQuRY3Bb3d1enhdmRq', 'https://api.cryptapi.io/ltc/create/'),
	(36, '1234', '1', 'USD', 'ltc', '0.0026181', '', 'pending', '1620806477', '2021-05-12 13:31:27', '0000-00-00 00:00:00', 'X3NbPxaoZTB4olcNqUYudnu3NKMY9boC', 'LLGrzcbppKCii4377sMpRiiBeNP8QjWnqt', 'MCgSvXi8MAJBBZeWcQuRY3Bb3d1enhdmRq', 'https://api.cryptapi.io/ltc/create/'),
	(37, '12345', '10', 'USD', 'ltc', '0.081446', '', 'pending', '1621789136', '2021-05-23 22:29:16', '0000-00-00 00:00:00', 'HPFbf8o1bUSBEgBZ9kkF8fv4zJO2Vy0B', 'LLGrzcbppKCii4377sMpRiiBeNP8QjWnqt', 'MCgSvXi8MAJBBZeWcQuRY3Bb3d1enhdmRq', 'https://api.cryptapi.io/ltc/create/'),
	(38, '12345', '10', 'USD', 'ltc', '0.072202', '', 'pending', '1621789987', '2021-05-23 22:43:19', '0000-00-00 00:00:00', 'ggJQMMBm1WFIN5mJfd9OwTSEN5amlbxw', 'LLGrzcbppKCii4377sMpRiiBeNP8QjWnqt', 'MTdvbtWrbnLWD1y2HeroFLYzRBCtD9Nzj3', 'https://api.cryptapi.io/ltc/create/'),
	(39, '12345', '11', 'USD', 'ltc', '0.0829', '', 'pending', '1621790548', '2021-05-23 22:55:14', '0000-00-00 00:00:00', 'QK6wnltuoWjUQpdZDMfjmjRihVupBNM1', 'LLGrzcbppKCii4377sMpRiiBeNP8QjWnqt', 'MTdvbtWrbnLWD1y2HeroFLYzRBCtD9Nzj3', 'https://api.cryptapi.io/ltc/create/'),
	(40, '12345', '11', 'USD', 'ltc', '0.084955', '', 'pending', '1621793119', '2021-05-23 23:35:32', '0000-00-00 00:00:00', 'IdPYhewGSrEk88F7Sgk4iiuHJhjh1uFK', 'LLGrzcbppKCii4377sMpRiiBeNP8QjWnqt', 'MRShYKayqDhzycEd2zPe3hyMGdD4RwKQzY', 'https://api.cryptapi.io/ltc/create/');
/*!40000 ALTER TABLE `ci_crypto_deposit` ENABLE KEYS */;

-- Dumping structure for table middleware_api.ci_crypto_withdraw
CREATE TABLE IF NOT EXISTS `ci_crypto_withdraw` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `user_id` varchar(255) NOT NULL,
  `amount` varchar(255) NOT NULL,
  `currency` varchar(255) NOT NULL,
  `crypto_coin` varchar(255) NOT NULL,
  `crypto_amount` varchar(255) NOT NULL,
  `crypto_amount_remaining` varchar(255) NOT NULL,
  `status` varchar(255) NOT NULL,
  `order_id` varchar(255) NOT NULL,
  `order_procced_time` timestamp NOT NULL DEFAULT current_timestamp(),
  `payment_received_time` timestamp NOT NULL DEFAULT current_timestamp(),
  `nonce` varchar(255) NOT NULL,
  `coin_address` varchar(255) NOT NULL,
  `coin_address_in` varchar(255) NOT NULL,
  `prefix_url` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8mb4;

-- Dumping data for table middleware_api.ci_crypto_withdraw: ~0 rows (approximately)
DELETE FROM `ci_crypto_withdraw`;
/*!40000 ALTER TABLE `ci_crypto_withdraw` DISABLE KEYS */;
/*!40000 ALTER TABLE `ci_crypto_withdraw` ENABLE KEYS */;

-- Dumping structure for table middleware_api.ci_dohone_withdrawal
CREATE TABLE IF NOT EXISTS `ci_dohone_withdrawal` (
  `id` int(30) NOT NULL AUTO_INCREMENT,
  `userid` varchar(200) DEFAULT NULL,
  `transaction_id` varchar(200) DEFAULT NULL,
  `amount` varchar(200) DEFAULT NULL,
  `currency` varchar(200) DEFAULT NULL,
  `email` varchar(200) DEFAULT NULL,
  `notify_url` varchar(200) DEFAULT NULL,
  `unique_hash_code` varchar(200) DEFAULT NULL,
  `merchant_token` varchar(200) DEFAULT NULL,
  `mode` varchar(200) DEFAULT NULL,
  `dohone_payout_hash` varchar(200) DEFAULT NULL,
  `payer_account_telephone_no` varchar(200) DEFAULT NULL,
  `destination_acc_tele_number` varchar(200) DEFAULT NULL,
  `receiver_name` varchar(200) DEFAULT NULL,
  `receiver_city` varchar(200) DEFAULT NULL,
  `receiver_country` varchar(200) DEFAULT NULL,
  `response_text` varchar(200) DEFAULT NULL,
  `transaction_status` varchar(200) DEFAULT NULL,
  `entry_date` varchar(200) DEFAULT NULL,
  `entry_ip` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- Dumping data for table middleware_api.ci_dohone_withdrawal: ~0 rows (approximately)
DELETE FROM `ci_dohone_withdrawal`;
/*!40000 ALTER TABLE `ci_dohone_withdrawal` DISABLE KEYS */;
/*!40000 ALTER TABLE `ci_dohone_withdrawal` ENABLE KEYS */;

-- Dumping structure for table middleware_api.ci_dynamic_menu
CREATE TABLE IF NOT EXISTS `ci_dynamic_menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `link_type` varchar(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'uri',
  `menu_id` int(11) NOT NULL DEFAULT 0,
  `module_name` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `url` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `uri` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `dyn_group_id` int(11) NOT NULL DEFAULT 0,
  `position` int(11) NOT NULL DEFAULT 0,
  `target` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `parent_id` int(11) NOT NULL DEFAULT 0,
  `is_parent` tinyint(1) NOT NULL DEFAULT 0,
  `show_menu` enum('0','1') COLLATE utf8_unicode_ci NOT NULL DEFAULT '1',
  `created_on` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `ip_address` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table middleware_api.ci_dynamic_menu: ~1 rows (approximately)
DELETE FROM `ci_dynamic_menu`;
/*!40000 ALTER TABLE `ci_dynamic_menu` DISABLE KEYS */;
INSERT INTO `ci_dynamic_menu` (`id`, `title`, `link_type`, `menu_id`, `module_name`, `url`, `uri`, `dyn_group_id`, `position`, `target`, `parent_id`, `is_parent`, `show_menu`, `created_on`, `ip_address`) VALUES
	(1, 'Check menu creation', 'page', 1, '', 'http://', '', 1, 0, NULL, 0, 0, '1', '2021-06-01 10:33:09', '127.0.0.1');
/*!40000 ALTER TABLE `ci_dynamic_menu` ENABLE KEYS */;

-- Dumping structure for table middleware_api.ci_file_master
CREATE TABLE IF NOT EXISTS `ci_file_master` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role_id` int(11) NOT NULL,
  `file_name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `url` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `position` int(11) NOT NULL DEFAULT 0,
  `target` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `is_parent` int(11) NOT NULL DEFAULT 0,
  `parent_id` int(11) NOT NULL DEFAULT 0,
  `show_menu` int(11) NOT NULL DEFAULT 0,
  `created_on` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `ip_address` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table middleware_api.ci_file_master: ~3 rows (approximately)
DELETE FROM `ci_file_master`;
/*!40000 ALTER TABLE `ci_file_master` DISABLE KEYS */;
INSERT INTO `ci_file_master` (`id`, `role_id`, `file_name`, `url`, `position`, `target`, `is_parent`, `parent_id`, `show_menu`, `created_on`, `ip_address`) VALUES
	(1, 2, 'Demo', '#', 1, '', 1, 0, 1, '2021-01-01 00:00:00', '::1'),
	(2, 2, 'demo1', 'demo1', 2, '', 0, 4, 1, '2021-01-01 00:00:00', '::1'),
	(3, 2, 'demo2', '#', 1, '', 1, 4, 1, '2021-01-01 00:00:00', '::1');
/*!40000 ALTER TABLE `ci_file_master` ENABLE KEYS */;

-- Dumping structure for table middleware_api.ci_file_permissions
CREATE TABLE IF NOT EXISTS `ci_file_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role_id` int(11) NOT NULL,
  `page_id` int(11) NOT NULL,
  `menu_name` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `menu_url` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `status` int(11) NOT NULL DEFAULT 0,
  `created_on` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `ip_address` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table middleware_api.ci_file_permissions: ~0 rows (approximately)
DELETE FROM `ci_file_permissions`;
/*!40000 ALTER TABLE `ci_file_permissions` DISABLE KEYS */;
/*!40000 ALTER TABLE `ci_file_permissions` ENABLE KEYS */;

-- Dumping structure for table middleware_api.ci_fy_mstr
CREATE TABLE IF NOT EXISTS `ci_fy_mstr` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `fy_start_month` bigint(20) DEFAULT NULL,
  `start_year` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `end_year` bigint(20) DEFAULT NULL,
  `created_by` bigint(20) DEFAULT NULL,
  `created_date` date DEFAULT NULL,
  `suspended_status` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table middleware_api.ci_fy_mstr: ~0 rows (approximately)
DELETE FROM `ci_fy_mstr`;
/*!40000 ALTER TABLE `ci_fy_mstr` DISABLE KEYS */;
/*!40000 ALTER TABLE `ci_fy_mstr` ENABLE KEYS */;

-- Dumping structure for table middleware_api.ci_genome_deposit
CREATE TABLE IF NOT EXISTS `ci_genome_deposit` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `userid` varchar(200) DEFAULT NULL,
  `transaction_id` varchar(200) DEFAULT NULL,
  `amount` decimal(14,2) DEFAULT NULL,
  `currency` varchar(10) DEFAULT NULL,
  `genome_key` varchar(200) DEFAULT NULL,
  `genome_skey` varchar(200) DEFAULT NULL,
  `customproduct` varchar(2000) DEFAULT NULL,
  `resulting` varchar(2000) DEFAULT NULL,
  `signature` varchar(2000) DEFAULT NULL,
  `success_url` varchar(500) DEFAULT NULL,
  `decline_url` varchar(500) DEFAULT NULL,
  `backUrl` varchar(500) DEFAULT NULL,
  `response_text` varchar(2000) DEFAULT NULL,
  `transaction_status` varchar(100) DEFAULT NULL,
  `entry_date` timestamp NOT NULL DEFAULT current_timestamp(),
  `entry_ip` varchar(30) DEFAULT NULL,
  `STATUS` int(11) NOT NULL DEFAULT 1,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;

-- Dumping data for table middleware_api.ci_genome_deposit: ~2 rows (approximately)
DELETE FROM `ci_genome_deposit`;
/*!40000 ALTER TABLE `ci_genome_deposit` DISABLE KEYS */;
INSERT INTO `ci_genome_deposit` (`id`, `userid`, `transaction_id`, `amount`, `currency`, `genome_key`, `genome_skey`, `customproduct`, `resulting`, `signature`, `success_url`, `decline_url`, `backUrl`, `response_text`, `transaction_status`, `entry_date`, `entry_ip`, `STATUS`) VALUES
	(1, '12345', '1621791181', 11.00, 'USD', 'pkTest_c0IAIDHXDExfC6BQw1uU8NXHLIUeSdTV', 'skTest_7lTvywsJPM6dpo6gD1oIJqyXrJE69q0L', '{"productId":"12345","productType":"fixedProduct","productName":"12345","currency":"USD","amount":"11"}', 'backurl=https://54.87.232.138/middleware_api/index.php/genome/cancel|buttontext=pay!|customproduct=[{"productid":"12345","producttype":"fixedproduct","productname":"12345","currency":"usd","amount":"11"}]|decline_url=https://54.87.232.138/middleware_api/index.php/genome/decline|height=auto|iframesrc=https://hpp-service.genome.eu/hpp|key=pktest_c0iaidhxdexfc6bqw1uu8nxhliuesdtv|name=payment page|success_url=https://54.87.232.138/middleware_api/index.php/genome/success|type=integrated|uniquetransactionid=1621791181|uniqueuserid=12345|width=auto|sktest_7ltvywsjpm6dpo6gd1oijqyxrje69q0l', 'a538b50a259cf5b7365a7d00efb72dbbbe1b278d5a05266a170c6c97c6d01bb9', 'https://54.87.232.138/middleware_api/index.php/genome/success', 'https://54.87.232.138/middleware_api/index.php/genome/decline', 'https://54.87.232.138/middleware_api/index.php/genome/cancel', NULL, 'PENDING', '2021-05-23 23:03:05', '::1', 1),
	(2, '12345', '1621792824', 11.00, 'USD', 'pkTest_c0IAIDHXDExfC6BQw1uU8NXHLIUeSdTV', 'skTest_7lTvywsJPM6dpo6gD1oIJqyXrJE69q0L', '{"productId":"12345","productType":"fixedProduct","productName":"12345","currency":"USD","amount":"11"}', 'backurl=http://192.168.0.100:8080/middleware_api//index.php/genome/cancel|buttontext=pay!|customproduct=[{"productid":"12345","producttype":"fixedproduct","productname":"12345","currency":"usd","amount":"11"}]|decline_url=http://192.168.0.100:8080/middleware_api//index.php/genome/decline|height=auto|iframesrc=https://hpp-service.genome.eu/hpp|key=pktest_c0iaidhxdexfc6bqw1uu8nxhliuesdtv|name=payment page|success_url=http://192.168.0.100:8080/middleware_api//index.php/genome/success|type=integrated|uniquetransactionid=1621792824|uniqueuserid=12345|width=auto|sktest_7ltvywsjpm6dpo6gd1oijqyxrje69q0l', 'cf3bba6090b0be629e2cca6c2d4d14f1d176c30bd18175cd2a82721b99b404e6', 'http://192.168.0.100:8080/middleware_api//index.php/genome/success', 'http://192.168.0.100:8080/middleware_api//index.php/genome/decline', 'http://192.168.0.100:8080/middleware_api//index.php/genome/cancel', NULL, 'PENDING', '2021-05-23 23:30:28', '192.168.0.100', 1);
/*!40000 ALTER TABLE `ci_genome_deposit` ENABLE KEYS */;

-- Dumping structure for table middleware_api.ci_genome_withdraw
CREATE TABLE IF NOT EXISTS `ci_genome_withdraw` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `userid` varchar(200) DEFAULT NULL,
  `transaction_id` varchar(200) DEFAULT NULL,
  `amount` decimal(14,2) DEFAULT NULL,
  `currency` varchar(10) DEFAULT NULL,
  `genome_key` varchar(200) DEFAULT NULL,
  `genome_skey` varchar(200) DEFAULT NULL,
  `merchant_account` varchar(200) DEFAULT NULL,
  `merchant_password` varchar(200) DEFAULT NULL,
  `customproduct` varchar(2000) DEFAULT NULL,
  `resulting` varchar(2000) DEFAULT NULL,
  `signature` varchar(2000) DEFAULT NULL,
  `success_url` varchar(500) DEFAULT NULL,
  `decline_url` varchar(500) DEFAULT NULL,
  `backUrl` varchar(500) DEFAULT NULL,
  `response_text` varchar(2000) DEFAULT NULL,
  `transaction_status` varchar(100) DEFAULT NULL,
  `entry_date` timestamp NOT NULL DEFAULT current_timestamp(),
  `entry_ip` varchar(30) DEFAULT NULL,
  `STATUS` int(11) NOT NULL DEFAULT 1,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- Dumping data for table middleware_api.ci_genome_withdraw: ~0 rows (approximately)
DELETE FROM `ci_genome_withdraw`;
/*!40000 ALTER TABLE `ci_genome_withdraw` DISABLE KEYS */;
/*!40000 ALTER TABLE `ci_genome_withdraw` ENABLE KEYS */;

-- Dumping structure for table middleware_api.ci_oauth_providers
CREATE TABLE IF NOT EXISTS `ci_oauth_providers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `oauth_type` int(11) NOT NULL DEFAULT 2,
  `client_id` text COLLATE utf8_unicode_ci NOT NULL,
  `client_secret` text COLLATE utf8_unicode_ci NOT NULL,
  `enabled` int(11) NOT NULL DEFAULT 1,
  `order` int(11) NOT NULL DEFAULT 999,
  `date_modified` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table middleware_api.ci_oauth_providers: ~0 rows (approximately)
DELETE FROM `ci_oauth_providers`;
/*!40000 ALTER TABLE `ci_oauth_providers` DISABLE KEYS */;
/*!40000 ALTER TABLE `ci_oauth_providers` ENABLE KEYS */;

-- Dumping structure for table middleware_api.ci_permission
CREATE TABLE IF NOT EXISTS `ci_permission` (
  `permission_id` int(11) NOT NULL,
  `permission_description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `permission_system` tinyint(1) NOT NULL DEFAULT 0,
  `permission_order` int(11) NOT NULL,
  `entry_by` bigint(20) DEFAULT NULL,
  `entry_date` timestamp NOT NULL DEFAULT current_timestamp(),
  `ipaddress` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table middleware_api.ci_permission: ~15 rows (approximately)
DELETE FROM `ci_permission`;
/*!40000 ALTER TABLE `ci_permission` DISABLE KEYS */;
INSERT INTO `ci_permission` (`permission_id`, `permission_description`, `permission_system`, `permission_order`, `entry_by`, `entry_date`, `ipaddress`) VALUES
	(1, 'View members', 1, 0, NULL, '2021-08-15 18:35:37', NULL),
	(2, 'View settings', 1, 21, NULL, '2021-08-15 18:35:37', NULL),
	(3, 'Add member', 1, 1, NULL, '2021-08-15 18:35:37', NULL),
	(4, 'Edit member', 1, 3, NULL, '2021-08-15 18:35:37', NULL),
	(5, 'Delete members', 1, 4, NULL, '2021-08-15 18:35:37', NULL),
	(6, 'OAuth2 providers', 1, 10, NULL, '2021-08-15 18:35:37', NULL),
	(7, 'Dashboard', 1, 20, NULL, '2021-08-15 18:35:37', NULL),
	(8, 'Ban/unban members', 1, 6, NULL, '2021-08-15 18:35:37', NULL),
	(9, 'Activate/deactivate members', 1, 5, NULL, '2021-08-15 18:35:37', NULL),
	(10, 'Save settings', 1, 22, NULL, '2021-08-15 18:35:37', NULL),
	(11, 'Clear sessions', 1, 25, NULL, '2021-08-15 18:35:37', NULL),
	(12, 'Manage roles', 1, 7, NULL, '2021-08-15 18:35:37', NULL),
	(13, 'Backup and export', 1, 30, NULL, '2021-08-15 18:35:37', NULL),
	(14, 'Email member', 1, 8, NULL, '2021-08-15 18:35:37', NULL),
	(15, 'Add Menu', 1, 31, NULL, '2021-08-15 18:35:37', NULL);
/*!40000 ALTER TABLE `ci_permission` ENABLE KEYS */;

-- Dumping structure for table middleware_api.ci_pmoney_deposit
CREATE TABLE IF NOT EXISTS `ci_pmoney_deposit` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `pm_accountid` varchar(200) DEFAULT NULL,
  `pm_phrase` varchar(200) DEFAULT NULL,
  `userid` varchar(200) DEFAULT NULL,
  `transaction_id` varchar(200) DEFAULT NULL,
  `amount` decimal(14,2) DEFAULT NULL,
  `currency` varchar(10) DEFAULT NULL,
  `customproduct` varchar(2000) DEFAULT NULL,
  `success_url` varchar(500) DEFAULT NULL,
  `decline_url` varchar(500) DEFAULT NULL,
  `back_url` varchar(500) DEFAULT NULL,
  `response_text` varchar(2000) DEFAULT NULL,
  `transaction_status` varchar(100) DEFAULT NULL,
  `entry_date` timestamp NOT NULL DEFAULT current_timestamp(),
  `entry_ip` varchar(30) DEFAULT NULL,
  `STATUS` int(11) NOT NULL DEFAULT 1,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4;

-- Dumping data for table middleware_api.ci_pmoney_deposit: ~3 rows (approximately)
DELETE FROM `ci_pmoney_deposit`;
/*!40000 ALTER TABLE `ci_pmoney_deposit` DISABLE KEYS */;
INSERT INTO `ci_pmoney_deposit` (`id`, `pm_accountid`, `pm_phrase`, `userid`, `transaction_id`, `amount`, `currency`, `customproduct`, `success_url`, `decline_url`, `back_url`, `response_text`, `transaction_status`, `entry_date`, `entry_ip`, `STATUS`) VALUES
	(1, 'U23568901', NULL, '12345', '1621791354', 10.00, 'USD', '{"currency":"USD","amount":"10"}', 'http://192.168.1.162/bitlogik_api/index.php/perfectmoney/success', 'http://192.168.1.162/bitlogik_api/index.php/perfectmoney/decline', 'http://192.168.1.162/bitlogik_api/index.php/perfectmoney/cancel', NULL, 'PENDING', '2021-05-23 23:06:05', '::1', 1),
	(2, 'U23568901', NULL, '12345', '1621792986', 11.00, 'USD', '{"currency":"USD","amount":"11"}', 'http://192.168.0.100:8080/middleware_api//index.php/perfectmoney/success', 'http://192.168.0.100:8080/middleware_api//index.php/perfectmoney/decline', 'http://192.168.0.100:8080/middleware_api//index.php/perfectmoney/cancel', NULL, 'PENDING', '2021-05-23 23:33:15', '192.168.0.100', 1),
	(3, 'U23568901', NULL, '12345', '1621793103', 11.00, 'USD', '{"currency":"USD","amount":"11"}', 'http://192.168.0.100:8080/middleware_api/index.php/perfectmoney/success', 'http://192.168.0.100:8080/middleware_api/index.php/perfectmoney/decline', 'http://192.168.0.100:8080/middleware_api/index.php/perfectmoney/cancel', '{"PAYEE_ACCOUNT":"U23568901","PAYMENT_AMOUNT":"11","PAYMENT_UNITS":"USD","PAYMENT_ID":"1621793103","SUGGESTED_MEMO":"","PAYMENT_BATCH_NUM":"0","UserId":"12345"}', 'FAILED', '2021-05-23 23:35:08', '192.168.0.100', 1);
/*!40000 ALTER TABLE `ci_pmoney_deposit` ENABLE KEYS */;

-- Dumping structure for table middleware_api.ci_recover_password
CREATE TABLE IF NOT EXISTS `ci_recover_password` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) unsigned NOT NULL,
  `token` char(40) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `date_added` datetime DEFAULT NULL,
  `entry_by` bigint(20) DEFAULT NULL,
  `entry_date` timestamp NOT NULL DEFAULT current_timestamp(),
  `ipaddress` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table middleware_api.ci_recover_password: ~0 rows (approximately)
DELETE FROM `ci_recover_password`;
/*!40000 ALTER TABLE `ci_recover_password` DISABLE KEYS */;
/*!40000 ALTER TABLE `ci_recover_password` ENABLE KEYS */;

-- Dumping structure for table middleware_api.ci_role
CREATE TABLE IF NOT EXISTS `ci_role` (
  `role_id` int(11) NOT NULL AUTO_INCREMENT,
  `role_name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `role_description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `role_selectable` tinyint(1) NOT NULL DEFAULT 1,
  PRIMARY KEY (`role_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table middleware_api.ci_role: ~1 rows (approximately)
DELETE FROM `ci_role`;
/*!40000 ALTER TABLE `ci_role` DISABLE KEYS */;
INSERT INTO `ci_role` (`role_id`, `role_name`, `role_description`, `role_selectable`) VALUES
	(1, 'Administrator', 'CAN NOT BE DELETED - All system permissions are active by default.', 1);
/*!40000 ALTER TABLE `ci_role` ENABLE KEYS */;

-- Dumping structure for table middleware_api.ci_role_permission
CREATE TABLE IF NOT EXISTS `ci_role_permission` (
  `role_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  `entry_by` bigint(20) DEFAULT NULL,
  `entry_date` timestamp NOT NULL DEFAULT current_timestamp(),
  `ipaddress` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table middleware_api.ci_role_permission: ~15 rows (approximately)
DELETE FROM `ci_role_permission`;
/*!40000 ALTER TABLE `ci_role_permission` DISABLE KEYS */;
INSERT INTO `ci_role_permission` (`role_id`, `permission_id`, `entry_by`, `entry_date`, `ipaddress`) VALUES
	(1, 1, NULL, '2021-01-01 00:00:00', NULL),
	(1, 2, NULL, '2021-01-01 00:00:00', NULL),
	(1, 3, NULL, '2021-01-01 00:00:00', NULL),
	(1, 4, NULL, '2021-01-01 00:00:00', NULL),
	(1, 5, NULL, '2021-01-01 00:00:00', NULL),
	(1, 6, NULL, '2021-01-01 00:00:00', NULL),
	(1, 7, NULL, '2021-01-01 00:00:00', NULL),
	(1, 8, NULL, '2021-01-01 00:00:00', NULL),
	(1, 9, NULL, '2021-01-01 00:00:00', NULL),
	(1, 10, NULL, '2021-01-01 00:00:00', NULL),
	(1, 11, NULL, '2021-01-01 00:00:00', NULL),
	(1, 12, NULL, '2021-01-01 00:00:00', NULL),
	(1, 13, NULL, '2021-01-01 00:00:00', NULL),
	(1, 14, NULL, '2021-01-01 00:00:00', NULL),
	(1, 15, NULL, '2021-01-01 00:00:00', NULL);
/*!40000 ALTER TABLE `ci_role_permission` ENABLE KEYS */;

-- Dumping structure for table middleware_api.ci_sessions
CREATE TABLE IF NOT EXISTS `ci_sessions` (
  `id` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `ip_address` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `timestamp` int(10) unsigned NOT NULL DEFAULT 0,
  `data` blob NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table middleware_api.ci_sessions: ~7 rows (approximately)
DELETE FROM `ci_sessions`;
/*!40000 ALTER TABLE `ci_sessions` DISABLE KEYS */;
INSERT INTO `ci_sessions` (`id`, `ip_address`, `timestamp`, `data`) VALUES
	('tk9ok1ljku9prbcnamab1t0s0s9eefni', '::1', 1622796401, _binary 0x5F5F63695F6C6173745F726567656E65726174657C693A313632323739363430313B),
	('qsnad0fbsaqpoolfa0d4ql191kf658qt', '::1', 1622796703, _binary 0x5F5F63695F6C6173745F726567656E65726174657C693A313632323739363730333B),
	('r5d9r54g1e6bp2qccr5l4t88j45hgpoo', '::1', 1622797190, _binary 0x5F5F63695F6C6173745F726567656E65726174657C693A313632323739373139303B),
	('0bt9jmc7pds14f8cj9quh2fhveu8cs3o', '::1', 1622797453, _binary 0x5F5F63695F6C6173745F726567656E65726174657C693A313632323739373139303B),
	('6gdq2v82dkkug29vupq6jo22dtdu33m8', '::1', 1622802940, _binary 0x5F5F63695F6C6173745F726567656E65726174657C693A313632323830323735383B),
	('g2e8lkm5cm0eafkntrmbmf48q6kcg1j5', '::1', 1622804636, _binary 0x5F5F63695F6C6173745F726567656E65726174657C693A313632323830343633363B),
	('tg184bvt6ms40mb1a48h6brh53mfdb4a', '::1', 1622806264, _binary 0x5F5F63695F6C6173745F726567656E65726174657C693A313632323830363236343B);
/*!40000 ALTER TABLE `ci_sessions` ENABLE KEYS */;

-- Dumping structure for table middleware_api.ci_setting
CREATE TABLE IF NOT EXISTS `ci_setting` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `value` text COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table middleware_api.ci_setting: ~32 rows (approximately)
DELETE FROM `ci_setting`;
/*!40000 ALTER TABLE `ci_setting` DISABLE KEYS */;
INSERT INTO `ci_setting` (`id`, `name`, `value`) VALUES
	(1, 'root_admin_username', 'administrator'),
	(2, 'cim_version', '3.2.1'),
	(3, 'login_enabled', '1'),
	(4, 'register_enabled', '0'),
	(5, 'members_per_page', '12'),
	(6, 'admin_email', 'admin@admin.com'),
	(7, 'home_page', 'My_dashboard'),
	(8, 'previous_url_after_login', '0'),
	(9, 'active_theme', 'bootstrap3'),
	(10, 'adminpanel_theme', 'adminpanel'),
	(11, 'login_attempts', '5'),
	(12, 'max_login_attempts', '30'),
	(13, 'email_protocol', '1'),
	(14, 'sendmail_path', '/usr/sbin/sendmail'),
	(15, 'smtp_host', 'ssl://smtp.googlemail.com'),
	(16, 'smtp_port', '465'),
	(17, 'smtp_user', 'sADXYMr5CFvcCYsMnPQhMsFkTEL1IqZ8KOKPK0f5UqD3+k+3Ii1IditRNQFrID86hRO1O+N+HphXL8chrHN6KA=='),
	(18, 'smtp_pass', 'jVm2gVtVe+DgKFK31Zj8wO5M+3PWDugRfZIl5dAt/D0mvzzkDoxjNsbKzX+QE322SuR70J7ROXLF1hiHy+2d/A=='),
	(19, 'site_title', 'Bitlogik Middleware'),
	(20, 'cookie_expires', '259200'),
	(21, 'password_link_expires', '1800'),
	(22, 'activation_link_expires', '43200'),
	(23, 'disable_all', '0'),
	(24, 'site_disabled_text', 'This website is momentarily offline.'),
	(25, 'remember_me_enabled', '1'),
	(26, 'recaptchav2_enabled', '0'),
	(27, 'recaptchav2_site_key', ''),
	(28, 'recaptchav2_secret', ''),
	(29, 'oauth_enabled', '1'),
	(30, 'picture_max_upload_size', '100'),
	(31, 'allow_login_by_email', '1'),
	(32, 'genome_theme', 'gateway');
/*!40000 ALTER TABLE `ci_setting` ENABLE KEYS */;

-- Dumping structure for table middleware_api.ci_update_log
CREATE TABLE IF NOT EXISTS `ci_update_log` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `ref_table` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `ref_table_id` bigint(20) NOT NULL,
  `ref_column` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `old_value` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `new_value` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `update_by` bigint(20) DEFAULT NULL,
  `update_date` timestamp NOT NULL DEFAULT current_timestamp(),
  `ipaddress` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `distcode` varchar(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `projectcode` varchar(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `sectorcode` varchar(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `awccode` varchar(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `active` int(11) NOT NULL DEFAULT 1,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table middleware_api.ci_update_log: ~0 rows (approximately)
DELETE FROM `ci_update_log`;
/*!40000 ALTER TABLE `ci_update_log` DISABLE KEYS */;
/*!40000 ALTER TABLE `ci_update_log` ENABLE KEYS */;

-- Dumping structure for table middleware_api.ci_user
CREATE TABLE IF NOT EXISTS `ci_user` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(24) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `date_registered` datetime NOT NULL,
  `last_login` datetime NOT NULL,
  `first_name` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_name` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `active` tinyint(1) NOT NULL DEFAULT 0,
  `banned` tinyint(1) NOT NULL DEFAULT 0,
  `login_attempts` tinyint(4) NOT NULL DEFAULT 0,
  `gender` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `profile_img` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'members_generic.png',
  `entry_by` bigint(20) DEFAULT NULL,
  `entry_date` timestamp NOT NULL DEFAULT current_timestamp(),
  `ipaddress` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table middleware_api.ci_user: ~1 rows (approximately)
DELETE FROM `ci_user`;
/*!40000 ALTER TABLE `ci_user` DISABLE KEYS */;
INSERT INTO `ci_user` (`user_id`, `username`, `password`, `email`, `date_registered`, `last_login`, `first_name`, `last_name`, `active`, `banned`, `login_attempts`, `gender`, `profile_img`, `entry_by`, `entry_date`, `ipaddress`) VALUES
	(1, 'administrator', '$2y$10$KIWWk8hlS8VcUov7NmYLH.7klzHIF8hUUIPoqIQuzuKfDn7D3Lz0S', 'admin@admin.com', '2021-01-01 00:00:00', '2021-06-03 09:43:46', 'Administrator', '', 1, 0, 0, NULL, 'members_generic.png', NULL, '2021-01-01 00:00:00', NULL);
/*!40000 ALTER TABLE `ci_user` ENABLE KEYS */;

-- Dumping structure for table middleware_api.ci_user_cookie_part
CREATE TABLE IF NOT EXISTS `ci_user_cookie_part` (
  `user_id` int(11) NOT NULL,
  `cookie_part` char(32) COLLATE utf8_unicode_ci NOT NULL,
  `ip_address` varchar(45) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table middleware_api.ci_user_cookie_part: ~3 rows (approximately)
DELETE FROM `ci_user_cookie_part`;
/*!40000 ALTER TABLE `ci_user_cookie_part` DISABLE KEYS */;
INSERT INTO `ci_user_cookie_part` (`user_id`, `cookie_part`, `ip_address`) VALUES
	(1, 'eba231c0aeef53fd5f63776203c77a57', '::1'),
	(1, '5f4e13c4058aaa485e7375e771b031bf', '192.168.0.100'),
	(1, '26269576c066143086b124743f6f6d74', '127.0.0.1');
/*!40000 ALTER TABLE `ci_user_cookie_part` ENABLE KEYS */;

-- Dumping structure for table middleware_api.ci_user_role
CREATE TABLE IF NOT EXISTS `ci_user_role` (
  `user_id` int(11) unsigned NOT NULL,
  `role_id` int(11) unsigned NOT NULL,
  `entry_by` bigint(20) DEFAULT NULL,
  `entry_date` timestamp NOT NULL DEFAULT current_timestamp(),
  `ipaddress` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table middleware_api.ci_user_role: ~1 rows (approximately)
DELETE FROM `ci_user_role`;
/*!40000 ALTER TABLE `ci_user_role` DISABLE KEYS */;
INSERT INTO `ci_user_role` (`user_id`, `role_id`, `entry_by`, `entry_date`, `ipaddress`) VALUES
	(1, 1, NULL, '2021-01-01 00:00:00', NULL);
/*!40000 ALTER TABLE `ci_user_role` ENABLE KEYS */;

-- Dumping structure for table middleware_api.keys
CREATE TABLE IF NOT EXISTS `keys` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `key` varchar(40) NOT NULL,
  `level` int(2) NOT NULL,
  `ignore_limits` tinyint(1) NOT NULL DEFAULT 0,
  `is_private_key` tinyint(1) NOT NULL DEFAULT 0,
  `ip_addresses` text DEFAULT NULL,
  `date_created` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- Dumping data for table middleware_api.keys: ~1 rows (approximately)
DELETE FROM `keys`;
/*!40000 ALTER TABLE `keys` DISABLE KEYS */;
INSERT INTO `keys` (`id`, `key`, `level`, `ignore_limits`, `is_private_key`, `ip_addresses`, `date_created`) VALUES
	(1, 'm9isGH4my9HlRMYsMCQxXqTha9GXoslh', 0, 0, 0, NULL, 0);
/*!40000 ALTER TABLE `keys` ENABLE KEYS */;

-- Dumping structure for table middleware_api.logs
CREATE TABLE IF NOT EXISTS `logs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uri` varchar(255) NOT NULL,
  `method` varchar(6) NOT NULL,
  `params` text DEFAULT NULL,
  `api_key` varchar(40) NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `time` int(11) NOT NULL,
  `rtime` float DEFAULT NULL,
  `authorized` varchar(1) NOT NULL,
  `response_code` smallint(3) DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=86 DEFAULT CHARSET=utf8;

-- Dumping data for table middleware_api.logs: ~85 rows (approximately)
DELETE FROM `logs`;
/*!40000 ALTER TABLE `logs` DISABLE KEYS */;
INSERT INTO `logs` (`id`, `uri`, `method`, `params`, `api_key`, `ip_address`, `time`, `rtime`, `authorized`, `response_code`) VALUES
	(1, 'middleware/checkout', 'get', '{"Host":"localhost","Connection":"keep-alive","Pragma":"no-cache","Cache-Control":"no-cache","sec-ch-ua":"\\"Chromium\\";v=\\"88\\", \\"Google Chrome\\";v=\\"88\\", \\";Not A Brand\\";v=\\"99\\"","sec-ch-ua-mobile":"?0","Upgrade-Insecure-Requests":"1","User-Agent":"Mozilla\\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\\/537.36 (KHTML, like Gecko) Chrome\\/88.0.4324.104 Safari\\/537.36","Accept":"text\\/html,application\\/xhtml+xml,application\\/xml;q=0.9,image\\/avif,image\\/webp,image\\/apng,*\\/*;q=0.8,application\\/signed-exchange;v=b3;q=0.9","Sec-Fetch-Site":"none","Sec-Fetch-Mode":"navigate","Sec-Fetch-User":"?1","Sec-Fetch-Dest":"document","Accept-Encoding":"gzip, deflate, br","Accept-Language":"en-US,en;q=0.9"}', '', '::1', 1612328523, 0.125129, '0', 405),
	(2, 'middleware/checkout', 'get', '{"Host":"localhost","Connection":"keep-alive","Pragma":"no-cache","Cache-Control":"no-cache","sec-ch-ua":"\\"Chromium\\";v=\\"88\\", \\"Google Chrome\\";v=\\"88\\", \\";Not A Brand\\";v=\\"99\\"","sec-ch-ua-mobile":"?0","Upgrade-Insecure-Requests":"1","User-Agent":"Mozilla\\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\\/537.36 (KHTML, like Gecko) Chrome\\/88.0.4324.104 Safari\\/537.36","Accept":"text\\/html,application\\/xhtml+xml,application\\/xml;q=0.9,image\\/avif,image\\/webp,image\\/apng,*\\/*;q=0.8,application\\/signed-exchange;v=b3;q=0.9","Sec-Fetch-Site":"none","Sec-Fetch-Mode":"navigate","Sec-Fetch-User":"?1","Sec-Fetch-Dest":"document","Accept-Encoding":"gzip, deflate, br","Accept-Language":"en-US,en;q=0.9"}', '', '::1', 1612328523, 0.125129, '1', 0),
	(3, 'middleware/checkout', 'get', '{"Host":"localhost","Connection":"keep-alive","Pragma":"no-cache","Cache-Control":"no-cache","sec-ch-ua":"\\"Chromium\\";v=\\"88\\", \\"Google Chrome\\";v=\\"88\\", \\";Not A Brand\\";v=\\"99\\"","sec-ch-ua-mobile":"?0","Upgrade-Insecure-Requests":"1","User-Agent":"Mozilla\\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\\/537.36 (KHTML, like Gecko) Chrome\\/88.0.4324.104 Safari\\/537.36","Accept":"text\\/html,application\\/xhtml+xml,application\\/xml;q=0.9,image\\/avif,image\\/webp,image\\/apng,*\\/*;q=0.8,application\\/signed-exchange;v=b3;q=0.9","Sec-Fetch-Site":"none","Sec-Fetch-Mode":"navigate","Sec-Fetch-User":"?1","Sec-Fetch-Dest":"document","Accept-Encoding":"gzip, deflate, br","Accept-Language":"en-US,en;q=0.9"}', '', '::1', 1612328595, NULL, '0', 403),
	(4, 'middleware/checkout', 'get', '{"Host":"localhost","Connection":"keep-alive","Pragma":"no-cache","Cache-Control":"no-cache","sec-ch-ua":"\\"Chromium\\";v=\\"88\\", \\"Google Chrome\\";v=\\"88\\", \\";Not A Brand\\";v=\\"99\\"","sec-ch-ua-mobile":"?0","Upgrade-Insecure-Requests":"1","User-Agent":"Mozilla\\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\\/537.36 (KHTML, like Gecko) Chrome\\/88.0.4324.104 Safari\\/537.36","Accept":"text\\/html,application\\/xhtml+xml,application\\/xml;q=0.9,image\\/avif,image\\/webp,image\\/apng,*\\/*;q=0.8,application\\/signed-exchange;v=b3;q=0.9","Sec-Fetch-Site":"none","Sec-Fetch-Mode":"navigate","Sec-Fetch-User":"?1","Sec-Fetch-Dest":"document","Accept-Encoding":"gzip, deflate, br","Accept-Language":"en-US,en;q=0.9"}', '', '::1', 1612328595, 0.196435, '1', 0),
	(5, 'middleware/checkout', 'post', '{"key":"m9isGH4my9HlRMYsMCQxXqTha9GXoslh","User-Agent":"PostmanRuntime\\/7.26.10","Accept":"*\\/*","Postman-Token":"4c81edf1-9a22-4bbf-b8ad-3392229fa46a","Host":"localhost","Accept-Encoding":"gzip, deflate, br","Connection":"keep-alive","Content-Type":"application\\/x-www-form-urlencoded","Content-Length":"13","user":"sandhiya"}', '', '::1', 1612328635, NULL, '0', 405),
	(6, 'middleware/checkout', 'post', '{"key":"m9isGH4my9HlRMYsMCQxXqTha9GXoslh","User-Agent":"PostmanRuntime\\/7.26.10","Accept":"*\\/*","Postman-Token":"4c81edf1-9a22-4bbf-b8ad-3392229fa46a","Host":"localhost","Accept-Encoding":"gzip, deflate, br","Connection":"keep-alive","Content-Type":"application\\/x-www-form-urlencoded","Content-Length":"13","user":"sandhiya"}', '', '::1', 1612328635, 0.191326, '1', 0),
	(7, 'middleware/checkout', 'post', '{"key":"m9isGH4my9HlRMYsMCQxXqTha9GXoslh","User-Agent":"PostmanRuntime\\/7.26.10","Accept":"*\\/*","Postman-Token":"41a95a26-5020-4c2d-8646-833ee79e7448","Host":"localhost","Accept-Encoding":"gzip, deflate, br","Connection":"keep-alive","Content-Type":"application\\/x-www-form-urlencoded","Content-Length":"13","user":"sandhiya"}', '', '::1', 1612328654, NULL, '0', 403),
	(8, 'middleware/checkout', 'post', '{"key":"m9isGH4my9HlRMYsMCQxXqTha9GXoslh","User-Agent":"PostmanRuntime\\/7.26.10","Accept":"*\\/*","Postman-Token":"41a95a26-5020-4c2d-8646-833ee79e7448","Host":"localhost","Accept-Encoding":"gzip, deflate, br","Connection":"keep-alive","Content-Type":"application\\/x-www-form-urlencoded","Content-Length":"13","user":"sandhiya"}', '', '::1', 1612328654, 0.114639, '1', 0),
	(9, 'middleware/checkout', 'post', '{"key":"m9isGH4my9HlRMYsMCQxXqTha9GXoslh","User-Agent":"PostmanRuntime\\/7.26.10","Accept":"*\\/*","Postman-Token":"100f245a-d688-4fa3-8822-5baeb23b0a98","Host":"localhost","Accept-Encoding":"gzip, deflate, br","Connection":"keep-alive","Content-Type":"application\\/x-www-form-urlencoded","Content-Length":"13","user":"sandhiya"}', '', '::1', 1612328673, NULL, '0', 403),
	(10, 'middleware/checkout', 'post', '{"key":"m9isGH4my9HlRMYsMCQxXqTha9GXoslh","User-Agent":"PostmanRuntime\\/7.26.10","Accept":"*\\/*","Postman-Token":"100f245a-d688-4fa3-8822-5baeb23b0a98","Host":"localhost","Accept-Encoding":"gzip, deflate, br","Connection":"keep-alive","Content-Type":"application\\/x-www-form-urlencoded","Content-Length":"13","user":"sandhiya"}', '', '::1', 1612328673, 0.095892, '1', 0),
	(11, 'middleware/checkout', 'post', '{"key":"m9isGH4my9HlRMYsMCQxXqTha9GXoslh","User-Agent":"PostmanRuntime\\/7.26.10","Accept":"*\\/*","Postman-Token":"1dbf75c7-b599-4c6c-806e-d086b1cbc69e","Host":"localhost","Accept-Encoding":"gzip, deflate, br","Connection":"keep-alive","Content-Type":"application\\/x-www-form-urlencoded","Content-Length":"13","user":"sandhiya"}', '', '::1', 1612328695, NULL, '0', 403),
	(12, 'middleware/checkout', 'post', '{"key":"m9isGH4my9HlRMYsMCQxXqTha9GXoslh","User-Agent":"PostmanRuntime\\/7.26.10","Accept":"*\\/*","Postman-Token":"1dbf75c7-b599-4c6c-806e-d086b1cbc69e","Host":"localhost","Accept-Encoding":"gzip, deflate, br","Connection":"keep-alive","Content-Type":"application\\/x-www-form-urlencoded","Content-Length":"13","user":"sandhiya"}', '', '::1', 1612328695, 0.10983, '1', 0),
	(13, 'middleware/checkout', 'post', '{"User-Agent":"PostmanRuntime\\/7.26.10","Accept":"*\\/*","Postman-Token":"d30e5e1e-a60c-404e-827f-7eca6d7e5d3b","Host":"localhost","Accept-Encoding":"gzip, deflate, br","Connection":"keep-alive","Content-Type":"application\\/x-www-form-urlencoded","Content-Length":"50","user":"sandhiya","key":"m9isGH4my9HlRMYsMCQxXqTha9GXoslh"}', '', '::1', 1612328764, NULL, '0', 403),
	(14, 'middleware/checkout', 'post', '{"User-Agent":"PostmanRuntime\\/7.26.10","Accept":"*\\/*","Postman-Token":"d30e5e1e-a60c-404e-827f-7eca6d7e5d3b","Host":"localhost","Accept-Encoding":"gzip, deflate, br","Connection":"keep-alive","Content-Type":"application\\/x-www-form-urlencoded","Content-Length":"50","user":"sandhiya","key":"m9isGH4my9HlRMYsMCQxXqTha9GXoslh"}', '', '::1', 1612328764, 0.156212, '1', 0),
	(15, 'middleware/checkout', 'post', '{"key":"m9isGH4my9HlRMYsMCQxXqTha9GXoslh","Authorization":"Basic YWRtaW46MTIzNA==","User-Agent":"PostmanRuntime\\/7.26.10","Accept":"*\\/*","Postman-Token":"f1652e35-6f23-4acd-9d24-b40c05de220a","Host":"localhost","Accept-Encoding":"gzip, deflate, br","Connection":"keep-alive","Content-Type":"application\\/x-www-form-urlencoded","Content-Length":"13","user":"sandhiya"}', '', '::1', 1612329034, NULL, '0', 403),
	(16, 'middleware/checkout', 'post', '{"key":"m9isGH4my9HlRMYsMCQxXqTha9GXoslh","Authorization":"Basic YWRtaW46MTIzNA==","User-Agent":"PostmanRuntime\\/7.26.10","Accept":"*\\/*","Postman-Token":"f1652e35-6f23-4acd-9d24-b40c05de220a","Host":"localhost","Accept-Encoding":"gzip, deflate, br","Connection":"keep-alive","Content-Type":"application\\/x-www-form-urlencoded","Content-Length":"13","user":"sandhiya"}', '', '::1', 1612329034, 0.13465, '1', 0),
	(17, 'middleware/checkout', 'post', '{"Authorization":"Basic YWRtaW46MTIzNA==","User-Agent":"PostmanRuntime\\/7.26.10","Accept":"*\\/*","Postman-Token":"86622e78-4811-4dd0-a297-3a085f2048c6","Host":"localhost","Accept-Encoding":"gzip, deflate, br","Connection":"keep-alive","Content-Type":"application\\/x-www-form-urlencoded","Content-Length":"50","user":"sandhiya","key":"m9isGH4my9HlRMYsMCQxXqTha9GXoslh"}', '', '::1', 1612329044, NULL, '0', 403),
	(18, 'middleware/checkout', 'post', '{"Authorization":"Basic YWRtaW46MTIzNA==","User-Agent":"PostmanRuntime\\/7.26.10","Accept":"*\\/*","Postman-Token":"86622e78-4811-4dd0-a297-3a085f2048c6","Host":"localhost","Accept-Encoding":"gzip, deflate, br","Connection":"keep-alive","Content-Type":"application\\/x-www-form-urlencoded","Content-Length":"50","user":"sandhiya","key":"m9isGH4my9HlRMYsMCQxXqTha9GXoslh"}', '', '::1', 1612329044, 0.135419, '1', 0),
	(19, 'middleware/checkout', 'post', '{"User-Agent":"PostmanRuntime\\/7.26.10","Accept":"*\\/*","Postman-Token":"e5240f0c-2c2e-4898-8af0-994ccbb166e1","Host":"localhost","Accept-Encoding":"gzip, deflate, br","Connection":"keep-alive","Content-Type":"application\\/x-www-form-urlencoded","Content-Length":"50","user":"sandhiya","key":"m9isGH4my9HlRMYsMCQxXqTha9GXoslh"}', '', '::1', 1612329087, NULL, '0', 403),
	(20, 'middleware/checkout', 'post', '{"User-Agent":"PostmanRuntime\\/7.26.10","Accept":"*\\/*","Postman-Token":"e5240f0c-2c2e-4898-8af0-994ccbb166e1","Host":"localhost","Accept-Encoding":"gzip, deflate, br","Connection":"keep-alive","Content-Type":"application\\/x-www-form-urlencoded","Content-Length":"50","user":"sandhiya","key":"m9isGH4my9HlRMYsMCQxXqTha9GXoslh"}', '', '::1', 1612329087, 0.137932, '1', 0),
	(21, 'middleware/checkout', 'post', '{"key":"m9isGH4my9HlRMYsMCQxXqTha9GXoslh","User-Agent":"PostmanRuntime\\/7.26.10","Accept":"*\\/*","Postman-Token":"7e0fc303-0a82-4066-858a-6d582139067b","Host":"localhost","Accept-Encoding":"gzip, deflate, br","Connection":"keep-alive","Content-Type":"application\\/x-www-form-urlencoded","Content-Length":"13","user":"sandhiya"}', '', '::1', 1612329101, NULL, '0', 403),
	(22, 'middleware/checkout', 'post', '{"key":"m9isGH4my9HlRMYsMCQxXqTha9GXoslh","User-Agent":"PostmanRuntime\\/7.26.10","Accept":"*\\/*","Postman-Token":"7e0fc303-0a82-4066-858a-6d582139067b","Host":"localhost","Accept-Encoding":"gzip, deflate, br","Connection":"keep-alive","Content-Type":"application\\/x-www-form-urlencoded","Content-Length":"13","user":"sandhiya"}', '', '::1', 1612329101, 0.118634, '1', 0),
	(23, 'middleware/checkout', 'post', '{"User-Agent":"PostmanRuntime\\/7.26.10","Accept":"*\\/*","Postman-Token":"2861b29c-309e-46d8-ac1b-f52c79bb02c5","Host":"localhost","Accept-Encoding":"gzip, deflate, br","Connection":"keep-alive","Content-Type":"application\\/x-www-form-urlencoded","Content-Length":"50","user":"sandhiya","key":"m9isGH4my9HlRMYsMCQxXqTha9GXoslh"}', '', '::1', 1612329181, NULL, '0', 403),
	(24, 'middleware/checkout', 'post', '{"User-Agent":"PostmanRuntime\\/7.26.10","Accept":"*\\/*","Postman-Token":"2861b29c-309e-46d8-ac1b-f52c79bb02c5","Host":"localhost","Accept-Encoding":"gzip, deflate, br","Connection":"keep-alive","Content-Type":"application\\/x-www-form-urlencoded","Content-Length":"50","user":"sandhiya","key":"m9isGH4my9HlRMYsMCQxXqTha9GXoslh"}', '', '::1', 1612329181, 0.176642, '1', 0),
	(25, 'middleware/checkout', 'post', '{"User-Agent":"PostmanRuntime\\/7.26.10","Accept":"*\\/*","Postman-Token":"e5460585-2b57-4e5f-a2ae-a8853f2b7d2a","Host":"localhost","Accept-Encoding":"gzip, deflate, br","Connection":"keep-alive","Content-Type":"application\\/x-www-form-urlencoded","Content-Length":"56","user":"sandhiya","X-API-KEY":"m9isGH4my9HlRMYsMCQxXqTha9GXoslh"}', 'm9isGH4my9HlRMYsMCQxXqTha9GXoslh', '::1', 1612329481, NULL, '0', 403),
	(26, 'middleware/checkout', 'post', '{"User-Agent":"PostmanRuntime\\/7.26.10","Accept":"*\\/*","Postman-Token":"e5460585-2b57-4e5f-a2ae-a8853f2b7d2a","Host":"localhost","Accept-Encoding":"gzip, deflate, br","Connection":"keep-alive","Content-Type":"application\\/x-www-form-urlencoded","Content-Length":"56","user":"sandhiya","X-API-KEY":"m9isGH4my9HlRMYsMCQxXqTha9GXoslh"}', 'm9isGH4my9HlRMYsMCQxXqTha9GXoslh', '::1', 1612329481, 0.136083, '1', 0),
	(27, 'middleware/checkout', 'post', '{"User-Agent":"PostmanRuntime\\/7.26.10","Accept":"*\\/*","Postman-Token":"a7db202f-c43e-49f8-8b03-63b7ef1a0175","Host":"localhost","Accept-Encoding":"gzip, deflate, br","Connection":"keep-alive","Content-Type":"application\\/x-www-form-urlencoded","Content-Length":"56","user":"sandhiya","X-API-KEY":"m9isGH4my9HlRMYsMCQxXqTha9GXoslh"}', 'm9isGH4my9HlRMYsMCQxXqTha9GXoslh', '::1', 1612329608, NULL, '0', 403),
	(28, 'middleware/checkout', 'post', '{"User-Agent":"PostmanRuntime\\/7.26.10","Accept":"*\\/*","Postman-Token":"a7db202f-c43e-49f8-8b03-63b7ef1a0175","Host":"localhost","Accept-Encoding":"gzip, deflate, br","Connection":"keep-alive","Content-Type":"application\\/x-www-form-urlencoded","Content-Length":"56","user":"sandhiya","X-API-KEY":"m9isGH4my9HlRMYsMCQxXqTha9GXoslh"}', 'm9isGH4my9HlRMYsMCQxXqTha9GXoslh', '::1', 1612329608, 0.124225, '1', 0),
	(29, 'middleware/checkout', 'post', '{"User-Agent":"PostmanRuntime\\/7.26.10","Accept":"*\\/*","Postman-Token":"201cc360-b3a8-4aa1-a453-2aaf4491b576","Host":"localhost","Accept-Encoding":"gzip, deflate, br","Connection":"keep-alive","Content-Type":"application\\/x-www-form-urlencoded","Content-Length":"56","user":"sandhiya","X-API-KEY":"m9isGH4my9HlRMYsMCQxXqTha9GXoslh"}', 'm9isGH4my9HlRMYsMCQxXqTha9GXoslh', '::1', 1612329703, NULL, '0', 403),
	(30, 'middleware/checkout', 'post', '{"User-Agent":"PostmanRuntime\\/7.26.10","Accept":"*\\/*","Postman-Token":"201cc360-b3a8-4aa1-a453-2aaf4491b576","Host":"localhost","Accept-Encoding":"gzip, deflate, br","Connection":"keep-alive","Content-Type":"application\\/x-www-form-urlencoded","Content-Length":"56","user":"sandhiya","X-API-KEY":"m9isGH4my9HlRMYsMCQxXqTha9GXoslh"}', 'm9isGH4my9HlRMYsMCQxXqTha9GXoslh', '::1', 1612329703, 0.139103, '1', 0),
	(31, 'middleware/checkout', 'post', '{"User-Agent":"PostmanRuntime\\/7.26.10","Accept":"*\\/*","Postman-Token":"96ccfec1-0ca2-4a7c-acfa-de517316a8aa","Host":"localhost","Accept-Encoding":"gzip, deflate, br","Connection":"keep-alive","Content-Type":"application\\/x-www-form-urlencoded","Content-Length":"56","user":"sandhiya","X-API-KEY":"m9isGH4my9HlRMYsMCQxXqTha9GXoslh"}', 'm9isGH4my9HlRMYsMCQxXqTha9GXoslh', '::1', 1612329787, 0.115495, '1', 0),
	(32, 'middleware/checkout', 'post', '{"X-API-KEY":"m9isGH4my9HlRMYsMCQxXqTha9GXoslh","User-Agent":"PostmanRuntime\\/7.26.10","Accept":"*\\/*","Postman-Token":"5847730e-3ff8-41ae-9ad6-ec70419461b4","Host":"localhost","Accept-Encoding":"gzip, deflate, br","Connection":"keep-alive","Content-Type":"application\\/x-www-form-urlencoded","Content-Length":"13","user":"sandhiya"}', 'm9isGH4my9HlRMYsMCQxXqTha9GXoslh', '::1', 1612329832, 0.079443, '1', 0),
	(33, 'middleware/checkout', 'post', '{"X-API-KEY":"m9isGH4my9HlRMYsMCQxXqTha9GXosl","User-Agent":"PostmanRuntime\\/7.26.10","Accept":"*\\/*","Postman-Token":"5d5cb5a2-024e-4e24-a408-e2856e08be8b","Host":"localhost","Accept-Encoding":"gzip, deflate, br","Connection":"keep-alive","Content-Type":"application\\/x-www-form-urlencoded","Content-Length":"13","user":"sandhiya"}', '', '::1', 1612329845, NULL, '0', 403),
	(34, 'middleware/checkout', 'post', '{"X-API-KEY":"m9isGH4my9HlRMYsMCQxXqTha9GXosl","User-Agent":"PostmanRuntime\\/7.26.10","Accept":"*\\/*","Postman-Token":"5d5cb5a2-024e-4e24-a408-e2856e08be8b","Host":"localhost","Accept-Encoding":"gzip, deflate, br","Connection":"keep-alive","Content-Type":"application\\/x-www-form-urlencoded","Content-Length":"13","user":"sandhiya"}', '', '::1', 1612329846, 0.240716, '1', 0),
	(35, 'middleware/checkout', 'post', '{"X-API-KEY":"m9isGH4my9HlRMYsMCQxXqTha9GXoslh","User-Agent":"PostmanRuntime\\/7.26.10","Accept":"*\\/*","Postman-Token":"8b1a5067-bd0c-4418-bf0d-e865efae4808","Host":"localhost","Accept-Encoding":"gzip, deflate, br","Connection":"keep-alive","Content-Type":"application\\/x-www-form-urlencoded","Content-Length":"13","user":"sandhiya"}', 'm9isGH4my9HlRMYsMCQxXqTha9GXoslh', '::1', 1612330022, 0.0779128, '1', 0),
	(36, 'middleware/checkout', 'post', '{"X-API-KEY":"m9isGH4my9HlRMYsMCQxXqTha9GXosl","User-Agent":"PostmanRuntime\\/7.26.10","Accept":"*\\/*","Postman-Token":"1f692975-4f19-4a94-8b40-8fa785a0aba4","Host":"localhost","Accept-Encoding":"gzip, deflate, br","Connection":"keep-alive","Content-Type":"application\\/x-www-form-urlencoded","Content-Length":"13","user":"sandhiya"}', '', '::1', 1612330027, NULL, '0', 403),
	(37, 'middleware/checkout', 'post', '{"X-API-KEY":"m9isGH4my9HlRMYsMCQxXqTha9GXosl","User-Agent":"PostmanRuntime\\/7.26.10","Accept":"*\\/*","Postman-Token":"1f692975-4f19-4a94-8b40-8fa785a0aba4","Host":"localhost","Accept-Encoding":"gzip, deflate, br","Connection":"keep-alive","Content-Type":"application\\/x-www-form-urlencoded","Content-Length":"13","user":"sandhiya"}', '', '::1', 1612330027, 0.0872691, '1', 0),
	(38, 'middleware/checkout', 'post', '{"X-API-KEY":"m9isGH4my9HlRMYsMCQxXqTha9GXoslh","Authorization":"Basic bWlkZGxld2FyZTptaWRkbGV3YXJl","User-Agent":"PostmanRuntime\\/7.26.10","Accept":"*\\/*","Postman-Token":"d3500438-b1dd-488b-9d2e-b1056dbde7df","Host":"localhost","Accept-Encoding":"gzip, deflate, br","Connection":"keep-alive","Content-Type":"application\\/x-www-form-urlencoded","Content-Length":"13","user":"sandhiya"}', 'm9isGH4my9HlRMYsMCQxXqTha9GXoslh', '::1', 1612330262, 0.0380709, '1', 0),
	(39, 'middleware/checkout', 'post', '{"X-API-KEY":"m9isGH4my9HlRMYsMCQxXqTha9GXosl","Authorization":"Basic bWlkZGxld2FyZTptaWRkbGV3YXJl","User-Agent":"PostmanRuntime\\/7.26.10","Accept":"*\\/*","Postman-Token":"4e9a0413-c903-4c2d-8ff8-4caca3b20572","Host":"localhost","Accept-Encoding":"gzip, deflate, br","Connection":"keep-alive","Content-Type":"application\\/x-www-form-urlencoded","Content-Length":"55","user":"sandhiya"}', '', '::1', 1612330272, NULL, '0', 403),
	(40, 'middleware/checkout', 'post', '{"X-API-KEY":"m9isGH4my9HlRMYsMCQxXqTha9GXosl","Authorization":"Basic bWlkZGxld2FyZTptaWRkbGV3YXJl","User-Agent":"PostmanRuntime\\/7.26.10","Accept":"*\\/*","Postman-Token":"4e9a0413-c903-4c2d-8ff8-4caca3b20572","Host":"localhost","Accept-Encoding":"gzip, deflate, br","Connection":"keep-alive","Content-Type":"application\\/x-www-form-urlencoded","Content-Length":"55","user":"sandhiya"}', '', '::1', 1612330272, 0.198984, '1', 0),
	(41, 'middleware/checkout', 'post', '{"X-API-KEY":"m9isGH4my9HlRMYsMCQxXqTha9GXoslh","Authorization":"Basic bWlkZGxld2FyZTptaWRkbGV3YXJl","User-Agent":"PostmanRuntime\\/7.26.10","Accept":"*\\/*","Postman-Token":"162f959d-dfbf-4e74-8f2e-4a021c5111f3","Host":"localhost","Accept-Encoding":"gzip, deflate, br","Connection":"keep-alive","Content-Type":"application\\/x-www-form-urlencoded","Content-Length":"13","user":"sandhiya"}', 'm9isGH4my9HlRMYsMCQxXqTha9GXoslh', '::1', 1612330281, 0.0603349, '1', 0),
	(42, 'middleware/checkout', 'post', '{"X-API-KEY":"m9isGH4my9HlRMYsMCQxXqTha9GXosl","Authorization":"Basic bWlkZGxld2FyZTptaWRkbGV3YXJl","User-Agent":"PostmanRuntime\\/7.26.10","Accept":"*\\/*","Postman-Token":"2990766c-e391-4c70-823c-a9b5c9d26238","Host":"localhost","Accept-Encoding":"gzip, deflate, br","Connection":"keep-alive","Content-Type":"application\\/x-www-form-urlencoded","Content-Length":"13","user":"sandhiya"}', '', '::1', 1612330286, NULL, '0', 403),
	(43, 'middleware/checkout', 'post', '{"X-API-KEY":"m9isGH4my9HlRMYsMCQxXqTha9GXosl","Authorization":"Basic bWlkZGxld2FyZTptaWRkbGV3YXJl","User-Agent":"PostmanRuntime\\/7.26.10","Accept":"*\\/*","Postman-Token":"2990766c-e391-4c70-823c-a9b5c9d26238","Host":"localhost","Accept-Encoding":"gzip, deflate, br","Connection":"keep-alive","Content-Type":"application\\/x-www-form-urlencoded","Content-Length":"13","user":"sandhiya"}', '', '::1', 1612330286, 0.131234, '1', 0),
	(44, 'middleware/checkout', 'post', '{"X-API-KEY":"m9isGH4my9HlRMYsMCQxXqTha9GXoslh","Authorization":"Basic bWlkZGxld2FyZTptaWRkbGV3YXJl","User-Agent":"PostmanRuntime\\/7.26.10","Accept":"*\\/*","Postman-Token":"cf62b443-e86a-4dff-8e72-95075356eea1","Host":"localhost","Accept-Encoding":"gzip, deflate, br","Connection":"keep-alive","Content-Type":"application\\/x-www-form-urlencoded","Content-Length":"13","user":"sandhiya"}', 'm9isGH4my9HlRMYsMCQxXqTha9GXoslh', '::1', 1612330293, 0.0984089, '1', 0),
	(45, 'middleware/checkout', 'get', '{"Host":"localhost","Connection":"keep-alive","Cache-Control":"max-age=0","sec-ch-ua":"\\"Chromium\\";v=\\"88\\", \\"Google Chrome\\";v=\\"88\\", \\";Not A Brand\\";v=\\"99\\"","sec-ch-ua-mobile":"?0","Upgrade-Insecure-Requests":"1","User-Agent":"Mozilla\\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\\/537.36 (KHTML, like Gecko) Chrome\\/88.0.4324.104 Safari\\/537.36","Accept":"text\\/html,application\\/xhtml+xml,application\\/xml;q=0.9,image\\/avif,image\\/webp,image\\/apng,*\\/*;q=0.8,application\\/signed-exchange;v=b3;q=0.9","Sec-Fetch-Site":"none","Sec-Fetch-Mode":"navigate","Sec-Fetch-User":"?1","Sec-Fetch-Dest":"document","Accept-Encoding":"gzip, deflate, br","Accept-Language":"en-US,en;q=0.9"}', '', '::1', 1612355225, NULL, '0', 405),
	(46, 'middleware/checkout', 'get', '{"Host":"localhost","Connection":"keep-alive","Cache-Control":"max-age=0","sec-ch-ua":"\\"Chromium\\";v=\\"88\\", \\"Google Chrome\\";v=\\"88\\", \\";Not A Brand\\";v=\\"99\\"","sec-ch-ua-mobile":"?0","Upgrade-Insecure-Requests":"1","User-Agent":"Mozilla\\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\\/537.36 (KHTML, like Gecko) Chrome\\/88.0.4324.104 Safari\\/537.36","Accept":"text\\/html,application\\/xhtml+xml,application\\/xml;q=0.9,image\\/avif,image\\/webp,image\\/apng,*\\/*;q=0.8,application\\/signed-exchange;v=b3;q=0.9","Sec-Fetch-Site":"none","Sec-Fetch-Mode":"navigate","Sec-Fetch-User":"?1","Sec-Fetch-Dest":"document","Accept-Encoding":"gzip, deflate, br","Accept-Language":"en-US,en;q=0.9"}', '', '::1', 1612355225, 0.132385, '1', 0),
	(47, 'middleware/checkout', 'get', '{"Host":"192.168.43.90:8081","Connection":"keep-alive","Cache-Control":"max-age=0","Upgrade-Insecure-Requests":"1","User-Agent":"Mozilla\\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\\/537.36 (KHTML, like Gecko) Chrome\\/88.0.4324.104 Safari\\/537.36","Accept":"text\\/html,application\\/xhtml+xml,application\\/xml;q=0.9,image\\/avif,image\\/webp,image\\/apng,*\\/*;q=0.8,application\\/signed-exchange;v=b3;q=0.9","Accept-Encoding":"gzip, deflate","Accept-Language":"en-US,en;q=0.9,hi;q=0.8"}', '', '192.168.43.90', 1612505424, NULL, '0', 405),
	(48, 'middleware/checkout', 'get', '{"Host":"192.168.43.90:8081","Connection":"keep-alive","Cache-Control":"max-age=0","Upgrade-Insecure-Requests":"1","User-Agent":"Mozilla\\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\\/537.36 (KHTML, like Gecko) Chrome\\/88.0.4324.104 Safari\\/537.36","Accept":"text\\/html,application\\/xhtml+xml,application\\/xml;q=0.9,image\\/avif,image\\/webp,image\\/apng,*\\/*;q=0.8,application\\/signed-exchange;v=b3;q=0.9","Accept-Encoding":"gzip, deflate","Accept-Language":"en-US,en;q=0.9,hi;q=0.8"}', '', '192.168.43.90', 1612505424, 0.112652, '1', 0),
	(49, 'middleware/checkout', 'get', '{"Host":"192.168.43.90:8081","Connection":"keep-alive","Cache-Control":"max-age=0","Upgrade-Insecure-Requests":"1","User-Agent":"Mozilla\\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\\/537.36 (KHTML, like Gecko) Chrome\\/88.0.4324.104 Safari\\/537.36","Accept":"text\\/html,application\\/xhtml+xml,application\\/xml;q=0.9,image\\/avif,image\\/webp,image\\/apng,*\\/*;q=0.8,application\\/signed-exchange;v=b3;q=0.9","Accept-Encoding":"gzip, deflate","Accept-Language":"en-US,en;q=0.9,hi;q=0.8"}', '', '192.168.43.90', 1612505462, NULL, '0', 403),
	(50, 'middleware/checkout', 'get', '{"Host":"192.168.43.90:8081","Connection":"keep-alive","Cache-Control":"max-age=0","Upgrade-Insecure-Requests":"1","User-Agent":"Mozilla\\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\\/537.36 (KHTML, like Gecko) Chrome\\/88.0.4324.104 Safari\\/537.36","Accept":"text\\/html,application\\/xhtml+xml,application\\/xml;q=0.9,image\\/avif,image\\/webp,image\\/apng,*\\/*;q=0.8,application\\/signed-exchange;v=b3;q=0.9","Accept-Encoding":"gzip, deflate","Accept-Language":"en-US,en;q=0.9,hi;q=0.8"}', '', '192.168.43.90', 1612505462, 0.023993, '1', 0),
	(51, 'middleware/checkout', 'get', '{"Host":"192.168.43.90:8081","Connection":"keep-alive","Cache-Control":"max-age=0","Upgrade-Insecure-Requests":"1","User-Agent":"Mozilla\\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\\/537.36 (KHTML, like Gecko) Chrome\\/88.0.4324.104 Safari\\/537.36","Accept":"text\\/html,application\\/xhtml+xml,application\\/xml;q=0.9,image\\/avif,image\\/webp,image\\/apng,*\\/*;q=0.8,application\\/signed-exchange;v=b3;q=0.9","Accept-Encoding":"gzip, deflate","Accept-Language":"en-US,en;q=0.9,hi;q=0.8"}', '', '192.168.43.90', 1612505574, NULL, '0', 403),
	(52, 'middleware/checkout', 'get', '{"Host":"192.168.43.90:8081","Connection":"keep-alive","Cache-Control":"max-age=0","Upgrade-Insecure-Requests":"1","User-Agent":"Mozilla\\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\\/537.36 (KHTML, like Gecko) Chrome\\/88.0.4324.104 Safari\\/537.36","Accept":"text\\/html,application\\/xhtml+xml,application\\/xml;q=0.9,image\\/avif,image\\/webp,image\\/apng,*\\/*;q=0.8,application\\/signed-exchange;v=b3;q=0.9","Accept-Encoding":"gzip, deflate","Accept-Language":"en-US,en;q=0.9,hi;q=0.8"}', '', '192.168.43.90', 1612505574, 0.0159159, '1', 0),
	(53, 'middleware/checkout', 'get', '{"Host":"192.168.43.90:8081","Connection":"keep-alive","Cache-Control":"max-age=0","Upgrade-Insecure-Requests":"1","User-Agent":"Mozilla\\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\\/537.36 (KHTML, like Gecko) Chrome\\/88.0.4324.104 Safari\\/537.36","Accept":"text\\/html,application\\/xhtml+xml,application\\/xml;q=0.9,image\\/avif,image\\/webp,image\\/apng,*\\/*;q=0.8,application\\/signed-exchange;v=b3;q=0.9","Accept-Encoding":"gzip, deflate","Accept-Language":"en-US,en;q=0.9,hi;q=0.8"}', '', '192.168.43.90', 1612505588, NULL, '0', 403),
	(54, 'middleware/checkout', 'get', '{"Host":"192.168.43.90:8081","Connection":"keep-alive","Cache-Control":"max-age=0","Upgrade-Insecure-Requests":"1","User-Agent":"Mozilla\\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\\/537.36 (KHTML, like Gecko) Chrome\\/88.0.4324.104 Safari\\/537.36","Accept":"text\\/html,application\\/xhtml+xml,application\\/xml;q=0.9,image\\/avif,image\\/webp,image\\/apng,*\\/*;q=0.8,application\\/signed-exchange;v=b3;q=0.9","Accept-Encoding":"gzip, deflate","Accept-Language":"en-US,en;q=0.9,hi;q=0.8"}', '', '192.168.43.90', 1612505588, 0.0176301, '1', 0),
	(55, 'middleware/checkout', 'get', '{"Host":"192.168.43.90:8081","Connection":"keep-alive","Cache-Control":"max-age=0","Upgrade-Insecure-Requests":"1","User-Agent":"Mozilla\\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\\/537.36 (KHTML, like Gecko) Chrome\\/88.0.4324.104 Safari\\/537.36","Accept":"text\\/html,application\\/xhtml+xml,application\\/xml;q=0.9,image\\/avif,image\\/webp,image\\/apng,*\\/*;q=0.8,application\\/signed-exchange;v=b3;q=0.9","Accept-Encoding":"gzip, deflate","Accept-Language":"en-US,en;q=0.9,hi;q=0.8"}', '', '192.168.43.90', 1612505684, NULL, '0', 403),
	(56, 'middleware/checkout', 'get', '{"Host":"192.168.43.90:8081","Connection":"keep-alive","Cache-Control":"max-age=0","Upgrade-Insecure-Requests":"1","User-Agent":"Mozilla\\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\\/537.36 (KHTML, like Gecko) Chrome\\/88.0.4324.104 Safari\\/537.36","Accept":"text\\/html,application\\/xhtml+xml,application\\/xml;q=0.9,image\\/avif,image\\/webp,image\\/apng,*\\/*;q=0.8,application\\/signed-exchange;v=b3;q=0.9","Accept-Encoding":"gzip, deflate","Accept-Language":"en-US,en;q=0.9,hi;q=0.8"}', '', '192.168.43.90', 1612505684, 0.025069, '1', 0),
	(57, 'middleware/checkout', 'get', '{"Host":"192.168.43.90:8081","Connection":"keep-alive","Cache-Control":"max-age=0","Upgrade-Insecure-Requests":"1","User-Agent":"Mozilla\\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\\/537.36 (KHTML, like Gecko) Chrome\\/88.0.4324.104 Safari\\/537.36","Accept":"text\\/html,application\\/xhtml+xml,application\\/xml;q=0.9,image\\/avif,image\\/webp,image\\/apng,*\\/*;q=0.8,application\\/signed-exchange;v=b3;q=0.9","Accept-Encoding":"gzip, deflate","Accept-Language":"en-US,en;q=0.9,hi;q=0.8"}', '', '192.168.43.90', 1612505701, NULL, '0', 403),
	(58, 'middleware/checkout', 'get', '{"Host":"192.168.43.90:8081","Connection":"keep-alive","Cache-Control":"max-age=0","Upgrade-Insecure-Requests":"1","User-Agent":"Mozilla\\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\\/537.36 (KHTML, like Gecko) Chrome\\/88.0.4324.104 Safari\\/537.36","Accept":"text\\/html,application\\/xhtml+xml,application\\/xml;q=0.9,image\\/avif,image\\/webp,image\\/apng,*\\/*;q=0.8,application\\/signed-exchange;v=b3;q=0.9","Accept-Encoding":"gzip, deflate","Accept-Language":"en-US,en;q=0.9,hi;q=0.8"}', '', '192.168.43.90', 1612505701, 0.0242171, '1', 0),
	(59, 'middleware/checkout', 'get', '{"Host":"192.168.43.90:8081","Connection":"keep-alive","Cache-Control":"max-age=0","Upgrade-Insecure-Requests":"1","User-Agent":"Mozilla\\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\\/537.36 (KHTML, like Gecko) Chrome\\/88.0.4324.104 Safari\\/537.36","Accept":"text\\/html,application\\/xhtml+xml,application\\/xml;q=0.9,image\\/avif,image\\/webp,image\\/apng,*\\/*;q=0.8,application\\/signed-exchange;v=b3;q=0.9","Accept-Encoding":"gzip, deflate","Accept-Language":"en-US,en;q=0.9,hi;q=0.8"}', '', '192.168.43.90', 1612505724, NULL, '0', 403),
	(60, 'middleware/checkout', 'get', '{"Host":"192.168.43.90:8081","Connection":"keep-alive","Cache-Control":"max-age=0","Upgrade-Insecure-Requests":"1","User-Agent":"Mozilla\\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\\/537.36 (KHTML, like Gecko) Chrome\\/88.0.4324.104 Safari\\/537.36","Accept":"text\\/html,application\\/xhtml+xml,application\\/xml;q=0.9,image\\/avif,image\\/webp,image\\/apng,*\\/*;q=0.8,application\\/signed-exchange;v=b3;q=0.9","Accept-Encoding":"gzip, deflate","Accept-Language":"en-US,en;q=0.9,hi;q=0.8"}', '', '192.168.43.90', 1612505724, 0.0146642, '1', 0),
	(61, 'middleware/checkout', 'get', '{"Host":"192.168.43.90:8081","Connection":"keep-alive","Cache-Control":"max-age=0","Upgrade-Insecure-Requests":"1","User-Agent":"Mozilla\\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\\/537.36 (KHTML, like Gecko) Chrome\\/88.0.4324.104 Safari\\/537.36","Accept":"text\\/html,application\\/xhtml+xml,application\\/xml;q=0.9,image\\/avif,image\\/webp,image\\/apng,*\\/*;q=0.8,application\\/signed-exchange;v=b3;q=0.9","Accept-Encoding":"gzip, deflate","Accept-Language":"en-US,en;q=0.9,hi;q=0.8"}', '', '192.168.43.90', 1612505737, NULL, '0', 403),
	(62, 'middleware/checkout', 'get', '{"Host":"192.168.43.90:8081","Connection":"keep-alive","Cache-Control":"max-age=0","Upgrade-Insecure-Requests":"1","User-Agent":"Mozilla\\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\\/537.36 (KHTML, like Gecko) Chrome\\/88.0.4324.104 Safari\\/537.36","Accept":"text\\/html,application\\/xhtml+xml,application\\/xml;q=0.9,image\\/avif,image\\/webp,image\\/apng,*\\/*;q=0.8,application\\/signed-exchange;v=b3;q=0.9","Accept-Encoding":"gzip, deflate","Accept-Language":"en-US,en;q=0.9,hi;q=0.8"}', '', '192.168.43.90', 1612505737, 0.018219, '1', 0),
	(63, 'middleware/checkout', 'get', '{"Host":"192.168.43.90:8081","Connection":"keep-alive","Cache-Control":"max-age=0","Upgrade-Insecure-Requests":"1","User-Agent":"Mozilla\\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\\/537.36 (KHTML, like Gecko) Chrome\\/88.0.4324.104 Safari\\/537.36","Accept":"text\\/html,application\\/xhtml+xml,application\\/xml;q=0.9,image\\/avif,image\\/webp,image\\/apng,*\\/*;q=0.8,application\\/signed-exchange;v=b3;q=0.9","Accept-Encoding":"gzip, deflate","Accept-Language":"en-US,en;q=0.9,hi;q=0.8"}', '', '192.168.43.90', 1612505789, NULL, '0', 403),
	(64, 'middleware/checkout', 'get', '{"Host":"192.168.43.90:8081","Connection":"keep-alive","Cache-Control":"max-age=0","Upgrade-Insecure-Requests":"1","User-Agent":"Mozilla\\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\\/537.36 (KHTML, like Gecko) Chrome\\/88.0.4324.104 Safari\\/537.36","Accept":"text\\/html,application\\/xhtml+xml,application\\/xml;q=0.9,image\\/avif,image\\/webp,image\\/apng,*\\/*;q=0.8,application\\/signed-exchange;v=b3;q=0.9","Accept-Encoding":"gzip, deflate","Accept-Language":"en-US,en;q=0.9,hi;q=0.8"}', '', '192.168.43.90', 1612505789, 0.0189462, '1', 0),
	(65, 'middleware/checkout', 'get', '{"Host":"192.168.43.90:8081","Connection":"keep-alive","Cache-Control":"max-age=0","Upgrade-Insecure-Requests":"1","User-Agent":"Mozilla\\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\\/537.36 (KHTML, like Gecko) Chrome\\/88.0.4324.104 Safari\\/537.36","Accept":"text\\/html,application\\/xhtml+xml,application\\/xml;q=0.9,image\\/avif,image\\/webp,image\\/apng,*\\/*;q=0.8,application\\/signed-exchange;v=b3;q=0.9","Accept-Encoding":"gzip, deflate","Accept-Language":"en-US,en;q=0.9,hi;q=0.8"}', '', '192.168.43.90', 1612505794, NULL, '0', 403),
	(66, 'middleware/checkout', 'get', '{"Host":"192.168.43.90:8081","Connection":"keep-alive","Cache-Control":"max-age=0","Upgrade-Insecure-Requests":"1","User-Agent":"Mozilla\\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\\/537.36 (KHTML, like Gecko) Chrome\\/88.0.4324.104 Safari\\/537.36","Accept":"text\\/html,application\\/xhtml+xml,application\\/xml;q=0.9,image\\/avif,image\\/webp,image\\/apng,*\\/*;q=0.8,application\\/signed-exchange;v=b3;q=0.9","Accept-Encoding":"gzip, deflate","Accept-Language":"en-US,en;q=0.9,hi;q=0.8"}', '', '192.168.43.90', 1612505794, 0.0380712, '1', 0),
	(67, 'middleware/checkout', 'get', '{"Host":"192.168.43.90:8081","Connection":"keep-alive","Cache-Control":"max-age=0","Upgrade-Insecure-Requests":"1","User-Agent":"Mozilla\\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\\/537.36 (KHTML, like Gecko) Chrome\\/88.0.4324.104 Safari\\/537.36","Accept":"text\\/html,application\\/xhtml+xml,application\\/xml;q=0.9,image\\/avif,image\\/webp,image\\/apng,*\\/*;q=0.8,application\\/signed-exchange;v=b3;q=0.9","Accept-Encoding":"gzip, deflate","Accept-Language":"en-US,en;q=0.9,hi;q=0.8"}', '', '192.168.43.90', 1612505827, NULL, '0', 403),
	(68, 'middleware/checkout', 'get', '{"Host":"192.168.43.90:8081","Connection":"keep-alive","Cache-Control":"max-age=0","Upgrade-Insecure-Requests":"1","User-Agent":"Mozilla\\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\\/537.36 (KHTML, like Gecko) Chrome\\/88.0.4324.104 Safari\\/537.36","Accept":"text\\/html,application\\/xhtml+xml,application\\/xml;q=0.9,image\\/avif,image\\/webp,image\\/apng,*\\/*;q=0.8,application\\/signed-exchange;v=b3;q=0.9","Accept-Encoding":"gzip, deflate","Accept-Language":"en-US,en;q=0.9,hi;q=0.8"}', '', '192.168.43.90', 1612505827, 0.0154669, '1', 0),
	(69, 'middleware/checkout', 'get', '{"Host":"192.168.43.90:8081","Connection":"keep-alive","Cache-Control":"max-age=0","Upgrade-Insecure-Requests":"1","User-Agent":"Mozilla\\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\\/537.36 (KHTML, like Gecko) Chrome\\/88.0.4324.104 Safari\\/537.36","Accept":"text\\/html,application\\/xhtml+xml,application\\/xml;q=0.9,image\\/avif,image\\/webp,image\\/apng,*\\/*;q=0.8,application\\/signed-exchange;v=b3;q=0.9","Accept-Encoding":"gzip, deflate","Accept-Language":"en-US,en;q=0.9,hi;q=0.8"}', '', '192.168.43.90', 1612507317, NULL, '0', 403),
	(70, 'middleware/checkout', 'get', '{"Host":"192.168.43.90:8081","Connection":"keep-alive","Cache-Control":"max-age=0","Upgrade-Insecure-Requests":"1","User-Agent":"Mozilla\\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\\/537.36 (KHTML, like Gecko) Chrome\\/88.0.4324.104 Safari\\/537.36","Accept":"text\\/html,application\\/xhtml+xml,application\\/xml;q=0.9,image\\/avif,image\\/webp,image\\/apng,*\\/*;q=0.8,application\\/signed-exchange;v=b3;q=0.9","Accept-Encoding":"gzip, deflate","Accept-Language":"en-US,en;q=0.9,hi;q=0.8"}', '', '192.168.43.90', 1612507317, 1.04098, '1', 0),
	(71, 'middleware/checkout', 'get', '{"Host":"192.168.43.90:8081","Connection":"keep-alive","Cache-Control":"max-age=0","Upgrade-Insecure-Requests":"1","User-Agent":"Mozilla\\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\\/537.36 (KHTML, like Gecko) Chrome\\/88.0.4324.104 Safari\\/537.36","Accept":"text\\/html,application\\/xhtml+xml,application\\/xml;q=0.9,image\\/avif,image\\/webp,image\\/apng,*\\/*;q=0.8,application\\/signed-exchange;v=b3;q=0.9","Accept-Encoding":"gzip, deflate","Accept-Language":"en-US,en;q=0.9,hi;q=0.8"}', '', '192.168.43.90', 1612507363, NULL, '0', 403),
	(72, 'middleware/checkout', 'get', '{"Host":"192.168.43.90:8081","Connection":"keep-alive","Cache-Control":"max-age=0","Upgrade-Insecure-Requests":"1","User-Agent":"Mozilla\\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\\/537.36 (KHTML, like Gecko) Chrome\\/88.0.4324.104 Safari\\/537.36","Accept":"text\\/html,application\\/xhtml+xml,application\\/xml;q=0.9,image\\/avif,image\\/webp,image\\/apng,*\\/*;q=0.8,application\\/signed-exchange;v=b3;q=0.9","Accept-Encoding":"gzip, deflate","Accept-Language":"en-US,en;q=0.9,hi;q=0.8"}', '', '192.168.43.90', 1612507363, 1.81185, '1', 0),
	(73, 'middleware/checkout', 'get', '{"Host":"192.168.43.90:8081","Connection":"keep-alive","Cache-Control":"max-age=0","Upgrade-Insecure-Requests":"1","User-Agent":"Mozilla\\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\\/537.36 (KHTML, like Gecko) Chrome\\/88.0.4324.104 Safari\\/537.36","Accept":"text\\/html,application\\/xhtml+xml,application\\/xml;q=0.9,image\\/avif,image\\/webp,image\\/apng,*\\/*;q=0.8,application\\/signed-exchange;v=b3;q=0.9","Accept-Encoding":"gzip, deflate","Accept-Language":"en-US,en;q=0.9,hi;q=0.8"}', '', '192.168.43.90', 1612507478, NULL, '0', 403),
	(74, 'middleware/checkout', 'get', '{"Host":"192.168.43.90:8081","Connection":"keep-alive","Cache-Control":"max-age=0","Upgrade-Insecure-Requests":"1","User-Agent":"Mozilla\\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\\/537.36 (KHTML, like Gecko) Chrome\\/88.0.4324.104 Safari\\/537.36","Accept":"text\\/html,application\\/xhtml+xml,application\\/xml;q=0.9,image\\/avif,image\\/webp,image\\/apng,*\\/*;q=0.8,application\\/signed-exchange;v=b3;q=0.9","Accept-Encoding":"gzip, deflate","Accept-Language":"en-US,en;q=0.9,hi;q=0.8"}', '', '192.168.43.90', 1612507478, 1.38348, '1', 0),
	(75, 'middleware/checkout', 'get', '{"Host":"192.168.43.90:8081","Connection":"keep-alive","Cache-Control":"max-age=0","Upgrade-Insecure-Requests":"1","User-Agent":"Mozilla\\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\\/537.36 (KHTML, like Gecko) Chrome\\/88.0.4324.104 Safari\\/537.36","Accept":"text\\/html,application\\/xhtml+xml,application\\/xml;q=0.9,image\\/avif,image\\/webp,image\\/apng,*\\/*;q=0.8,application\\/signed-exchange;v=b3;q=0.9","Accept-Encoding":"gzip, deflate","Accept-Language":"en-US,en;q=0.9,hi;q=0.8"}', '', '192.168.43.90', 1612507552, NULL, '0', 403),
	(76, 'middleware/checkout', 'get', '{"Host":"192.168.43.90:8081","Connection":"keep-alive","Cache-Control":"max-age=0","Upgrade-Insecure-Requests":"1","User-Agent":"Mozilla\\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\\/537.36 (KHTML, like Gecko) Chrome\\/88.0.4324.104 Safari\\/537.36","Accept":"text\\/html,application\\/xhtml+xml,application\\/xml;q=0.9,image\\/avif,image\\/webp,image\\/apng,*\\/*;q=0.8,application\\/signed-exchange;v=b3;q=0.9","Accept-Encoding":"gzip, deflate","Accept-Language":"en-US,en;q=0.9,hi;q=0.8"}', '', '192.168.43.90', 1612507552, 1.58321, '1', 0),
	(77, 'middleware/checkout', 'get', '{"Host":"192.168.43.90:8081","Connection":"keep-alive","Cache-Control":"max-age=0","Upgrade-Insecure-Requests":"1","User-Agent":"Mozilla\\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\\/537.36 (KHTML, like Gecko) Chrome\\/88.0.4324.104 Safari\\/537.36","Accept":"text\\/html,application\\/xhtml+xml,application\\/xml;q=0.9,image\\/avif,image\\/webp,image\\/apng,*\\/*;q=0.8,application\\/signed-exchange;v=b3;q=0.9","Accept-Encoding":"gzip, deflate","Accept-Language":"en-US,en;q=0.9,hi;q=0.8"}', '', '192.168.43.90', 1612507591, 1.49125, '1', 0),
	(78, 'middleware/checkout', 'get', '{"Host":"192.168.43.90:8081","Connection":"keep-alive","Cache-Control":"max-age=0","Authorization":"Basic dGVzdEFjY291bnRGb3JfbWVyY2hhbnRfNTE3Njg6RlJnQ0NQUW15dlRVZVY2Sg==","Upgrade-Insecure-Requests":"1","User-Agent":"Mozilla\\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\\/537.36 (KHTML, like Gecko) Chrome\\/88.0.4324.104 Safari\\/537.36","Accept":"text\\/html,application\\/xhtml+xml,application\\/xml;q=0.9,image\\/avif,image\\/webp,image\\/apng,*\\/*;q=0.8,application\\/signed-exchange;v=b3;q=0.9","Accept-Encoding":"gzip, deflate","Accept-Language":"en-US,en;q=0.9,hi;q=0.8"}', '', '192.168.43.90', 1612507637, 1.55273, '1', 0),
	(79, 'middleware/checkout', 'get', '{"Host":"192.168.43.90:8081","Connection":"keep-alive","Cache-Control":"max-age=0","Authorization":"Basic dGVzdEFjY291bnRGb3JfbWVyY2hhbnRfNTE3Njg6RlJnQ0NQUW15dlRVZVY2Sg==","Upgrade-Insecure-Requests":"1","User-Agent":"Mozilla\\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\\/537.36 (KHTML, like Gecko) Chrome\\/88.0.4324.104 Safari\\/537.36","Accept":"text\\/html,application\\/xhtml+xml,application\\/xml;q=0.9,image\\/avif,image\\/webp,image\\/apng,*\\/*;q=0.8,application\\/signed-exchange;v=b3;q=0.9","Accept-Encoding":"gzip, deflate","Accept-Language":"en-US,en;q=0.9,hi;q=0.8"}', '', '192.168.43.90', 1612507673, 1.7567, '1', 0),
	(80, 'middleware/checkout', 'get', '{"Host":"192.168.43.90:8081","Connection":"keep-alive","Cache-Control":"max-age=0","Authorization":"Basic aW5mb0BiZXRsb2dpay5jb206Q3JFK3VkJGZyaStSVS01bVU2Km8=","Upgrade-Insecure-Requests":"1","User-Agent":"Mozilla\\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\\/537.36 (KHTML, like Gecko) Chrome\\/88.0.4324.104 Safari\\/537.36","Accept":"text\\/html,application\\/xhtml+xml,application\\/xml;q=0.9,image\\/avif,image\\/webp,image\\/apng,*\\/*;q=0.8,application\\/signed-exchange;v=b3;q=0.9","Accept-Encoding":"gzip, deflate","Accept-Language":"en-US,en;q=0.9,hi;q=0.8"}', '', '192.168.43.90', 1612507689, 1.48696, '1', 0),
	(81, 'middleware/checkout', 'get', '{"Host":"192.168.43.90:8081","Connection":"keep-alive","Cache-Control":"max-age=0","Authorization":"Basic Og==","Upgrade-Insecure-Requests":"1","User-Agent":"Mozilla\\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\\/537.36 (KHTML, like Gecko) Chrome\\/88.0.4324.104 Safari\\/537.36","Accept":"text\\/html,application\\/xhtml+xml,application\\/xml;q=0.9,image\\/avif,image\\/webp,image\\/apng,*\\/*;q=0.8,application\\/signed-exchange;v=b3;q=0.9","Accept-Encoding":"gzip, deflate","Accept-Language":"en-US,en;q=0.9,hi;q=0.8"}', '', '192.168.43.90', 1612507694, 1.38251, '1', 0),
	(82, 'middleware/checkout', 'get', '{"Host":"192.168.43.90:8081","Connection":"keep-alive","Cache-Control":"max-age=0","Upgrade-Insecure-Requests":"1","User-Agent":"Mozilla\\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\\/537.36 (KHTML, like Gecko) Chrome\\/88.0.4324.104 Safari\\/537.36","Accept":"text\\/html,application\\/xhtml+xml,application\\/xml;q=0.9,image\\/avif,image\\/webp,image\\/apng,*\\/*;q=0.8,application\\/signed-exchange;v=b3;q=0.9","Accept-Encoding":"gzip, deflate","Accept-Language":"en-US,en;q=0.9,hi;q=0.8"}', '', '192.168.43.90', 1612507701, 1.87298, '1', 0),
	(83, 'middleware/checkout', 'get', '{"Host":"192.168.43.90:8081","Connection":"keep-alive","Cache-Control":"max-age=0","Upgrade-Insecure-Requests":"1","User-Agent":"Mozilla\\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\\/537.36 (KHTML, like Gecko) Chrome\\/88.0.4324.104 Safari\\/537.36","Accept":"text\\/html,application\\/xhtml+xml,application\\/xml;q=0.9,image\\/avif,image\\/webp,image\\/apng,*\\/*;q=0.8,application\\/signed-exchange;v=b3;q=0.9","Accept-Encoding":"gzip, deflate","Accept-Language":"en-US,en;q=0.9,hi;q=0.8"}', '', '192.168.43.90', 1612507871, 1.36014, '1', 0),
	(84, 'middleware/checkout', 'get', '{"Host":"192.168.43.90:8081","Connection":"keep-alive","Cache-Control":"max-age=0","Authorization":"Basic dGVzdEFjY291bnRGb3JfbWVyY2hhbnRfNTE3Njg6N0U2OXpBR1BZV2prQ2kxQg==","Upgrade-Insecure-Requests":"1","User-Agent":"Mozilla\\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\\/537.36 (KHTML, like Gecko) Chrome\\/88.0.4324.104 Safari\\/537.36","Accept":"text\\/html,application\\/xhtml+xml,application\\/xml;q=0.9,image\\/avif,image\\/webp,image\\/apng,*\\/*;q=0.8,application\\/signed-exchange;v=b3;q=0.9","Accept-Encoding":"gzip, deflate","Accept-Language":"en-US,en;q=0.9,hi;q=0.8"}', '', '192.168.43.90', 1612507892, 1.37902, '1', 0),
	(85, 'middleware/checkout', 'get', '{"Host":"192.168.43.90:8081","Connection":"keep-alive","Cache-Control":"max-age=0","Upgrade-Insecure-Requests":"1","User-Agent":"Mozilla\\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\\/537.36 (KHTML, like Gecko) Chrome\\/88.0.4324.104 Safari\\/537.36","Accept":"text\\/html,application\\/xhtml+xml,application\\/xml;q=0.9,image\\/avif,image\\/webp,image\\/apng,*\\/*;q=0.8,application\\/signed-exchange;v=b3;q=0.9","Accept-Encoding":"gzip, deflate","Accept-Language":"en-US,en;q=0.9,hi;q=0.8"}', '', '192.168.43.90', 1612507915, 1.22836, '1', 0);
/*!40000 ALTER TABLE `logs` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
